#!/usr/bin/python

import requests
import json
import random
import socket
import argparse

def session_setup():

    #acquire content segment group call to retrieve rtsp url
    url = 'http://%s:%s/service/segment_groups/offerings/%s' % (MDMS_IP, MDMS_PORT, OFFERING)
    headers = {'content-type': 'application/json'}
    payload = {"view_type":VIEW_TYPE,"sms_id":SMS_ID,"subscriber_id":SUBSCRIBER_ID,"user_id":USER_ID,"equip_id":EQUIP_ID,"receiver_type":RECEIVER_TYPE,"encrypting_system_info":ENCRYPTING_SYSTEM_INFO,"equipment_capability_profiles":EQUIPMENT_CAPABILITY_PROFILES}
    r = requests.post(url, data=json.dumps(payload), headers=headers, timeout=5)

    #print "\nRESPONSE FROM ACQUIRE CONTENT SEGMENT GROUPS: ", r.text

    #make RTSP call to MDMS using content_urls from acquire content segment group
    #currently grabbing first item in content_urls which assumes it's the RTSP URL
    response = json.loads(r.text)
    rtsp_url = response['segment_groups'][0]['segments'][0]['content_configuration']['content_urls'][0]
    #print '\nRTSP_URL:', rtsp_url

    rtsp_field = "SETUP %s RTSP/1.0\r\n" % (rtsp_url)
    cseq_field = "CSeq: %s\r\n" % (random.randint(1000,9999))
    
    if DELIVERY_TYPE == "RF":
        transport_field = "Transport: MP2T/DVBC/QAM;unicast;destination=0.1;headend=%s\r\n" % HEADEND_ID
    elif DELIVERY_TYPE == "IP":
        transport_field = "Transport: RAW/RAW/UDP;unicast;destination=10.10.10.14;client_port=%s\r\n" % (random.randint(1000,9999))
    
    bandwidth_field = "Bandwidth: %s\r\n" % BANDWIDTH
    user_agent_field = "User-Agent: %s\r\n" % (USER_AGENT)

    message = "%s%s%s%s%s\r\n" % (rtsp_field, cseq_field, transport_field, bandwidth_field, user_agent_field)

    print message

    #send RTSP setup request to network adapter
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(15)
    #TODO need separate parameters for network adapter ip and port (than is used for acsg call)
    s.connect((MDMS_IP, 554))
    s.send(message)
    data = s.recv(1024)
    s.close()
    print data

    #TODO need some validation that MDMS responded with 307 Temorary Redirect.  do not attempt to pull session_id out of data if setup did not succeed

    #need to pull out session_id to be used in teardown
    global session_id
    session_id = data.split("ession:")[1].split(";")[0].strip()
    #print "session id:", session_id

###########################################################################################

def session_teardown():

   #TODO check session_id != "" before trying to tear down

    url = 'http://%s:%s/odrm/EventNotify' % (MDMS_IP, TEARDOWN_PORT)
    headers = {'content-type': 'application/xml; charset=utf-8'}
    #probably better to give current timestamp for eventDate or at least more current than 2010
    #payload = '<EventNotify xmlns="urn:com:comcast:ngsrm:2010:08:01" eventCode="5402" eventText="End of Stream Reached" eventDate="2010-10-23T16:04:37Z" npt="10.43"><SessionList><SessionId>%s</SessionId></SessionList></EventNotify>' % (session_id)

    payload="""<EventNotify
  xmlns="urn:com:comcast:ngsrm:2010:08:01"
  eventCode="5402"
  eventText="End of Stream Reached"
  eventDate="2010-10-23T16:04:37Z"
  npt="10.43">
  <SessionList>
    <SessionId>%s</SessionId>
  </SessionList>
</EventNotify>
""" % (session_id)

    #print "\nTeardown payload:", payload

    r = requests.post(url, payload, headers=headers, timeout=5)

    if r.status_code == 204:
        print "Session teardown SUCCESSFUL\n"
    else:
        print "Session teardown FAILED!!!\n"

    
###########################################################################################

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    #parameters for acquire content segment group call
    parser.add_argument("--mdmsip", dest = "mdms_ip", default="1.1.1.1")
    parser.add_argument("--mdmsport", dest = "mdms_port", default="8000")
    parser.add_argument("--offering", dest = "offering", default="")

    parser.add_argument("--viewtype", dest = "view_type", choices = ["MOVIE", "PREVIEW"], help = "default = MOVIE", default="MOVIE")
    parser.add_argument("--smsid", dest = "sms_id", default="msmw")
    parser.add_argument("--subscriberid", dest = "subscriber_id", default="sub1")
    parser.add_argument("--userid", dest = "user_id", default="user1")
    parser.add_argument("--equipmentid", dest = "equip_id", default="BEBEBEBEBEBE")
    parser.add_argument("--receivertype", dest = "receiver_type", default="vcas")
    parser.add_argument("--encryptingsysteminfo", dest = "encrypting_system_info", default="vcas-IPTV")
    parser.add_argument("--equipmentcapabilityprofiles", dest = "equipment_capability_profiles", default="M2T")

    #additional parameters for making RTSP call
    parser.add_argument("--useragent", dest = "user_agent", default="ZENTERIO")
    parser.add_argument("--headendid", dest = "headend_id", default="10001")
    parser.add_argument("--bandwidth", dest = "bandwidth", default="3750000")
    parser.add_argument("--deliverytype", dest = "deliverytype", choices = ["IP", "RF"], help = "default = IP", default="IP")

    parser.add_argument("--teardownport", dest = "teardown_port", default="29999")

    #parser.set_defaults(mdms_ip = "1.1.1.1", mdms_port = "8000", offering="1", viewtype="MOVIE", sms_id="msmw", subscriber_id="sub1", user_id="user1", equip_id="BEBEBEBEBEBE", receiver_type="vcas", encrypting_system_info="vcas-IPTV", equipment_capability_profiles="M2T", user_agent="ZENTERIO", headend_id="10001", bandwidth="3750000", teardown_port="29999")
    args = parser.parse_args()

    MDMS_IP = args.mdms_ip
    MDMS_PORT= args.mdms_port
    OFFERING = args.offering

    VIEW_TYPE = args.view_type
    SMS_ID = args.sms_id
    SUBSCRIBER_ID = args.subscriber_id
    USER_ID = args.user_id
    EQUIP_ID = args.equip_id
    RECEIVER_TYPE = args.receiver_type
    ENCRYPTING_SYSTEM_INFO = args.encrypting_system_info
    EQUIPMENT_CAPABILITY_PROFILES = args.equipment_capability_profiles

    USER_AGENT = args.user_agent
    HEADEND_ID = args.headend_id
    BANDWIDTH = args.bandwidth
    DELIVERY_TYPE = args.deliverytype

    TEARDOWN_PORT = args.teardown_port

    session_id=""

    session_setup()

    choice = "first time"
    options = [ "0", "1", "x", "X" ]

    while (choice != "x") and (choice != "X"):
        print "Options:"
        print "[0] Teardown"
        print "[1] Setup session"
        print "[X] Exit"

        choice = raw_input("Enter choice: ")
        choice = str(choice)

        while choice not in options:
            choice = raw_input("Enter choice: ")
            choice = str(choice)
	
        if choice == "0":
            session_teardown()
        
        elif choice == "1":
            session_setup()