"""Script to simulate a simple ADS plus helper methods for automated functional testing"""

from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
from SocketServer import ThreadingMixIn
from urlparse import urlparse, parse_qs

import argparse
import uuid
import requests
import json
import time

from xml.dom import minidom

def readAdPlacements():
    """read assets.csv file containing ad placements
    first value is the provider id followed by a list of paids which are the ads
    to be placed into content for that provider id
    Example: providerA,adprovider1::adasset1,adprovider2::adasset2
    """

    try:
        with open("assets.csv", "r") as f:
            for line in f:
                line = line.strip()
                adPlacements[line.split(',')[0]] = line.split(',')[1:]
        print "adPlacements: ", adPlacements
    except IOError:
        print "Could not find ./assets.csv!  I'm OK though."

###############################################################################

def buildPlacementDecision(numAds, placementOpportunityId, providerId, opportunityType):
    """build PlacementDecision element for ads"""

    if providerId in adPlacements:
        ads = adPlacements[providerId]
    else:
        ads = ['adsrus.com::ADAD1234123412341234']

    #opportunityType attribute is not required according to SCTE130-3 but ADMCore
    #parses it to be included in the playlist response.  It was present in the example from
    # the BlackArrow PIE.
    beginning = """\n<ns2:PlacementDecision id="%s" placementOpportunityRef="%s"><ns2:OpportunityBinding opportunityType="%s" />""" % (str(uuid.uuid1()), placementOpportunityId, opportunityType)
    end = """\n</ns2:PlacementDecision>"""
    placements = ""

    if numAds > 0:
        for i in range(numAds):
            indexOfAd = (len(ads)+i) % len(ads)
            temp = """\n    <ns2:Placement action="replace" id="%s">
        <Content>
            <AssetRef assetID="%s" providerID="%s" />
            <Tracking>%s</Tracking>
        </Content>
    </ns2:Placement>""" % (str(uuid.uuid1()), ads[indexOfAd].split('::')[1], ads[indexOfAd].split('::')[0], str(uuid.uuid1()))
            placements = placements + temp

    return beginning + placements + end

###############################################################################

def buildEntertainmentPlacementDecision(providerId, assetId, startNpt, endNpt, placementOpportunityId):
    """build PlacementDecision element for entertainment"""

    beginning = """\n<ns2:PlacementDecision id="%s" placementOpportunityRef="%s">""" % (str(uuid.uuid1()), placementOpportunityId)
    end = """\n</ns2:PlacementDecision>"""

    #build optional entertainmentNpt if not playing in entirety
    startNpt = str(startNpt)
    endNpt = str(endNpt)
    if startNpt == "BOS" and endNpt == "EOS":
        entertainmentNpt = ""
    else:
        entertainmentNpt = """\n        <ns2:EntertainmentNPT scale="1.0">%s-%s</ns2:EntertainmentNPT>""" % (startNpt, endNpt)

    entertainment = """\n    <ns2:Entertainment>
        <Content>
            <AssetRef assetID="%s" providerID="%s" />
        </Content>%s
    </ns2:Entertainment>""" % (assetId, providerId, entertainmentNpt)

    return beginning + entertainment + end

###############################################################################

def buildPlacementResponse(identity, system, messageId, placementOpportunityId, providerId, assetId, numPreroll, numMidroll, numPostroll):
    """build entire PlacementRequest"""

    beginning = """<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
    <S:Body>
        <ns2:PlacementResponse xmlns="http://www.scte.org/schemas/130-2/2008a/core" xmlns:ns2="http://www.scte.org/schemas/130-3/2008a/adm" xmlns:ns3="http://www.scte.org/schemas/130-8/2010a/gis" xmlns:ns4="http://www.scte.org/schemas/130-3/2008a/adm/podm" identity="%s" messageId="%s" messageRef="%s" system="%s" version="1.1">
    <StatusCode class="0" />""" % (identity, str(uuid.uuid1()), messageId, system)
    end = """\n        </ns2:PlacementResponse>
    </S:Body>
</S:Envelope>"""

    if numPreroll > 0:
        preRoll = buildPlacementDecision(numPreroll, placementOpportunityId, providerId, "Preroll")
    else:
        preRoll = ""

    if numMidroll > 0:
        midRoll = buildPlacementDecision(numMidroll, placementOpportunityId, providerId, "Midroll")
        entertainment1 = buildEntertainmentPlacementDecision(providerId, assetId, 0, 30, placementOpportunityId)
        entertainment2 = buildEntertainmentPlacementDecision(providerId, assetId, 30, 60, placementOpportunityId)
    else:
        midRoll = ""
        entertainment1 = buildEntertainmentPlacementDecision(providerId, assetId, "BOS", "EOS", placementOpportunityId)
        entertainment2 = ""

    if numPostroll > 0:
        postRoll = buildPlacementDecision(numPostroll, placementOpportunityId, providerId, "Postroll")
    else:
        postRoll = ""

    return beginning + preRoll + entertainment1 + midRoll + entertainment2 + postRoll + end

###############################################################################

def clearMessageCache():
    """clear the cache of last* messages the sim remembers"""

    global lastPlacementRequest
    global lastPlacementResponse
    global lastPlacementStatusNotification
    global lastDeregistrationNotification
    global lastServiceStatusNotification

    lastPlacementRequest = ''
    lastPlacementResponse = ''
    lastPlacementStatusNotification = ''
    lastDeregistrationNotification = ''
    lastServiceStatusNotification = ''

###############################################################################

def clearNegativeResponseSettings():
    """clear any negative response settings"""

    global negativePlacementStatusAck
    global negativePlacementStatusAckCount
    global psnAckSleep
    global soapRequestSleep
    global repeatAds
    global dynamicAdPlacements
    global numPreroll
    global numMidroll
    global numPostroll

    negativePlacementStatusAck = False
    negativePlacementStatusAckCount = 0
    psnAckSleep = 0
    soapRequestSleep = False
    repeatAds = False
    dynamicAdPlacements = False
    numPreroll = 0
    numMidroll = 0
    numPostroll = 0

###############################################################################

class ADSSim(BaseHTTPRequestHandler):
    """Class to extend BaseHTTPRequestHandler to implement HTTP methods"""

    def do_GET(self):
        """Get method triggers registration with ADM and returns some specific last requests/responses"""
        print "**client request:", self.path

        if self.path.find("?wsdl") >= 0:
            #ADM asking for the wsdl on ADS registration
            response = """
<wsdl:definitions xmlns:tns="http://www.scte.org/wsdl/130-3/2010/ads" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:soap12="http://schemas.xmlsoap.org/wsdl/soap12/" xmlns:http="http://schemas.xmlsoap.org/wsdl/http/" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/" xmlns:adm="http://www.scte.org/schemas/130-3/2008a/adm" xmlns:podm="http://www.scte.org/schemas/130-3/2008a/adm/podm" xmlns:core="http://www.scte.org/schemas/130-2/2008a/core" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" name="ADS" targetNamespace="http://www.scte.org/wsdl/130-3/2010/ads" xsi:schemaLocation="http://www.scte.org/schemas/130-3/2008a/adm/podm SCTE_130-3_2010_podm.xsd">
	<wsdl:types>
		<xs:schema targetNamespace="http://www.scte.org/wsdl/130-3/2010/ads" version="1.0" elementFormDefault="qualified" attributeFormDefault="unqualified">
			<xs:import namespace="http://www.scte.org/schemas/130-3/2008a/adm" schemaLocation="http://%s:%s/adrouter/130_playlist/2010/soap?xsd=2"/>
			<xs:import namespace="http://www.scte.org/schemas/130-3/2008a/adm/podm" schemaLocation="http://%s:%s/adrouter/130_playlist/2010/soap?xsd=3"/>
		</xs:schema>
	</wsdl:types>
	<!-- Common Client/Server Message Types -->
	<wsdl:message name="ServiceCheckRequest">
		<wsdl:part name="request" element="core:ServiceCheckRequest"/>
	</wsdl:message>
	<wsdl:message name="ServiceCheckResponse">
		<wsdl:part name="response" element="core:ServiceCheckResponse"/>
	</wsdl:message>
	<wsdl:message name="ServiceStatusNotification">
		<wsdl:part name="request" element="core:ServiceStatusNotification"/>
	</wsdl:message>
	<wsdl:message name="ServiceStatusAcknowledgement">
		<wsdl:part name="response" element="core:ServiceStatusAcknowledgement"/>
	</wsdl:message>
	<!-- ADS Server Message Types -->
	<wsdl:message name="PlacementStatusNotification">
		<wsdl:part name="request" element="adm:PlacementStatusNotification"/>
	</wsdl:message>
	<wsdl:message name="PlacementStatusAcknowledgement">
		<wsdl:part name="response" element="adm:PlacementStatusAcknowledgement"/>
	</wsdl:message>
	<wsdl:message name="ADSDeregistrationAcknowledgement">
		<wsdl:part name="response" element="adm:ADSDeregistrationAcknowledgement"/>
	</wsdl:message>
	<wsdl:message name="ADSDeregistrationNotification">
		<wsdl:part name="request" element="adm:ADSDeregistrationNotification"/>
	</wsdl:message>
	<wsdl:message name="PlacementRequest">
		<wsdl:part name="request" element="adm:PlacementRequest"/>
	</wsdl:message>
	<wsdl:message name="PlacementResponse">
		<wsdl:part name="response" element="adm:PlacementResponse"/>
	</wsdl:message>
	<wsdl:portType name="ADS">
		<wsdl:operation name="PlacementRequest">
			<wsdl:input message="tns:PlacementRequest"/>
			<wsdl:output message="tns:PlacementResponse"/>
		</wsdl:operation>
		<wsdl:operation name="PlacementStatusNotification">
			<wsdl:input message="tns:PlacementStatusNotification"/>
			<wsdl:output message="tns:PlacementStatusAcknowledgement"/>
		</wsdl:operation>
		<wsdl:operation name="ServiceCheckRequest">
			<wsdl:input message="tns:ServiceCheckRequest"/>
			<wsdl:output message="tns:ServiceCheckResponse"/>
		</wsdl:operation>
		<wsdl:operation name="ServiceStatusNotification">
			<wsdl:input message="tns:ServiceStatusNotification"/>
			<wsdl:output message="tns:ServiceStatusAcknowledgement"/>
		</wsdl:operation>
		<wsdl:operation name="ADSDeregistrationNotification">
			<wsdl:input message="tns:ADSDeregistrationNotification"/>
			<wsdl:output message="tns:ADSDeregistrationAcknowledgement"/>
		</wsdl:operation>
	</wsdl:portType>

	<wsdl:binding name="ADSBindingSoap" type="tns:ADS">
		<soap:binding style="document" transport="http://schemas.xmlsoap.org/soap/http"/>
		<wsdl:operation name="PlacementRequest">
			<soap:operation soapAction="PlacementRequest"/>
			<wsdl:input>
				<soap:body use="literal"/>
			</wsdl:input>
			<wsdl:output>
				<soap:body use="literal"/>
			</wsdl:output>
		</wsdl:operation>
		<wsdl:operation name="PlacementStatusNotification">
			<soap:operation soapAction="PlacementStatusNotification"/>
			<wsdl:input>
				<soap:body use="literal"/>
			</wsdl:input>
			<wsdl:output>
				<soap:body use="literal"/>
			</wsdl:output>
		</wsdl:operation>
		<wsdl:operation name="ServiceCheckRequest">
			<soap:operation soapAction="ServiceCheckRequest"/>
			<wsdl:input>
				<soap:body use="literal"/>
			</wsdl:input>
			<wsdl:output>
				<soap:body use="literal"/>
			</wsdl:output>
		</wsdl:operation>
		<wsdl:operation name="ServiceStatusNotification">
			<soap:operation soapAction="ServiceStatusNotification"/>
			<wsdl:input>
				<soap:body use="literal"/>
			</wsdl:input>
			<wsdl:output>
				<soap:body use="literal"/>
			</wsdl:output>
		</wsdl:operation>
		<wsdl:operation name="ADSDeregistrationNotification">
			<soap:operation soapAction="ADSDeregistrationNotification"/>
			<wsdl:input>
				<soap:body use="literal"/>
			</wsdl:input>
			<wsdl:output>
				<soap:body use="literal"/>
			</wsdl:output>
		</wsdl:operation>
	</wsdl:binding>

	<wsdl:service name="ADSService">
<!--
    <wsdl:port name="ADSPortSoap" binding="tns:ADSBindingSoap">
			<soap:address location="http://%s:%s/adrouter/130_playlist/2010/soap"/>
		</wsdl:port>
-->
		<wsdl:port name="ADSPort" binding="tns:ADSBindingSoap">
			<soap:address location="http://%s:%s/adrouter/130_playlist/2010/soap"/>
		</wsdl:port>
	</wsdl:service>
</wsdl:definitions>
""" % (IP, PORT, IP, PORT, IP, PORT, IP, PORT)

            contentLength = len(response)
            self.server_version = "Python ADSSim"
            self.sys_version = ""
            self.send_response(200)
            self.send_header("X-Powered-By", "Servlet/2.5")
            self.send_header("Content-Type", 'text/xml;charset="utf-8"')
            self.send_header("Content-Length", contentLength)
            self.end_headers()
            self.wfile.write(response)
            print "\nSending " + self.path + " >>>>>>>>>>\n", response
            print "-----------------------------------------------\n"

###############################################################################
        elif self.path.find("?xsd=1") >= 0:
            response = """<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://www.scte.org/schemas/130-2/2008a/core" targetNamespace="http://www.scte.org/schemas/130-2/2008a/core" elementFormDefault="qualified" attributeFormDefault="unqualified">
	<!--***** Start Common Message ***** -->
	<!-- Service Check Request Message -->
	<xsd:complexType name="ServiceCheckRequestType">
		<xsd:complexContent>
			<xsd:extension base="Msg_RequestBaseType">
				<xsd:sequence>
					<xsd:element ref="Ext" minOccurs="0"/>
				</xsd:sequence>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:element name="ServiceCheckRequest" type="ServiceCheckRequestType">
		<xsd:annotation>
			<xsd:documentation>Service check request message element.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Service Check Response Message -->
	<xsd:complexType name="ServiceCheckResponseType">
		<xsd:complexContent>
			<xsd:extension base="Msg_ResponseBaseType">
				<xsd:sequence>
					<xsd:element ref="Ext" minOccurs="0"/>
				</xsd:sequence>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:element name="ServiceCheckResponse" type="ServiceCheckResponseType">
		<xsd:annotation>
			<xsd:documentation>Service check response message element.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Service Status Notification Message -->
	<xsd:complexType name="ServiceStatusNotificationType">
		<xsd:complexContent>
			<xsd:extension base="Msg_NotificationBaseType">
				<xsd:sequence>
					<xsd:element ref="StatusCode">
						<xsd:annotation>
							<xsd:documentation>Notification interpretation identifier.</xsd:documentation>
						</xsd:annotation>
					</xsd:element>
					<xsd:element ref="Ext" minOccurs="0"/>
				</xsd:sequence>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:element name="ServiceStatusNotification" type="ServiceStatusNotificationType">
		<xsd:annotation>
			<xsd:documentation>Service status notification message element.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Service Status Acknowledgement Message -->
	<xsd:complexType name="ServiceStatusAcknowledgementType">
		<xsd:complexContent>
			<xsd:extension base="Msg_AcknowledgementBaseType">
				<xsd:sequence>
					<xsd:element ref="Ext" minOccurs="0"/>
				</xsd:sequence>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:element name="ServiceStatusAcknowledgement" type="ServiceStatusAcknowledgementType">
		<xsd:annotation>
			<xsd:documentation>Service status acknowledgement message element.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--***** End Common Messages ***** -->
	<!---->
	<!--Global types -->
	<xsd:simpleType name="dateTimeTimezoneType">
		<xsd:annotation>
			<xsd:documentation>xsd:dateTime that requires a timezone indicator.</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="xsd:dateTime">
			<xsd:pattern value="[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}([\.][0-9]*)?(Z|([\-+][0-9]+:[0-9]+))"/>
		</xsd:restriction>
	</xsd:simpleType>
	<xsd:simpleType name="identityAttrType">
		<xsd:annotation>
			<xsd:documentation>Unique identity identifier.</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="nonEmptyStringType"/>
	</xsd:simpleType>
	<xsd:simpleType name="idAttrType">
		<xsd:annotation>
			<xsd:documentation>Unique identifier.</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="nonEmptyStringType"/>
	</xsd:simpleType>
	<xsd:simpleType name="mediaAvailableAttrType">
		<xsd:annotation>
			<xsd:documentation>Tri-state media availability indicator.</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="xsd:boolean"/>
	</xsd:simpleType>
	<xsd:simpleType name="messageIdAttrType">
		<xsd:annotation>
			<xsd:documentation>Original message identifier.</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="nonEmptyStringType"/>
	</xsd:simpleType>
	<xsd:simpleType name="messageRefAttrType">
		<xsd:annotation>
			<xsd:documentation>Reference to original message.</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="messageIdAttrType"/>
	</xsd:simpleType>
	<xsd:simpleType name="nonEmptyStringType">
		<xsd:annotation>
			<xsd:documentation>Non empty xsd:string.</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="xsd:string">
			<xsd:minLength value="1"/>
		</xsd:restriction>
	</xsd:simpleType>
	<xsd:simpleType name="registrationRefAttrType">
		<xsd:annotation>
			<xsd:documentation>Reference to a specific registration message.</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="messageIdAttrType"/>
	</xsd:simpleType>
	<xsd:simpleType name="resendAttrType">
		<xsd:annotation>
			<xsd:documentation>Message retransmission identifier.</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="messageIdAttrType"/>
	</xsd:simpleType>
	<xsd:simpleType name="systemAttrType">
		<xsd:annotation>
			<xsd:documentation>Message origin identifier.</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="nonEmptyStringType"/>
	</xsd:simpleType>
	<xsd:simpleType name="versionAttrType">
		<xsd:annotation>
			<xsd:documentation>Interface version number.</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="nonEmptyStringType"/>
	</xsd:simpleType>
	<!---->
	<!--Attributes on all messages -->
	<xsd:attributeGroup name="Msg_BaseAttrsType">
		<xsd:annotation>
			<xsd:documentation>Common attributes required by all message elements.</xsd:documentation>
		</xsd:annotation>
		<xsd:attribute name="messageId" type="messageIdAttrType" use="required">
			<xsd:annotation>
				<xsd:documentation>Unique message element identifier.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="version" type="versionAttrType" use="required">
			<xsd:annotation>
				<xsd:documentation>Assigned specification part version number.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="identity" type="identityAttrType" use="required">
			<xsd:annotation>
				<xsd:documentation>Logical service source identifier.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="system" type="systemAttrType" use="optional">
			<xsd:annotation>
				<xsd:documentation>Message origin identifier.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
	</xsd:attributeGroup>
	<xsd:attributeGroup name="Msg_ReqNotifyBaseAttrsType">
		<xsd:annotation>
			<xsd:documentation>Common attributes on all request/notification message elements.</xsd:documentation>
		</xsd:annotation>
		<xsd:attributeGroup ref="Msg_BaseAttrsType"/>
		<xsd:attribute name="resend" type="resendAttrType" use="optional">
			<xsd:annotation>
				<xsd:documentation>Message retransmission identifier.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
	</xsd:attributeGroup>
	<xsd:attributeGroup name="Msg_RespAckBaseAttrsType">
		<xsd:annotation>
			<xsd:documentation>Common attributes on all response/acknowledgement message elements.</xsd:documentation>
		</xsd:annotation>
		<xsd:attributeGroup ref="Msg_BaseAttrsType"/>
		<xsd:attribute name="messageRef" type="messageRefAttrType" use="required">
			<xsd:annotation>
				<xsd:documentation>Reference to original message.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
	</xsd:attributeGroup>
	<!---->
	<!--***** Start Common Message Bases ***** -->
	<!-- Common message bases used by all messages -->
	<xsd:complexType name="Msg_RequestBaseType">
		<xsd:annotation>
			<xsd:documentation>Common request message attributes.</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element ref="InitiatorData" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>Private data to be returned (i.e., echoed back) in the response message.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
		</xsd:sequence>
		<xsd:attributeGroup ref="Msg_ReqNotifyBaseAttrsType"/>
	</xsd:complexType>
	<xsd:complexType name="Msg_ResponseBaseType">
		<xsd:annotation>
			<xsd:documentation>Common response message attributes and elements.</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element ref="InitiatorData" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>Private data returned (i.e., echoed back) by the request message recipient if present in the original
						request message.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element ref="StatusCode">
				<xsd:annotation>
					<xsd:documentation>Request processing status code.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
		</xsd:sequence>
		<xsd:attributeGroup ref="Msg_RespAckBaseAttrsType"/>
	</xsd:complexType>
	<xsd:complexType name="Msg_NotificationBaseType">
		<xsd:annotation>
			<xsd:documentation>Common notification mesage attributes.</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element ref="InitiatorData" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>Private data to be returned (i.e., echoed back) in the acknowledgement message.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
		</xsd:sequence>
		<xsd:attributeGroup ref="Msg_ReqNotifyBaseAttrsType"/>
	</xsd:complexType>
	<xsd:complexType name="Msg_AcknowledgementBaseType">
		<xsd:annotation>
			<xsd:documentation>Common acknowledgement message attribues and elements.</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element ref="InitiatorData" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>Private data returned (i.e., echoed back) by the notification message recipient if present in the original
						notification message.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element ref="StatusCode">
				<xsd:annotation>
					<xsd:documentation>Notification processing status code.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
		</xsd:sequence>
		<xsd:attributeGroup ref="Msg_RespAckBaseAttrsType"/>
	</xsd:complexType>
	<!--***** End Common Message Elements ***** -->
	<!---->
	<!--***** Start Common Basic Elements ***** -->
	<!--Address element -->
	<xsd:complexType name="AddressType">
		<xsd:simpleContent>
			<xsd:extension base="xsd:string">
				<xsd:attribute name="type" type="nonEmptyStringType" use="optional">
					<xsd:annotation>
						<xsd:documentation>Endpoint information type. </xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:simpleContent>
	</xsd:complexType>
	<xsd:element name="Address" type="AddressType">
		<xsd:annotation>
			<xsd:documentation>Endpoint information.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Ad Type element -->
	<xsd:complexType name="AdTypeType">
		<xsd:simpleContent>
			<xsd:extension base="nonEmptyStringType">
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:simpleContent>
	</xsd:complexType>
	<xsd:element name="AdType" type="AdTypeType">
		<xsd:annotation>
			<xsd:documentation>Ad type identification.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Content Asset Reference (can be either entertainment, ad, etc.) -->
	<xsd:complexType name="AssetRefType">
		<xsd:attribute name="providerID" type="nonEmptyStringType" use="required">
			<xsd:annotation>
				<xsd:documentation>Provider identification. For example, CableLabs ADI.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="assetID" type="nonEmptyStringType" use="required">
			<xsd:annotation>
				<xsd:documentation>Asset identification and may be scoped by Provider identification. For example, CableLabs ADI.
				</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<xsd:element name="AssetRef" type="AssetRefType">
		<xsd:annotation>
			<xsd:documentation>Asset identification.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Callout Element -->
	<xsd:complexType name="CalloutType">
		<xsd:sequence>
			<xsd:element ref="Address" maxOccurs="unbounded"/>
			<xsd:element ref="Ext" minOccurs="0"/>
		</xsd:sequence>
		<xsd:attribute name="message" type="nonEmptyStringType" use="optional">
			<xsd:annotation>
				<xsd:documentation>Specific message to be delivered via the supplied destination. Omit if all messages are to be delivered to
					the specified destination.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<xsd:element name="Callout" type="CalloutType">
		<xsd:annotation>
			<xsd:documentation>All messages or per message type destination endpoint identification.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Content element -->
	<xsd:complexType name="ContentType">
		<xsd:sequence>
			<xsd:sequence>
				<xsd:annotation>
					<xsd:documentation>Content identification. Uniquely identifies a single piece of content (i.e. only one content entity).
						Multiple identification methods may be used to reference the single, unique content item. For exmaple, ad-ID and V-ISAN.
					</xsd:documentation>
				</xsd:annotation>
				<xsd:element ref="AssetRef" minOccurs="0"/>
				<xsd:element ref="Program" minOccurs="0"/>
				<xsd:sequence minOccurs="0">
					<xsd:element ref="SegmentationUpid" minOccurs="0" maxOccurs="unbounded"/>
				</xsd:sequence>
				<xsd:element ref="SpotRef" minOccurs="0"/>
				<xsd:element ref="URI" minOccurs="0"/>
			</xsd:sequence>
			<xsd:sequence>
				<xsd:annotation>
					<xsd:documentation>Group of content location identifiers.</xsd:documentation>
				</xsd:annotation>
				<xsd:element ref="ContentLocation" minOccurs="0" maxOccurs="unbounded"/>
			</xsd:sequence>
			<xsd:sequence>
				<xsd:annotation>
					<xsd:documentation>Additional content information</xsd:documentation>
				</xsd:annotation>
				<xsd:element ref="AdType" minOccurs="0"/>
				<xsd:element ref="Duration" minOccurs="0"/>
				<xsd:element ref="Tracking" minOccurs="0"/>
			</xsd:sequence>
			<xsd:element ref="Ext" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>Extensibility - elements from any namespace. The "metadata" from any auxillary sources are located herein.
					</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
		</xsd:sequence>
		<xsd:attribute name="id" type="idAttrType" use="optional">
			<xsd:annotation>
				<xsd:documentation>Unique identifier.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<xsd:element name="Content" type="ContentType">
		<xsd:annotation>
			<xsd:documentation>Content (entertainment, ad, etc.) description and identification.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--ContentDataModel element -->
	<xsd:complexType name="ContentDataModelType">
		<xsd:simpleContent>
			<xsd:extension base="nonEmptyStringType">
				<xsd:attribute name="type" type="nonEmptyStringType" use="optional">
					<xsd:annotation>
						<xsd:documentation>Content data model general type. </xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:simpleContent>
	</xsd:complexType>
	<xsd:element name="ContentDataModel" type="ContentDataModelType">
		<xsd:annotation>
			<xsd:documentation>Content data model identifier. Typically, a URI.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Content location element -->
	<xsd:complexType name="ContentLocationType">
		<xsd:simpleContent>
			<xsd:extension base="xsd:anyURI">
				<xsd:attribute name="mediaAvailable" type="mediaAvailableAttrType" use="optional">
					<xsd:annotation>
						<xsd:documentation>Media availability indicator. True=Asset available. False=Asset not available. Omitted = Asset availability
							unknown.</xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:simpleContent>
	</xsd:complexType>
	<xsd:element name="ContentLocation" type="ContentLocationType"/>
	<!--Current Time element -->
	<xsd:element name="CurrentDateTime" type="dateTimeTimezoneType"/>
	<!--Duration Control elements -->
	<xsd:complexType name="DurationType">
		<xsd:annotation>
			<xsd:documentation>Time format PnYn MnDTnH nMn.nnnS</xsd:documentation>
		</xsd:annotation>
		<xsd:simpleContent>
			<xsd:extension base="xsd:duration">
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:simpleContent>
	</xsd:complexType>
	<xsd:element name="Duration" type="DurationType">
		<xsd:annotation>
			<xsd:documentation>Specific run-time length.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Extensibility element -->
	<xsd:element name="Ext">
		<xsd:annotation>
			<xsd:documentation>Extensibility - elements from any namespace.</xsd:documentation>
		</xsd:annotation>
		<xsd:complexType>
			<xsd:sequence>
				<xsd:any namespace="##any" processContents="lax" minOccurs="0" maxOccurs="unbounded"/>
			</xsd:sequence>
			<xsd:anyAttribute namespace="##any" processContents="lax"/>
		</xsd:complexType>
	</xsd:element>
	<!--External status code element -->
	<xsd:complexType name="ExternalStatusCodeType">
		<xsd:sequence minOccurs="0">
			<xsd:element ref="Note" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element ref="Ext" minOccurs="0"/>
		</xsd:sequence>
		<xsd:attribute name="source" type="nonEmptyStringType" use="required">
			<xsd:annotation>
				<xsd:documentation>Identifier, typically a URL, specifying the detail attribute's source. </xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="detail" type="nonEmptyStringType" use="required">
			<xsd:annotation>
				<xsd:documentation>External status code as defined by the referenced source.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<xsd:element name="ExternalStatusCode" type="ExternalStatusCodeType">
		<xsd:annotation>
			<xsd:documentation>Status code from an external (non-SCTE 130) specification.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--IniatorData element -->
	<xsd:complexType name="InitiatorDataType">
		<xsd:simpleContent>
			<xsd:extension base="xsd:string">
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:simpleContent>
	</xsd:complexType>
	<xsd:element name="InitiatorData" type="InitiatorDataType">
		<xsd:annotation>
			<xsd:documentation>Private data returned (i.e., echoed back) by the message respondent.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Note element -->
	<xsd:complexType name="NoteType">
		<xsd:simpleContent>
			<xsd:extension base="nonEmptyStringType">
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:simpleContent>
	</xsd:complexType>
	<xsd:element name="Note" type="NoteType">
		<xsd:annotation>
			<xsd:documentation>Descriptive text.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Program identification element -->
	<xsd:complexType name="ProgramType">
		<xsd:simpleContent>
			<xsd:extension base="xsd:string">
				<xsd:attribute name="uniqueProgramID" type="xsd:nonNegativeInteger" use="optional">
					<xsd:annotation>
						<xsd:documentation>Unique program identifier.</xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
				<xsd:attribute name="referenceDateTime" type="dateTimeTimezoneType" use="optional">
					<xsd:annotation>
						<xsd:documentation>Reference date and time.</xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:simpleContent>
	</xsd:complexType>
	<xsd:element name="Program" type="ProgramType">
		<xsd:annotation>
			<xsd:documentation>Program name and/or identification.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--SCTE 35 Segmentation UPID element -->
	<xsd:complexType name="Scte35SegmentationUpidType">
		<xsd:simpleContent>
			<xsd:extension base="xsd:hexBinary">
				<xsd:attribute name="type" type="xsd:unsignedByte" use="required">
					<xsd:annotation>
						<xsd:documentation>A value from SCTE 35 Table 8-6 identifying the information format.</xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
				<xsd:attribute name="length" type="xsd:unsignedByte" use="optional">
					<xsd:annotation>
						<xsd:documentation>The information length in bytes. Limits may be applied as per SCTE 35 Table 8-6.</xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
				<xsd:attribute name="eventID" type="xsd:unsignedInt" use="optional">
					<xsd:annotation>
						<xsd:documentation>A 32-bit unique segmentation event identifier.</xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
				<xsd:attribute name="typeID" type="xsd:unsignedByte" use="optional">
					<xsd:annotation>
						<xsd:documentation>The segmentation type as specified by SCTE 35 Table 8-7.</xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
				<xsd:attribute name="segmentNum" type="xsd:unsignedByte" use="optional">
					<xsd:annotation>
						<xsd:documentation>Identification of a specific segment within a segmentation upid.</xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
				<xsd:attribute name="segmentsExpected" type="xsd:unsignedByte" use="optional">
					<xsd:annotation>
						<xsd:documentation>Expected number of individual segments within the segmentation upid.</xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
				<xsd:attribute name="referenceDateTime" type="dateTimeTimezoneType" use="optional">
					<xsd:annotation>
						<xsd:documentation>Contextual reference date and time.</xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:simpleContent>
	</xsd:complexType>
	<xsd:element name="SegmentationUpid" type="Scte35SegmentationUpidType">
		<xsd:annotation>
			<xsd:documentation>SCTE 35 segmentation descriptor construct. The value is the segmentation upid.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--SCTE 118 Spot element -->
	<xsd:complexType name="Scte118SpotType">
		<xsd:attribute name="trafficId" type="xsd:integer" use="required">
			<xsd:annotation>
				<xsd:documentation>Traffic ID generated by the traffic and billing system.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="spotId" type="nonEmptyStringType" use="required">
			<xsd:annotation>
				<xsd:documentation>Traffic and billing assigned spot identifier.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="advertiserName" type="nonEmptyStringType" use="optional">
			<xsd:annotation>
				<xsd:documentation>Operator/MSO entered value provided by the traffic and billing system.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="spotName" type="nonEmptyStringType" use="optional">
			<xsd:annotation>
				<xsd:documentation>Operator/MSO entered value provided by the traffic and billing system.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="spotType" type="nonEmptyStringType" use="optional">
			<xsd:annotation>
				<xsd:documentation>For SCTE 118, one of the following: SCHED=Scheduled, FILL=Fill, BONUS=Bonus.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="schedSource" type="nonEmptyStringType" use="optional">
			<xsd:annotation>
				<xsd:documentation>e.g., Interconnect, Local, National, Marketing, etc. Assigned by the originating traffic and billing system.
				</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="adId" type="nonEmptyStringType" use="optional">
			<xsd:annotation>
				<xsd:documentation>Advertising digital identifier.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<xsd:element name="SpotRef" type="Scte118SpotType">
		<xsd:annotation>
			<xsd:documentation>Typically, an SCTE 118 spot element.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Status code element -->
	<xsd:complexType name="StatusCodeType">
		<xsd:sequence>
			<xsd:element ref="Note" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element ref="ExternalStatusCode" minOccurs="0"/>
		</xsd:sequence>
		<xsd:attribute name="class" type="xsd:nonNegativeInteger" use="required">
			<xsd:annotation>
				<xsd:documentation>One of the following values:
					0 = Success
					1 = Error
					2 = Warning
					3 = Information
					4 = External status code
					... = User defined and outside the scope of this specification.
				</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="detail" type="xsd:nonNegativeInteger" use="optional">
			<xsd:annotation>
				<xsd:documentation>A SCTE 130 specific status code.</xsd:documentation>
				<xsd:documentation>See the specification for additional details.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<xsd:element name="StatusCode" type="StatusCodeType">
		<xsd:annotation>
			<xsd:documentation>SCTE 130 status code.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Tracking element -->
	<xsd:complexType name="TrackingType">
		<xsd:simpleContent>
			<xsd:extension base="xsd:string">
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:simpleContent>
	</xsd:complexType>
	<xsd:element name="Tracking" type="TrackingType">
		<xsd:annotation>
			<xsd:documentation>ADS specified identifier.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--URI element -->
	<xsd:complexType name="URIType">
		<xsd:simpleContent>
			<xsd:extension base="xsd:anyURI">
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:simpleContent>
	</xsd:complexType>
	<xsd:element name="URI" type="URIType">
		<xsd:annotation>
			<xsd:documentation>Uniform resource identifier.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!---->
	<!--***** End Basic Elements ***** -->
</xsd:schema>"""
            contentLength = len(response)
            self.server_version = "Python ADSSim"
            self.sys_version = ""
            self.send_response(200)
            self.send_header("X-Powered-By", "Servlet/2.5")
            self.send_header("Content-Type", 'text/xml;charset="utf-8"')
            self.send_header("Content-Length", contentLength)
            self.end_headers()
            self.wfile.write(response)
            print "\nSending " + self.path + " >>>>>>>>>>\n", response
            print "-----------------------------------------------\n"
###############################################################################
        elif self.path.find("?xsd=2") >= 0:
            response = """<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://www.scte.org/schemas/130-3/2008a/adm" xmlns:core="http://www.scte.org/schemas/130-2/2008a/core" targetNamespace="http://www.scte.org/schemas/130-3/2008a/adm" elementFormDefault="qualified" attributeFormDefault="unqualified">
	<xsd:import namespace="http://www.scte.org/schemas/130-2/2008a/core" schemaLocation="http://%s:%s/adrouter/130_playlist/2010/soap?xsd=1"/>
	<!-- -->
	<!--<<Start Placement Messaging>> -->
	<!--ADM to ADS Placement Request Message -->
	<xsd:complexType name="PlacementRequestType">
		<xsd:complexContent>
			<xsd:extension base="core:Msg_RequestBaseType">
				<xsd:sequence>
					<xsd:element ref="SystemContext" minOccurs="0">
						<xsd:annotation>
							<xsd:documentation>General system context and registration identification refinements specific to this PlacementRequest.
							</xsd:documentation>
						</xsd:annotation>
					</xsd:element>
					<xsd:element ref="Service" minOccurs="0" maxOccurs="unbounded">
						<xsd:annotation>
							<xsd:documentation>Placement service identification refinement details specific to this PlacementRequest.</xsd:documentation>
						</xsd:annotation>
					</xsd:element>
					<xsd:element ref="Entertainment" minOccurs="0"/>
					<xsd:element ref="Client" minOccurs="0"/>
					<xsd:element ref="ADMData" minOccurs="0">
						<xsd:annotation>
							<xsd:documentation>Private ADM data that shall be returned (echoed back) by the ADS in the PlacementResponse message.
							</xsd:documentation>
						</xsd:annotation>
					</xsd:element>
					<xsd:element ref="PlacementOpportunity" maxOccurs="unbounded"/>
					<xsd:element ref="core:Ext" minOccurs="0"/>
				</xsd:sequence>
				<xsd:attribute name="updatesAllowed" type="xsd:boolean" use="optional" default="false">
					<xsd:annotation>
						<xsd:documentation>The ADM allows/accepts updates for the PlacementRequest message.</xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:element name="PlacementRequest" type="PlacementRequestType">
		<xsd:annotation>
			<xsd:documentation>Placement request message element.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--ADS to ADM Placement Response Message -->
	<xsd:complexType name="PlacementResponseType">
		<xsd:complexContent>
			<xsd:extension base="core:Msg_ResponseBaseType">
				<xsd:sequence>
					<xsd:group ref="CommonPlacementDecisionMessageElements"/>
					<xsd:element ref="core:Ext" minOccurs="0"/>
				</xsd:sequence>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:element name="PlacementResponse" type="PlacementResponseType">
		<xsd:annotation>
			<xsd:documentation>Placement response message element.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--ADS to ADM Placement Update Notification Message -->
	<xsd:complexType name="PlacementUpdateNotificationType">
		<xsd:complexContent>
			<xsd:extension base="core:Msg_NotificationBaseType">
				<xsd:sequence>
					<xsd:element ref="core:StatusCode" minOccurs="0"/>
					<xsd:group ref="CommonPlacementDecisionMessageElements"/>
					<xsd:element ref="core:Ext" minOccurs="0"/>
				</xsd:sequence>
				<xsd:attribute name="messageRef" type="core:messageRefAttrType" use="required">
					<xsd:annotation>
						<xsd:documentation>Reference to the original PlacementRequest message.</xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
				<xsd:attribute name="updateAction" type="core:nonEmptyStringType" use="required">
					<xsd:annotation>
						<xsd:documentation>Message command. For example, "replace", "cancel, etc.</xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:element name="PlacementUpdateNotification" type="PlacementUpdateNotificationType">
		<xsd:annotation>
			<xsd:documentation>Placement update notification message element.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--ADM to ADS Placement Update Acknowledgement Message -->
	<xsd:complexType name="PlacementUpdateAcknowledgementType">
		<xsd:complexContent>
			<xsd:extension base="core:Msg_AcknowledgementBaseType">
				<xsd:sequence>
					<xsd:element ref="core:Ext" minOccurs="0"/>
				</xsd:sequence>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:element name="PlacementUpdateAcknowledgement" type="PlacementUpdateAcknowledgementType">
		<xsd:annotation>
			<xsd:documentation>Placement update acknowledgement message element.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--<<End Placement Messaging>> -->
	<!---->
	<!--<<Start Placement Status Messaging>> -->
	<!--ADM to any Placement Status Notification Message -->
	<xsd:complexType name="PlacementStatusNotificationType">
		<xsd:complexContent>
			<xsd:extension base="core:Msg_NotificationBaseType">
				<xsd:sequence>
					<xsd:element ref="PlayData" maxOccurs="unbounded"/>
					<xsd:element ref="core:Ext" minOccurs="0"/>
				</xsd:sequence>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:element name="PlacementStatusNotification" type="PlacementStatusNotificationType">
		<xsd:annotation>
			<xsd:documentation>Placement status notification message element.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Any to ADM Placement Status Acknowledgement Message -->
	<xsd:complexType name="PlacementStatusAcknowledgementType">
		<xsd:complexContent>
			<xsd:extension base="core:Msg_AcknowledgementBaseType">
				<xsd:sequence>
					<xsd:element ref="core:Ext" minOccurs="0"/>
				</xsd:sequence>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:element name="PlacementStatusAcknowledgement" type="PlacementStatusAcknowledgementType">
		<xsd:annotation>
			<xsd:documentation>Placement status acknoweldgement message element.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--<<End Placement Status Messaging>> -->
	<!---->
	<!--<<Start Service Channel Management>> -->
	<!--List ADM Offered Services Request Message -->
	<xsd:complexType name="ListADMServicesRequestType">
		<xsd:complexContent>
			<xsd:extension base="core:Msg_RequestBaseType">
				<xsd:sequence>
					<xsd:element ref="core:Ext" minOccurs="0"/>
				</xsd:sequence>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:element name="ListADMServicesRequest" type="ListADMServicesRequestType">
		<xsd:annotation>
			<xsd:documentation>List ADM offered placement services request message element.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--List ADM Offered Services Response Message -->
	<xsd:complexType name="ListADMServicesResponseType">
		<xsd:complexContent>
			<xsd:extension base="core:Msg_ResponseBaseType">
				<xsd:sequence>
					<xsd:element ref="ADMCapabilities" minOccurs="0"/>
					<xsd:element ref="ServiceDescription" minOccurs="0" maxOccurs="unbounded">
						<xsd:annotation>
							<xsd:documentation>List of placement service descriptions defined by a system context and a list of services within that
								context for which placement decisions may be provided.
							</xsd:documentation>
						</xsd:annotation>
					</xsd:element>
					<xsd:element ref="core:Callout" minOccurs="0" maxOccurs="unbounded"/>
					<xsd:element ref="core:Ext" minOccurs="0"/>
				</xsd:sequence>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:element name="ListADMServicesResponse" type="ListADMServicesResponseType">
		<xsd:annotation>
			<xsd:documentation>List ADM offered placement services response message element.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--List active ADS Registrations Request Message -->
	<xsd:complexType name="ListADSRegistrationRequestType">
		<xsd:complexContent>
			<xsd:extension base="core:Msg_RequestBaseType">
				<xsd:sequence>
					<xsd:element ref="core:Ext" minOccurs="0"/>
				</xsd:sequence>
				<xsd:attribute name="registrationRef" type="core:registrationRefAttrType" use="optional">
					<xsd:annotation>
						<xsd:documentation>Reference to a previous, original ADSRegistrationRequest message. If this attribute is ommitted then all
							registrations may be returned.
						</xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:element name="ListADSRegistrationRequest" type="ListADSRegistrationRequestType">
		<xsd:annotation>
			<xsd:documentation>Request active ADS registration list message element.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--List Acitve ADS Registrations Response Message -->
	<xsd:complexType name="ListADSRegistrationResponseType">
		<xsd:complexContent>
			<xsd:extension base="core:Msg_ResponseBaseType">
				<xsd:sequence>
					<xsd:element ref="ADSRegistrationRequest" minOccurs="0" maxOccurs="unbounded">
						<xsd:annotation>
							<xsd:documentation>List of active ADS registrations.</xsd:documentation>
						</xsd:annotation>
					</xsd:element>
					<xsd:element ref="core:Ext" minOccurs="0"/>
				</xsd:sequence>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:element name="ListADSRegistrationResponse" type="ListADSRegistrationResponseType">
		<xsd:annotation>
			<xsd:documentation>Response containing active ADS registration list message element.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--ADS to ADM Registration Request Message -->
	<xsd:complexType name="ADSRegistrationRequestType">
		<xsd:complexContent>
			<xsd:extension base="core:Msg_RequestBaseType">
				<xsd:sequence>
					<xsd:element ref="ServiceDescription" maxOccurs="unbounded">
						<xsd:annotation>
							<xsd:documentation>List of placement services (via ServiceDescriptions) for which the ADS desires to provide placement
								decisions.
							</xsd:documentation>
						</xsd:annotation>
					</xsd:element>
					<xsd:element ref="core:Callout" maxOccurs="unbounded"/>
					<xsd:element ref="core:Ext" minOccurs="0"/>
				</xsd:sequence>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:element name="ADSRegistrationRequest" type="ADSRegistrationRequestType">
		<xsd:annotation>
			<xsd:documentation>ADS registration request message element.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--ADM to ADS Registration Response Message -->
	<xsd:complexType name="ADSRegistrationResponseType">
		<xsd:complexContent>
			<xsd:extension base="core:Msg_ResponseBaseType">
				<xsd:sequence>
					<xsd:element ref="core:Callout" minOccurs="0" maxOccurs="unbounded"/>
					<xsd:element ref="core:Ext" minOccurs="0"/>
				</xsd:sequence>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:element name="ADSRegistrationResponse" type="ADSRegistrationResponseType">
		<xsd:annotation>
			<xsd:documentation>Registration response message element.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--ADS to ADM Deregister Request Message -->
	<xsd:complexType name="ADSDeregisterRequestType">
		<xsd:complexContent>
			<xsd:extension base="core:Msg_RequestBaseType">
				<xsd:sequence>
					<xsd:element ref="core:Ext" minOccurs="0"/>
				</xsd:sequence>
				<xsd:attribute name="registrationRef" type="core:registrationRefAttrType" use="optional">
					<xsd:annotation>
						<xsd:documentation>Reference to a previous ADSRegistrationRequest message.</xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:element name="ADSDeregisterRequest" type="ADSDeregisterRequestType">
		<xsd:annotation>
			<xsd:documentation>ADS deregister request message element.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--ADM to ADS Deregister Response Message -->
	<xsd:complexType name="ADSDeregisterResponseType">
		<xsd:complexContent>
			<xsd:extension base="core:Msg_ResponseBaseType">
				<xsd:sequence>
					<xsd:element ref="core:Ext" minOccurs="0"/>
				</xsd:sequence>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:element name="ADSDeregisterResponse" type="ADSDeregisterResponseType">
		<xsd:annotation>
			<xsd:documentation>Deregister response message element.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--ADM to ADS Deregistration Notification Message -->
	<xsd:complexType name="ADSDeregistrationNotificationType">
		<xsd:complexContent>
			<xsd:extension base="core:Msg_NotificationBaseType">
				<xsd:sequence>
					<xsd:element ref="core:StatusCode"/>
					<xsd:element ref="core:Ext" minOccurs="0"/>
				</xsd:sequence>
				<xsd:attribute name="registrationRef" type="core:registrationRefAttrType" use="optional">
					<xsd:annotation>
						<xsd:documentation>Reference to a previously accepted ADSRegistrationRequest message.</xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:element name="ADSDeregistrationNotification" type="ADSDeregistrationNotificationType">
		<xsd:annotation>
			<xsd:documentation>Deregistration notification message element.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--ADS Deregistration Acknowledgement Message -->
	<xsd:complexType name="ADSDeregistrationAcknowledgementType">
		<xsd:complexContent>
			<xsd:extension base="core:Msg_AcknowledgementBaseType">
				<xsd:sequence>
					<xsd:element ref="core:Ext" minOccurs="0"/>
				</xsd:sequence>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:element name="ADSDeregistrationAcknowledgement" type="ADSDeregistrationAcknowledgementType">
		<xsd:annotation>
			<xsd:documentation>Deregistration acknoweledgement message element.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!---->
	<!---->
	<!--Part 3 global attribute types -->
	<xsd:simpleType name="eventRangeEndDateTimeAttrType">
		<xsd:annotation>
			<xsd:documentation>End date and time for a group of events.</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="core:dateTimeTimezoneType"/>
	</xsd:simpleType>
	<xsd:simpleType name="eventRangeStartDateTimeAttrType">
		<xsd:annotation>
			<xsd:documentation>Start date and time for a group of events.</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="core:dateTimeTimezoneType"/>
	</xsd:simpleType>
	<xsd:simpleType name="eventTypeAttrType">
		<xsd:annotation>
			<xsd:documentation>Event type name identifier.</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="core:nonEmptyStringType"/>
	</xsd:simpleType>
	<xsd:simpleType name="localServiceRefAttrType">
		<xsd:annotation>
			<xsd:documentation>Placement messages local service element cross-reference linkage identifier.</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="core:idAttrType"/>
	</xsd:simpleType>
	<xsd:simpleType name="placementActionAttrType">
		<xsd:annotation>
			<xsd:documentation>Placement action identifier.</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="core:nonEmptyStringType"/>
	</xsd:simpleType>
	<xsd:simpleType name="placementOpportunitiesExpectedAttrType">
		<xsd:annotation>
			<xsd:documentation>Placement opportunities expected count relative to the content.</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="xsd:nonNegativeInteger"/>
	</xsd:simpleType>
	<xsd:simpleType name="placementOpportunityNumberAttrType">
		<xsd:annotation>
			<xsd:documentation>Placement opportunity number/identifier relative to the content.</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="xsd:nonNegativeInteger"/>
	</xsd:simpleType>
	<xsd:simpleType name="placementOpportunityTypeAttrType">
		<xsd:annotation>
			<xsd:documentation>Placement opportunity type identifier.</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="core:nonEmptyStringType"/>
	</xsd:simpleType>
	<xsd:simpleType name="positionAttrType">
		<xsd:annotation>
			<xsd:documentation>Subdivision position identifier.</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="xsd:nonNegativeInteger">
			<xsd:minInclusive value="1"/>
		</xsd:restriction>
	</xsd:simpleType>
	<!--Part 3 global attribute group types -->
	<xsd:attributeGroup name="eventBaseAttrsType">
		<xsd:annotation>
			<xsd:documentation>Common attributes required for all events.</xsd:documentation>
		</xsd:annotation>
		<xsd:attribute name="type" type="eventTypeAttrType" use="required">
			<xsd:annotation>
				<xsd:documentation>Event type identifier.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="time" type="core:dateTimeTimezoneType" use="required">
			<xsd:annotation>
				<xsd:documentation>Time event occurred.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="identityADM" type="core:identityAttrType" use="optional">
			<xsd:annotation>
				<xsd:documentation>Origin identifier.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="systemADM" type="core:systemAttrType" use="optional">
			<xsd:annotation>
				<xsd:documentation>Origin identifier.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="identityADS" type="core:identityAttrType" use="optional">
			<xsd:annotation>
				<xsd:documentation>The ADS origin/target logical service identifier.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="messageRef" type="core:messageRefAttrType" use="optional">
			<xsd:annotation>
				<xsd:documentation>Message reference.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
	</xsd:attributeGroup>
	<xsd:attributeGroup name="eventRangeDateTimeAttrsType">
		<xsd:annotation>
			<xsd:documentation>Start and end date time for the events contained within this element.</xsd:documentation>
		</xsd:annotation>
		<xsd:attribute name="eventRangeStartDateTime" type="eventRangeStartDateTimeAttrType" use="optional">
			<xsd:annotation>
				<xsd:documentation>A start date and time for all event elements contained within this element.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="eventRangeEndDateTime" type="eventRangeEndDateTimeAttrType" use="optional">
			<xsd:annotation>
				<xsd:documentation>A end date and time for all event elements contained within this element.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
	</xsd:attributeGroup>
	<xsd:attributeGroup name="opportunityBindingBaseAttrType">
		<xsd:annotation>
			<xsd:documentation>Common attributes required by all placement opportunity binding types.</xsd:documentation>
		</xsd:annotation>
		<xsd:attribute name="opportunityType" type="placementOpportunityTypeAttrType" use="optional">
			<xsd:annotation>
				<xsd:documentation>Placement opportunity type identification. Examples are: pre-roll, post-roll, interstitial, pause, etc.
				</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="opportunityNumber" type="placementOpportunityNumberAttrType" use="optional">
			<xsd:annotation>
				<xsd:documentation>Placement opportunity identification relative/within the content.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="opportunitiesExpected" type="placementOpportunitiesExpectedAttrType" use="optional">
			<xsd:annotation>
				<xsd:documentation>Total expected placement opportunity count.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
	</xsd:attributeGroup>
	<!---->
	<!--Part 3 global base types -->
	<xsd:complexType name="EventBaseType">
		<xsd:annotation>
			<xsd:documentation>Event elements base type.</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element ref="core:StatusCode" minOccurs="0"/>
			<xsd:element ref="SystemContext" minOccurs="0"/>
			<xsd:element ref="Client" minOccurs="0"/>
			<xsd:choice minOccurs="0">
				<xsd:element ref="Entertainment"/>
				<xsd:element ref="EntertainmentNPT"/>
			</xsd:choice>
			<xsd:choice minOccurs="0">
				<xsd:element ref="Spot"/>
				<xsd:element ref="SpotNPT"/>
			</xsd:choice>
			<xsd:element ref="NPT" minOccurs="0"/>
			<xsd:element ref="core:Ext" minOccurs="0"/>
		</xsd:sequence>
		<xsd:attributeGroup ref="eventBaseAttrsType"/>
	</xsd:complexType>
	<xsd:complexType name="NormalPlayTimeType">
		<xsd:annotation>
			<xsd:documentation>Normal play time (NPT) element base type.</xsd:documentation>
		</xsd:annotation>
		<xsd:simpleContent>
			<xsd:extension base="core:nonEmptyStringType">
				<xsd:attribute name="scale" type="xsd:decimal">
					<xsd:annotation>
						<xsd:documentation>Positive or negative decimal value (e.g. -2.0 for 2x rewind, etc.).</xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:simpleContent>
	</xsd:complexType>
	<xsd:complexType name="PlacementCountType">
		<xsd:annotation>
			<xsd:documentation>Placement count base type.</xsd:documentation>
		</xsd:annotation>
		<xsd:simpleContent>
			<xsd:extension base="xsd:nonNegativeInteger">
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:simpleContent>
	</xsd:complexType>
	<!---->
	<!--Part 3 global element groups -->
	<xsd:group name="CommonPlacementDecisionMessageElements">
		<xsd:annotation>
			<xsd:documentation>Common elements in PlacementResponse and PlacementUpdateNotification.</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element ref="SystemContext" minOccurs="0"/>
			<xsd:element ref="Service" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element ref="Entertainment" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>Reference to the programming/entertainment content.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element ref="Client" minOccurs="0"/>
			<xsd:element ref="ADMData" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>Copy of the element if received in the PlacementRequest message.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element ref="PlacementDecision" minOccurs="0" maxOccurs="unbounded"/>
		</xsd:sequence>
	</xsd:group>
	<!---->
	<!--############### -->
	<!--#Part 3 Elements# -->
	<!--############### -->
	<!--ADM Capabilities element -->
	<xsd:complexType name="ADMCapabilitiesType">
		<xsd:sequence>
			<xsd:element ref="core:Ext" minOccurs="0"/>
		</xsd:sequence>
		<xsd:attribute name="placementStatusOnlyRegistration" type="xsd:boolean" use="optional" default="false">
			<xsd:annotation>
				<xsd:documentation>PlacementStatusNotification only registration is supported. Omission indicates PlacementStatusNotification
					only registration is not supported.
				</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<xsd:element name="ADMCapabilities" type="ADMCapabilitiesType">
		<xsd:annotation>
			<xsd:documentation>ADM supported options.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--ADMData element -->
	<xsd:complexType name="ADMDataType">
		<xsd:simpleContent>
			<xsd:extension base="xsd:string">
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:simpleContent>
	</xsd:complexType>
	<xsd:element name="ADMData" type="ADMDataType">
		<xsd:annotation>
			<xsd:documentation>Data that must be returned if sent.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Channel element -->
	<xsd:complexType name="ChannelType">
		<xsd:simpleContent>
			<xsd:extension base="core:nonEmptyStringType">
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:simpleContent>
	</xsd:complexType>
	<xsd:element name="Channel" type="ChannelType">
		<xsd:annotation>
			<xsd:documentation>Channel identification.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Client element -->
	<xsd:complexType name="ClientType">
		<xsd:sequence>
			<xsd:element ref="core:CurrentDateTime" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>Current client time.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element ref="TerminalAddress" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>Client endpoint identification.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element ref="TargetCode" minOccurs="0" maxOccurs="unbounded">
				<xsd:annotation>
					<xsd:documentation>List of addressable (targeting) criteria.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element ref="core:Ext" minOccurs="0"/>
		</xsd:sequence>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<xsd:element name="Client" type="ClientType">
		<xsd:annotation>
			<xsd:documentation>Container for client addressability/targeting specifics and refinements.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Provider element -->
	<xsd:complexType name="ContentProviderType">
		<xsd:sequence>
			<xsd:element ref="core:ContentDataModel" minOccurs="0"/>
		</xsd:sequence>
		<xsd:attribute name="providerID" type="core:nonEmptyStringType" use="required">
			<xsd:annotation>
				<xsd:documentation>Content provider name/identification.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<xsd:element name="ContentProvider" type="ContentProviderType">
		<xsd:annotation>
			<xsd:documentation>Content provider name and/or identification.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Content Rotation List element -->
	<xsd:complexType name="ContentRotationListType">
		<xsd:sequence>
			<xsd:element ref="core:Content" minOccurs="2" maxOccurs="unbounded"/>
		</xsd:sequence>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<xsd:element name="ContentRotationList" type="ContentRotationListType">
		<xsd:annotation>
			<xsd:documentation>Container for ad spot list used in a placement rotation.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Desired/Ideal elements -->
	<xsd:element name="DesiredDuration" type="core:DurationType">
		<xsd:annotation>
			<xsd:documentation>Desired (ideal) run-time length.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:element name="DesiredPlacementCount" type="PlacementCountType">
		<xsd:annotation>
			<xsd:documentation>Desired (ideal) placement count.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Effective start and end date/time elements -->
	<xsd:element name="EffectiveEndDateTime">
		<xsd:annotation>
			<xsd:documentation>Expiration date and time when the container becomes inactive. Ommision implies never. </xsd:documentation>
		</xsd:annotation>
		<xsd:complexType>
			<xsd:simpleContent>
				<xsd:extension base="core:dateTimeTimezoneType">
					<xsd:anyAttribute namespace="##any" processContents="lax"/>
				</xsd:extension>
			</xsd:simpleContent>
		</xsd:complexType>
	</xsd:element>
	<xsd:element name="EffectiveStartDateTime">
		<xsd:annotation>
			<xsd:documentation>Commencement date and time when the container becomes active. Ommision implies immediate. </xsd:documentation>
		</xsd:annotation>
		<xsd:complexType>
			<xsd:simpleContent>
				<xsd:extension base="core:dateTimeTimezoneType">
					<xsd:anyAttribute namespace="##any" processContents="lax"/>
				</xsd:extension>
			</xsd:simpleContent>
		</xsd:complexType>
	</xsd:element>
	<!--Entertainment element -->
	<xsd:complexType name="EntertainmentType">
		<xsd:sequence>
			<xsd:element ref="core:Content"/>
			<xsd:element ref="EntertainmentNPT" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>NPT specific to the core:Content element above.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element ref="core:Ext" minOccurs="0"/>
		</xsd:sequence>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<xsd:element name="Entertainment" type="EntertainmentType">
		<xsd:annotation>
			<xsd:documentation>Entertainment content identification.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--EntertainmentNPT element -->
	<xsd:element name="EntertainmentNPT" type="NormalPlayTimeType">
		<xsd:annotation>
			<xsd:documentation>NPT scoped by (relative to) an Entertainment element.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--EntertainmentScopedEvents element -->
	<xsd:complexType name="EntertainmentScopedEventsType">
		<xsd:sequence>
			<xsd:annotation>
				<xsd:documentation>The Entertaiment element applies to all events in the choice sequence. The Entertainment element shall not
					appear in any child named event element.
				</xsd:documentation>
			</xsd:annotation>
			<xsd:element ref="Entertainment"/>
			<xsd:choice maxOccurs="unbounded">
				<xsd:element ref="Events"/>
				<xsd:element ref="SpotScopedEvents"/>
			</xsd:choice>
		</xsd:sequence>
		<xsd:attributeGroup ref="eventRangeDateTimeAttrsType"/>
		<xsd:anyAttribute namespace="##any"/>
	</xsd:complexType>
	<xsd:element name="EntertainmentScopedEvents" type="EntertainmentScopedEventsType">
		<xsd:annotation>
			<xsd:documentation>Events all relative to the static Entertainment element declaration.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Events element -->
	<xsd:complexType name="EventsType">
		<xsd:choice maxOccurs="unbounded">
			<xsd:element ref="PlacementStatusEvent"/>
			<xsd:element ref="SessionEvent"/>
			<xsd:element ref="SystemEvent"/>
			<xsd:element ref="ViewerEvent"/>
			<xsd:element ref="core:Ext"/>
		</xsd:choice>
		<xsd:attributeGroup ref="eventRangeDateTimeAttrsType"/>
		<xsd:anyAttribute namespace="##any"/>
	</xsd:complexType>
	<xsd:element name="Events" type="EventsType">
		<xsd:annotation>
			<xsd:documentation>Container for named event element sequence.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Linear Avail Binding element -->
	<xsd:complexType name="LinearAvailBindingType">
		<xsd:sequence>
			<xsd:element ref="ProviderAvailID" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element ref="Window" minOccurs="0"/>
			<xsd:element ref="core:Ext" minOccurs="0"/>
		</xsd:sequence>
		<xsd:attributeGroup ref="opportunityBindingBaseAttrType"/>
		<xsd:attribute name="spliceEventID" type="xsd:nonNegativeInteger" use="optional">
			<xsd:annotation>
				<xsd:documentation>Spice event identifier.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<xsd:element name="LinearAvailBinding" type="LinearAvailBindingType">
		<xsd:annotation>
			<xsd:documentation>Linear avail placement opportunity specific information.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Max/Min types -->
	<xsd:element name="MaxDuration" type="core:DurationType">
		<xsd:annotation>
			<xsd:documentation>Maximum run-time length.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:element name="MaxPlacementCount" type="PlacementCountType">
		<xsd:annotation>
			<xsd:documentation>Maximum placement count.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:element name="MinDuration" type="core:DurationType">
		<xsd:annotation>
			<xsd:documentation>Minimum run-time length.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:element name="MinPlacementCount" type="PlacementCountType">
		<xsd:annotation>
			<xsd:documentation>Minimum placement count.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Network element -->
	<xsd:complexType name="NetworkType">
		<xsd:simpleContent>
			<xsd:extension base="core:nonEmptyStringType">
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:simpleContent>
	</xsd:complexType>
	<xsd:element name="Network" type="NetworkType">
		<xsd:annotation>
			<xsd:documentation>Network identification.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Normal Play Time element -->
	<xsd:element name="NPT" type="NormalPlayTimeType">
		<xsd:annotation>
			<xsd:documentation>Normal play time.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Opportunity Binding element -->
	<xsd:complexType name="OpportunityBindingType">
		<xsd:sequence>
			<xsd:element ref="core:Ext" minOccurs="0"/>
		</xsd:sequence>
		<xsd:attributeGroup ref="opportunityBindingBaseAttrType"/>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<xsd:element name="OpportunityBinding" type="OpportunityBindingType">
		<xsd:annotation>
			<xsd:documentation>Generic opportunity information bridge.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Opportunity Constraints element -->
	<xsd:complexType name="OpportunityConstraintsType">
		<xsd:sequence>
			<xsd:element ref="core:AdType" minOccurs="0" maxOccurs="unbounded">
				<xsd:annotation>
					<xsd:documentation>Ad type fulfillment advisory.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:choice minOccurs="0">
				<xsd:sequence>
					<xsd:element ref="MaxDuration" minOccurs="0">
						<xsd:annotation>
							<xsd:documentation>Maximum duration for the sum of related returned placements.</xsd:documentation>
						</xsd:annotation>
					</xsd:element>
					<xsd:element ref="MinDuration" minOccurs="0">
						<xsd:annotation>
							<xsd:documentation>Minimum duration for the sum of related returned placements.</xsd:documentation>
						</xsd:annotation>
					</xsd:element>
					<xsd:element ref="DesiredDuration" minOccurs="0">
						<xsd:annotation>
							<xsd:documentation>Desired (ideal) duration for the sum of related returned placements.</xsd:documentation>
						</xsd:annotation>
					</xsd:element>
				</xsd:sequence>
				<xsd:element ref="core:Duration">
					<xsd:annotation>
						<xsd:documentation>Required duration for the sum of related returned placements.</xsd:documentation>
					</xsd:annotation>
				</xsd:element>
			</xsd:choice>
			<xsd:choice minOccurs="0">
				<xsd:sequence>
					<xsd:element ref="MaxPlacementCount" minOccurs="0">
						<xsd:annotation>
							<xsd:documentation>Maximum placement count for the sum of related returned placements.</xsd:documentation>
						</xsd:annotation>
					</xsd:element>
					<xsd:element ref="MinPlacementCount" minOccurs="0">
						<xsd:annotation>
							<xsd:documentation>Minimum placement count for the sum of related returned placements.</xsd:documentation>
						</xsd:annotation>
					</xsd:element>
					<xsd:element ref="DesiredPlacementCount" minOccurs="0">
						<xsd:annotation>
							<xsd:documentation>Desired (ideal) placement count for the sum of related returned placements.</xsd:documentation>
						</xsd:annotation>
					</xsd:element>
				</xsd:sequence>
				<xsd:element ref="PlacementCount">
					<xsd:annotation>
						<xsd:documentation>Required specific placement count for the sum of related returned placements.</xsd:documentation>
					</xsd:annotation>
				</xsd:element>
			</xsd:choice>
			<xsd:element ref="EffectiveStartDateTime" minOccurs="0"/>
			<xsd:element ref="EffectiveEndDateTime" minOccurs="0"/>
			<xsd:element ref="Scope" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element ref="core:Ext" minOccurs="0"/>
		</xsd:sequence>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<xsd:element name="OpportunityConstraints" type="OpportunityConstraintsType">
		<xsd:annotation>
			<xsd:documentation>Placement opportunity constraints via a combination of guidance and/or requirements.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Placement element -->
	<xsd:complexType name="PlacementType">
		<xsd:sequence>
			<xsd:element ref="core:Tracking" minOccurs="0"/>
			<xsd:choice minOccurs="0">
				<xsd:element ref="core:Content">
					<xsd:annotation>
						<xsd:documentation>Content specifier.</xsd:documentation>
					</xsd:annotation>
				</xsd:element>
				<xsd:element ref="ContentRotationList">
					<xsd:annotation>
						<xsd:documentation>Container for content list used in a placement rotation.</xsd:documentation>
					</xsd:annotation>
				</xsd:element>
			</xsd:choice>
			<xsd:element ref="PlacementConstraints" minOccurs="0"/>
			<xsd:element ref="core:Ext" minOccurs="0"/>
		</xsd:sequence>
		<xsd:attribute name="id" type="core:idAttrType" use="required">
			<xsd:annotation>
				<xsd:documentation>Unique identifier.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="action" type="placementActionAttrType" use="required">
			<xsd:annotation>
				<xsd:documentation>Specified placement behavior.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="placementControlRef" type="core:idAttrType" use="optional">
			<xsd:annotation>
				<xsd:documentation>Reference to a PlacementControl element.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="position" type="positionAttrType" use="optional">
			<xsd:annotation>
				<xsd:documentation>Placement position within this placement decision. Zero indicates unspecified.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<xsd:element name="Placement" type="PlacementType">
		<xsd:annotation>
			<xsd:documentation>Describes an individual content placement decision including an applicible content binding.
			</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Placement Constraints element -->
	<xsd:complexType name="PlacementConstraintsType">
		<xsd:sequence>
			<xsd:element ref="core:Duration" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>Specific run-time length for the content decision. The placement engine shall pad or cut to meet this exact
						time.
					</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element ref="EffectiveStartDateTime" minOccurs="0"/>
			<xsd:element ref="EffectiveEndDateTime" minOccurs="0"/>
			<xsd:element ref="Scope" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element ref="core:Ext" minOccurs="0"/>
		</xsd:sequence>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<xsd:element name="PlacementConstraints" type="PlacementConstraintsType">
		<xsd:annotation>
			<xsd:documentation>Placement constraints via a combination of guidance and/or requirements.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Placement Control element -->
	<xsd:complexType name="PlacementControlType">
		<xsd:sequence>
			<xsd:element ref="core:Content" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>Details about current ad content, if known.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element ref="OpportunityConstraints" minOccurs="0"/>
			<xsd:element ref="PlacementDateTime" minOccurs="0"/>
			<xsd:element ref="core:Ext" minOccurs="0"/>
		</xsd:sequence>
		<xsd:attribute name="id" type="core:idAttrType" use="required">
			<xsd:annotation>
				<xsd:documentation>Unique identifier.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="action" type="placementActionAttrType" use="optional">
			<xsd:annotation>
				<xsd:documentation>Planned/recommended action.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="position" type="positionAttrType" use="optional">
			<xsd:annotation>
				<xsd:documentation>Position within the placement opportunity. Zero indicates unknown.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<xsd:element name="PlacementControl" type="PlacementControlType">
		<xsd:annotation>
			<xsd:documentation>Placement opportunity subdivision. Individual constraints via a combination of guidance and/or requirements.
			</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Placement Count element -->
	<xsd:element name="PlacementCount" type="PlacementCountType">
		<xsd:annotation>
			<xsd:documentation>Specific placement count.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Placement Date Time element -->
	<xsd:complexType name="PlacementDateTimeType">
		<xsd:simpleContent>
			<xsd:extension base="core:dateTimeTimezoneType">
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:simpleContent>
	</xsd:complexType>
	<xsd:element name="PlacementDateTime" type="PlacementDateTimeType">
		<xsd:annotation>
			<xsd:documentation>Anticipated/calculated opportunity start date and time based on the entertainment content and the client time.
			</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Placement Decision element -->
	<xsd:complexType name="PlacementDecisionType">
		<xsd:sequence>
			<xsd:annotation>
				<xsd:documentation>The PlacementConstraints element applies to the entire PlacementDecision which may be subdivided via the list
					of Placement elements. Individual placements may be further controlled by their own local PlacementConstraints element.
				</xsd:documentation>
			</xsd:annotation>
			<xsd:element ref="Entertainment" minOccurs="0"/>
			<xsd:choice minOccurs="0">
				<xsd:element ref="OpportunityBinding"/>
				<xsd:element ref="LinearAvailBinding"/>
			</xsd:choice>
			<xsd:element ref="PlacementConstraints" minOccurs="0"/>
			<xsd:element ref="Placement" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element ref="core:Ext" minOccurs="0"/>
		</xsd:sequence>
		<xsd:attribute name="id" type="core:idAttrType" use="required">
			<xsd:annotation>
				<xsd:documentation>Unique identifier.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="placementOpportunityRef" type="core:idAttrType" use="required">
			<xsd:annotation>
				<xsd:documentation>Reference associating this PlacementDecision with a PlacementOpportunity element.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="localServiceRef" type="localServiceRefAttrType" use="optional">
			<xsd:annotation>
				<xsd:documentation>Reference to a Service element contained locally in the PlacementResponse message.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<xsd:element name="PlacementDecision" type="PlacementDecisionType">
		<xsd:annotation>
			<xsd:documentation>Individual placement decision description groups.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--PlacmentDecisionOwner element -->
	<xsd:complexType name="PlacementDecisionOwnerType">
		<xsd:simpleContent>
			<xsd:extension base="core:nonEmptyStringType">
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:simpleContent>
	</xsd:complexType>
	<xsd:element name="PlacementDecisionOwner" type="PlacementDecisionOwnerType">
		<xsd:annotation>
			<xsd:documentation>Placement decision owner identifier.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Placement Opportunity element -->
	<xsd:complexType name="PlacementOpportunityType">
		<xsd:sequence>
			<xsd:annotation>
				<xsd:documentation>The OpportunityConstraints element applies to the entire PlacementOpportunity which may be subdivided via the
					list of PlacementControl elements. Individual PlacementControl elements may further specify their own local
					OpportunityConstraints element which in turn has a more limited and specific scope.
				</xsd:documentation>
			</xsd:annotation>
			<xsd:element ref="Entertainment" minOccurs="0"/>
			<xsd:choice minOccurs="0">
				<xsd:element ref="OpportunityBinding"/>
				<xsd:element ref="LinearAvailBinding"/>
			</xsd:choice>
			<xsd:element ref="OpportunityConstraints" minOccurs="0"/>
			<xsd:element ref="PlacementDateTime" minOccurs="0"/>
			<xsd:element ref="PlacementControl" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element ref="core:Ext" minOccurs="0"/>
		</xsd:sequence>
		<xsd:attribute name="id" type="core:idAttrType" use="required">
			<xsd:annotation>
				<xsd:documentation>Unique identifier.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="serviceRegistrationRef" type="core:idAttrType" use="required">
			<xsd:annotation>
				<xsd:documentation>Reference to a specific service registration provided via the ADSRegistrationReguest message's Service
					element id attribute.
				</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="localServiceRef" type="localServiceRefAttrType" use="optional">
			<xsd:annotation>
				<xsd:documentation>Reference to a Service element contained locally in the PlacementRequest message.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<xsd:element name="PlacementOpportunity" type="PlacementOpportunityType">
		<xsd:annotation>
			<xsd:documentation>Individual placement opportunity descriptions.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Placement Status Event element -->
	<xsd:complexType name="PlacementStatusEventType">
		<xsd:complexContent>
			<xsd:extension base="EventBaseType">
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:element name="PlacementStatusEvent" type="PlacementStatusEventType">
		<xsd:annotation>
			<xsd:documentation>Placement specific event.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--PlayData element -->
	<xsd:complexType name="PlayDataType">
		<xsd:sequence>
			<xsd:sequence minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>These elements apply to all events below. If the element appears here then it should not appear in any named
						event anywhere below.
					</xsd:documentation>
				</xsd:annotation>
				<xsd:element ref="SystemContext" minOccurs="0"/>
				<xsd:element ref="Client" minOccurs="0"/>
			</xsd:sequence>
			<xsd:choice minOccurs="0" maxOccurs="unbounded">
				<xsd:annotation>
					<xsd:documentation>Information reduction by using static entertainment asset and/or static spot event groupings.
					</xsd:documentation>
				</xsd:annotation>
				<xsd:element ref="EntertainmentScopedEvents"/>
				<xsd:element ref="SpotScopedEvents">
					<xsd:annotation>
						<xsd:documentation>Events all relative to the static Spot element declaration.</xsd:documentation>
					</xsd:annotation>
				</xsd:element>
			</xsd:choice>
			<xsd:element ref="Events" minOccurs="0"/>
			<xsd:element ref="core:Ext" minOccurs="0"/>
		</xsd:sequence>
		<xsd:attribute name="identityADM" type="core:identityAttrType" use="optional">
			<xsd:annotation>
				<xsd:documentation>ADM identity origin identifier for all events contained within this element.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="systemADM" type="core:systemAttrType" use="optional">
			<xsd:annotation>
				<xsd:documentation>Origin identifier for all events contained within this element.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="identityADS" type="core:identityAttrType" use="optional">
			<xsd:annotation>
				<xsd:documentation>ADS identity origin identifier for all events contained within this element.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attributeGroup ref="eventRangeDateTimeAttrsType"/>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<xsd:element name="PlayData" type="PlayDataType">
		<xsd:annotation>
			<xsd:documentation>Container for event element groups.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Product Type element -->
	<xsd:complexType name="ProductTypeType">
		<xsd:simpleContent>
			<xsd:extension base="core:nonEmptyStringType">
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:simpleContent>
	</xsd:complexType>
	<xsd:element name="ProductType" type="ProductTypeType">
		<xsd:annotation>
			<xsd:documentation>Product identification. Examples: MOD, VOD, FOD, etc.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Provider Avail ID element -->
	<xsd:complexType name="ProviderAvailIDType">
		<xsd:simpleContent>
			<xsd:extension base="xsd:nonNegativeInteger">
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:simpleContent>
	</xsd:complexType>
	<xsd:element name="ProviderAvailID" type="ProviderAvailIDType">
		<xsd:annotation>
			<xsd:documentation>Provider avail identifier.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Scope element -->
	<xsd:complexType name="ScopeType">
		<xsd:simpleContent>
			<xsd:extension base="core:nonEmptyStringType">
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:simpleContent>
	</xsd:complexType>
	<xsd:element name="Scope" type="ScopeType">
		<xsd:annotation>
			<xsd:documentation>Scope fullfillment advisory. For example: National, Regional, Local, etc.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Service element -->
	<xsd:complexType name="ServiceType">
		<xsd:sequence>
			<xsd:element ref="core:Program" minOccurs="0"/>
			<xsd:element ref="core:AdType" minOccurs="0"/>
			<xsd:element ref="ContentProvider" minOccurs="0"/>
			<xsd:element ref="ProductType" minOccurs="0"/>
			<xsd:element ref="PlacementDecisionOwner" minOccurs="0"/>
			<xsd:element ref="Scope" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>Service scope.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element ref="core:Ext" minOccurs="0"/>
		</xsd:sequence>
		<xsd:attribute name="id" type="core:idAttrType" use="required">
			<xsd:annotation>
				<xsd:documentation>Service identifier.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="decisionPolicy" type="core:nonEmptyStringType" use="optional">
			<xsd:annotation>
				<xsd:documentation>Overall placement course of action. Examples: preload, default, interactive, etc.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="opportunityType" type="placementOpportunityTypeAttrType" use="optional">
			<xsd:annotation>
				<xsd:documentation>Placement opportunity type identification. Examples are: pre-roll, post-roll, interstitial, pause, etc.
				</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<xsd:element name="Service" type="ServiceType">
		<xsd:annotation>
			<xsd:documentation>Information for identifying a placement decision maker.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Service Description element -->
	<xsd:complexType name="ServiceDescriptionType">
		<xsd:sequence>
			<xsd:element ref="SystemContext">
				<xsd:annotation>
					<xsd:documentation>Associated service system parameter descripiton for which an ADS provides placement decisions.
					</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element ref="Service" maxOccurs="unbounded">
				<xsd:annotation>
					<xsd:documentation>Description of placement services for which an ADS provides placement decisions.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element ref="core:Ext" minOccurs="0"/>
		</xsd:sequence>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<xsd:element name="ServiceDescription" type="ServiceDescriptionType">
		<xsd:annotation>
			<xsd:documentation>Service specification defined by a system context and a list of services within that context.
			</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Service Group element -->
	<xsd:complexType name="ServiceGroupType">
		<xsd:simpleContent>
			<xsd:extension base="core:nonEmptyStringType">
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:simpleContent>
	</xsd:complexType>
	<xsd:element name="ServiceGroup" type="ServiceGroupType">
		<xsd:annotation>
			<xsd:documentation>Service group identification.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Session element -->
	<xsd:complexType name="SessionType">
		<xsd:simpleContent>
			<xsd:extension base="core:nonEmptyStringType">
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:simpleContent>
	</xsd:complexType>
	<xsd:element name="Session" type="SessionType">
		<xsd:annotation>
			<xsd:documentation>Session identification.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Session Event element -->
	<xsd:complexType name="SessionEventType">
		<xsd:complexContent>
			<xsd:extension base="EventBaseType">
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:element name="SessionEvent" type="SessionEventType">
		<xsd:annotation>
			<xsd:documentation>Event organized and identified based on some determined criteria.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Spot element -->
	<xsd:complexType name="SpotType">
		<xsd:sequence>
			<xsd:element ref="core:Tracking" minOccurs="0"/>
			<xsd:element ref="core:Content"/>
			<xsd:element ref="SpotNPT" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>NPT specific to the core:Content element above.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element ref="core:Ext" minOccurs="0"/>
		</xsd:sequence>
		<xsd:anyAttribute namespace="##any"/>
	</xsd:complexType>
	<xsd:element name="Spot" type="SpotType">
		<xsd:annotation>
			<xsd:documentation>Ad spot identification.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--SpotNPT element -->
	<xsd:element name="SpotNPT" type="NormalPlayTimeType">
		<xsd:annotation>
			<xsd:documentation>NPT scoped by (relative to) a declared Spot element.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--SpotScopedEvents element -->
	<xsd:complexType name="SpotScopedEventsType">
		<xsd:sequence>
			<xsd:annotation>
				<xsd:documentation>The Spot element applies to all events in the Events element and the Spot element shall not appear in any
					named event within the Events element
				</xsd:documentation>
			</xsd:annotation>
			<xsd:element ref="Spot"/>
			<xsd:element ref="Events"/>
		</xsd:sequence>
		<xsd:attributeGroup ref="eventRangeDateTimeAttrsType"/>
		<xsd:anyAttribute namespace="##any"/>
	</xsd:complexType>
	<xsd:element name="SpotScopedEvents" type="SpotScopedEventsType">
		<xsd:annotation>
			<xsd:documentation>Events all relative to the static Spot element declaration.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--System Context element -->
	<xsd:complexType name="SystemContextType">
		<xsd:sequence>
			<xsd:element ref="ServiceGroup" minOccurs="0"/>
			<xsd:element ref="Network" minOccurs="0"/>
			<xsd:element ref="Channel" minOccurs="0"/>
			<xsd:element ref="Zone" minOccurs="0"/>
			<xsd:element ref="Session" minOccurs="0"/>
			<xsd:element ref="core:Ext" minOccurs="0"/>
		</xsd:sequence>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<xsd:element name="SystemContext" type="SystemContextType">
		<xsd:annotation>
			<xsd:documentation>General system information.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--System Event element -->
	<xsd:complexType name="SystemEventType">
		<xsd:complexContent>
			<xsd:extension base="EventBaseType">
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:element name="SystemEvent" type="SystemEventType">
		<xsd:annotation>
			<xsd:documentation>Generic event container.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Targeting/Addressable Criteria element -->
	<xsd:element name="TargetCode">
		<xsd:annotation>
			<xsd:documentation>Addressable/target criteria element.</xsd:documentation>
		</xsd:annotation>
		<xsd:complexType mixed="true">
			<xsd:complexContent mixed="true">
				<xsd:extension base="xsd:anyType">
					<xsd:attribute name="key" type="core:nonEmptyStringType" use="required">
						<xsd:annotation>
							<xsd:documentation>Name of key in key/value pair.</xsd:documentation>
						</xsd:annotation>
					</xsd:attribute>
				</xsd:extension>
			</xsd:complexContent>
		</xsd:complexType>
	</xsd:element>
	<!--Terminal Address element -->
	<xsd:complexType name="TerminalAddressType">
		<xsd:simpleContent>
			<xsd:extension base="core:nonEmptyStringType">
				<xsd:attribute name="type" type="core:nonEmptyStringType" use="optional">
					<xsd:annotation>
						<xsd:documentation>Terminal endpoint type identification. Examples are: IP, MAC, etc.</xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:simpleContent>
	</xsd:complexType>
	<xsd:element name="TerminalAddress" type="TerminalAddressType">
		<xsd:annotation>
			<xsd:documentation>Client/subscriber/target endpoint identification. Transport specfic.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Viewer Event element -->
	<xsd:complexType name="ViewerEventType">
		<xsd:complexContent>
			<xsd:extension base="EventBaseType">
				<xsd:attribute name="eventSource" type="core:nonEmptyStringType" use="optional">
					<xsd:annotation>
						<xsd:documentation>Event generation source. Examples include: remote, software, etc.</xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:element name="ViewerEvent" type="ViewerEventType">
		<xsd:annotation>
			<xsd:documentation>Client/end-user/on-screen event.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Window element -->
	<xsd:complexType name="WindowType">
		<xsd:attribute name="start" type="core:dateTimeTimezoneType" use="required">
			<xsd:annotation>
				<xsd:documentation>Window start date and time.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="end" type="core:dateTimeTimezoneType" use="required">
			<xsd:annotation>
				<xsd:documentation>Window end date and time.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<xsd:element name="Window" type="WindowType">
		<xsd:annotation>
			<xsd:documentation>Placement opportunity scheduled time range.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!--Zone element -->
	<xsd:complexType name="ZoneType">
		<xsd:simpleContent>
			<xsd:extension base="core:nonEmptyStringType">
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:simpleContent>
	</xsd:complexType>
	<xsd:element name="Zone" type="ZoneType">
		<xsd:annotation>
			<xsd:documentation>Zone identifier.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!---->
	<!---->
</xsd:schema>""" % (IP, PORT)
            contentLength = len(response)
            self.server_version = "Python ADSSim"
            self.sys_version = ""
            self.send_response(200)
            self.send_header("X-Powered-By", "Servlet/2.5")
            self.send_header("Content-Type", 'text/xml;charset="utf-8"')
            self.send_header("Content-Length", contentLength)
            self.end_headers()
            self.wfile.write(response)
            print "\nSending " + self.path + " >>>>>>>>>>\n", response
            print "-----------------------------------------------\n"
###############################################################################
        elif self.path.find("?xsd=3") >= 0:
            response = """<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://www.scte.org/schemas/130-3/2008a/adm/podm" xmlns:core="http://www.scte.org/schemas/130-2/2008a/core" xmlns:adm="http://www.scte.org/schemas/130-3/2008a/adm" xmlns:gis="http://www.scte.org/schemas/130-8/2010a/gis" targetNamespace="http://www.scte.org/schemas/130-3/2008a/adm/podm" elementFormDefault="qualified" attributeFormDefault="unqualified">
	<!-- -->
	<xsd:import namespace="http://www.scte.org/schemas/130-2/2008a/core" schemaLocation="http://%s:%s/adrouter/130_playlist/2010/soap?xsd=1"/>
	<xsd:import namespace="http://www.scte.org/schemas/130-8/2010a/gis" schemaLocation="http://%s:%s/adrouter/130_playlist/2010/soap?xsd=4"/>
	<xsd:import namespace="http://www.scte.org/schemas/130-3/2008a/adm" schemaLocation="http://%s:%s/adrouter/130_playlist/2010/soap?xsd=2"/>
	<!---->
	<!---->
	<!--**************************** -->
	<!--***** POIS Elements ***** -->
	<!--**************************** -->
	<xsd:element name="ContentGroupFilter" type="ContentGroupFilterType">
		<xsd:annotation>
			<xsd:documentation>The filter mechanism for a group of content applicable to this Query Result.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:element name="Interactive" type="InteractiveType">
		<xsd:annotation>
			<xsd:documentation>The type of interactivity, if any, that is allowed</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:element name="LinearAvailBindingV2" type="LinearAvailBindingV2Type" substitutionGroup="adm:LinearAvailBinding">
		<xsd:annotation>
			<xsd:documentation>V2 extended linear avail placement opportunity specific information.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:element name="MenuCategory" type="MenuCategoryType">
		<xsd:annotation>
			<xsd:documentation>On screen menu grouping identifier.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:element name="OpportunityBindingV2" type="OpportunityBindingV2Type" substitutionGroup="adm:OpportunityBinding">
		<xsd:annotation>
			<xsd:documentation>V2 extended generic opportunity information bridge.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:element name="OpportunityConstraintsV2" type="OpportunityConstraintsV2Type" substitutionGroup="adm:OpportunityConstraints">
		<xsd:annotation>
			<xsd:documentation>The V2 revised OpportunityConstraints element.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:element name="PlacementOpportunityTerms" type="PlacementOpportunityTermsType">
		<xsd:annotation>
			<xsd:documentation>The advertising space identified owner and terms.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:element name="PlacementOpportunityV2" type="PlacementOpportunityV2Type" substitutionGroup="adm:PlacementOpportunity">
		<xsd:annotation>
			<xsd:documentation>The V2 revised Placement Opportunity root element (i.e., the root of the Placement Opportunity Data Model
				(PODM)).</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:element name="Plane" type="PlaneType">
		<xsd:annotation>
			<xsd:documentation>The show-ability of the ad space.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:element name="Programming" type="ProgrammingType"/>
	<xsd:element name="Size" type="SizeType">
		<xsd:annotation>
			<xsd:documentation>The spatial size of the ad space.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:element name="SpatialPosition" type="SpatialPositionType">
		<xsd:annotation>
			<xsd:documentation>Spatial information describing an ad space.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:element name="TopCorner" type="TopCornerType">
		<xsd:annotation>
			<xsd:documentation>The uppermost and leftmost coordinate of this space.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!---->
	<!--************************* -->
	<!--***** POIS Types ***** -->
	<!--************************* -->
	<!--POIS Simple Types -->
	<xsd:simpleType name="poGroupIndexAttrType">
		<xsd:restriction base="xsd:unsignedInt"/>
	</xsd:simpleType>
	<xsd:simpleType name="unitsIdentificationAttrType">
		<xsd:restriction base="core:nonEmptyStringType">
			<xsd:pattern value="(pixels|percentage|private:.+)"/>
		</xsd:restriction>
	</xsd:simpleType>
	<!---->
	<xsd:attributeGroup name="opportunityTypeV2ExtAttrGroup">
		<xsd:attribute name="poGroupIndex" type="poGroupIndexAttrType" use="optional">
			<xsd:annotation>
				<xsd:documentation>Placement Opportunity group index allows one or more Placement Opportunities to be commonly associated. The
					value zero means unknown or unused. Valid index values generally start at one and typcially increment by one.
				</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
	</xsd:attributeGroup>
	<!---->
	<!--POIS Complex Types -->
	<xsd:complexType name="ContentGroupFilterType">
		<xsd:choice>
			<xsd:element ref="gis:BasicQueryFilter"/>
			<xsd:element ref="gis:AdvancedQueryFilter"/>
			<!---->
			<xsd:element ref="core:Ext"/>
		</xsd:choice>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<xsd:complexType name="InteractiveType">
		<xsd:sequence>
			<xsd:element ref="core:Ext" minOccurs="0"/>
		</xsd:sequence>
		<xsd:attribute name="name" type="core:nonEmptyStringType" use="required">
			<xsd:annotation>
				<xsd:documentation>Interactive identification string.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<xsd:complexType name="LinearAvailBindingV2Type">
		<xsd:complexContent>
			<xsd:extension base="adm:LinearAvailBindingType">
				<xsd:attributeGroup ref="opportunityTypeV2ExtAttrGroup"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:complexType name="MenuCategoryType">
		<xsd:simpleContent>
			<xsd:extension base="core:nonEmptyStringType">
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:simpleContent>
	</xsd:complexType>
	<xsd:complexType name="OpportunityBindingV2Type">
		<xsd:complexContent>
			<xsd:extension base="adm:OpportunityBindingType">
				<xsd:attributeGroup ref="opportunityTypeV2ExtAttrGroup"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:complexType name="OpportunityConstraintsV2Type">
		<xsd:complexContent>
			<xsd:extension base="adm:OpportunityConstraintsType">
				<xsd:sequence>
					<xsd:element ref="SpatialPosition" minOccurs="0" maxOccurs="unbounded"/>
					<xsd:element ref="Interactive" minOccurs="0" maxOccurs="unbounded"/>
				</xsd:sequence>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:complexType name="PlacementOpportunityTermsType">
		<xsd:sequence>
			<xsd:element ref="core:Ext" minOccurs="0"/>
		</xsd:sequence>
		<xsd:attribute name="owner" type="core:nonEmptyStringType" use="required">
			<xsd:annotation>
				<xsd:documentation>The recognized name of that owner for billing purposes.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="exclusiveSales" type="xsd:boolean" use="optional">
			<xsd:annotation>
				<xsd:documentation>The recognized name of the exclusive sales channel for this opportunity.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="revenueSplit" use="optional">
			<xsd:annotation>
				<xsd:documentation>The portion of revenue from the sale of the opportunity destined to the owner.</xsd:documentation>
			</xsd:annotation>
			<xsd:simpleType>
				<xsd:restriction base="xsd:nonNegativeInteger">
					<xsd:minInclusive value="0"/>
					<xsd:maxInclusive value="100"/>
				</xsd:restriction>
			</xsd:simpleType>
		</xsd:attribute>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<xsd:complexType name="PlacementOpportunityV2Type">
		<xsd:complexContent>
			<xsd:extension base="adm:PlacementOpportunityType">
				<xsd:sequence>
					<xsd:element ref="adm:SystemContext" minOccurs="0"/>
					<xsd:element ref="Programming" minOccurs="0">
						<xsd:annotation>
							<xsd:documentation>The reference stream of content which contains the opportunity.</xsd:documentation>
						</xsd:annotation>
					</xsd:element>
					<xsd:element ref="PlacementOpportunityTerms" minOccurs="0" maxOccurs="unbounded">
						<xsd:annotation>
							<xsd:documentation>The business terms of the placement opportunity.</xsd:documentation>
						</xsd:annotation>
					</xsd:element>
				</xsd:sequence>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:complexType name="PlaneType">
		<xsd:attribute name="opacity" type="xsd:nonNegativeInteger" use="optional">
			<xsd:annotation>
				<xsd:documentation>The opaqueness level of this space (i.e. alpha channel).</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="zOrder" use="optional">
			<xsd:annotation>
				<xsd:documentation>The relative position in the z-plane to other spaces.</xsd:documentation>
			</xsd:annotation>
			<xsd:simpleType>
				<xsd:restriction base="xsd:nonNegativeInteger">
					<xsd:minInclusive value="1"/>
				</xsd:restriction>
			</xsd:simpleType>
		</xsd:attribute>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<xsd:complexType name="ProgrammingType">
		<xsd:sequence>
			<xsd:element ref="ContentGroupFilter" minOccurs="0" maxOccurs="unbounded">
				<xsd:annotation>
					<xsd:documentation>The content set description this placement opportunity is applicable to.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element ref="MenuCategory" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element ref="adm:Entertainment" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element ref="core:Ext" minOccurs="0"/>
		</xsd:sequence>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<xsd:complexType name="SizeType">
		<xsd:attribute name="xSize" type="xsd:nonNegativeInteger" use="required">
			<xsd:annotation>
				<xsd:documentation>The width of this space. </xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="ySize" type="xsd:nonNegativeInteger" use="required">
			<xsd:annotation>
				<xsd:documentation>The height of this space.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="units" type="unitsIdentificationAttrType" use="required">
			<xsd:annotation>
				<xsd:documentation>xSize and ySize attribute's unit identification. For example, pixels or percentage, etc.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<xsd:complexType name="SpatialPositionType">
		<xsd:sequence>
			<xsd:element ref="Plane" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>The show-ability of this space. </xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element ref="Size" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>The size of this space.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element ref="TopCorner" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>The uppermost and leftmost pixel of this space.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element ref="Interactive" minOccurs="0" maxOccurs="unbounded"/>
		</xsd:sequence>
		<xsd:attribute name="resolutionHorizontal" type="xsd:nonNegativeInteger" use="optional">
			<xsd:annotation>
				<xsd:documentation>The horizontal resolution.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="resolutionVertical" type="xsd:nonNegativeInteger" use="optional">
			<xsd:annotation>
				<xsd:documentation>The verticial resolution.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="units" type="unitsIdentificationAttrType" use="optional">
			<xsd:annotation>
				<xsd:documentation>resolutionHorizontal and resolutionVertical attribute's unit identification. For example, pixels or
					percentage, etc.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<xsd:complexType name="TopCornerType">
		<xsd:attribute name="xPosition" type="xsd:nonNegativeInteger" use="required">
			<xsd:annotation>
				<xsd:documentation>The leftmost position of this space.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="yPosition" type="xsd:nonNegativeInteger" use="required">
			<xsd:annotation>
				<xsd:documentation>The uppermost position of this space.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="units" type="unitsIdentificationAttrType" use="required">
			<xsd:annotation>
				<xsd:documentation>xPosition and yPosition attribute's unit identification. For example, pixels or percentage, etc.
				</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<!---->
	<!---->
</xsd:schema>""" % (IP, PORT, IP, PORT, IP, PORT)
            contentLength = len(response)
            self.server_version = "Python ADSSim"
            self.sys_version = ""
            self.send_response(200)
            self.send_header("X-Powered-By", "Servlet/2.5")
            self.send_header("Content-Type", 'text/xml;charset="utf-8"')
            self.send_header("Content-Length", contentLength)
            self.end_headers()
            self.wfile.write(response)
            print "\nSending " + self.path + " >>>>>>>>>>\n", response
            print "-----------------------------------------------\n"
###############################################################################
        elif self.path.find("?xsd=4") >= 0:
            response = """<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://www.scte.org/schemas/130-8/2010a/gis" xmlns:core="http://www.scte.org/schemas/130-2/2008a/core" targetNamespace="http://www.scte.org/schemas/130-8/2010a/gis" elementFormDefault="qualified">
	<!-- Import of CORE namespace -->
	<xsd:import namespace="http://www.scte.org/schemas/130-2/2008a/core" schemaLocation="http://%s:%s/adrouter/130_playlist/2010/soap?xsd=1"/>
	<!-- Misc GIS types -->
	<xsd:element name="AdvancedFilterElement" type="AdvancedFilterElementType">
		<xsd:annotation>
			<xsd:documentation>Advanced query language identification and processing commands.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:element name="AdvancedQueryFilter" type="AdvancedQueryFilterType">
		<xsd:annotation>
			<xsd:documentation>A sequence of AdvancedFilterElements.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:element name="AdvancedQueryLanguage" type="AdvancedQueryLanguageType">
		<xsd:annotation>
			<xsd:documentation>Supported advanced query language identification.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:element name="AdvancedQueryResult" type="AdvancedQueryResultType">
		<xsd:annotation>
			<xsd:documentation>Results from a query using an AdvancedQueryFilter sequence.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:element name="AdvancedQueryResultData" type="AdvancedQueryResultDataType"/>
	<xsd:element name="BasicFilterElement" type="BasicFilterElementType"/>
	<xsd:element name="BasicQueryDataModelDescription" type="BasicQueryDataModelDescriptionType">
		<xsd:annotation>
			<xsd:documentation>The unqiue qualifiers definitions and descriptions associated with a service data model and applicable to
				constructing a basic query.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:element name="BasicQueryFilter" type="BasicQueryFilterType">
		<xsd:annotation>
			<xsd:documentation>A sequence of BasicFilterElements.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:element name="BasicQueryResult" type="BasicQueryResultType" substitutionGroup="BasicQueryResultAbstract">
		<xsd:annotation>
			<xsd:documentation>Results from a query using either a UniqueQualifier element or a BasicQueryFilter sequence.
			</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:element name="BasicQueryResultAbstract" type="BasicQueryResultAbstractType" abstract="true">
		<xsd:annotation>
			<xsd:documentation>Abstract base type for result.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:element name="Cursor" type="CursorType">
		<xsd:annotation>
			<xsd:documentation>Cursor based access to a data model inquiry.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:element name="EnumerationValue" type="EnumerationValueType">
		<xsd:annotation>
			<xsd:documentation>Specific enumeration string.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:element name="MaxFloat" type="MinMaxFloatType">
		<xsd:annotation>
			<xsd:documentation>Maximum float Qualifier value.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:element name="MinFloat" type="MinMaxFloatType">
		<xsd:annotation>
			<xsd:documentation>Minimum float Qualifier value.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:element name="MaxInteger" type="MinMaxIntegerType">
		<xsd:annotation>
			<xsd:documentation>Maximum interger Qualifier value.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:element name="MinInteger" type="MinMaxIntegerType">
		<xsd:annotation>
			<xsd:documentation>Minimum interger Qualifier value.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:element name="MaxLength" type="MaxLengthType">
		<xsd:annotation>
			<xsd:documentation>Maximum Qualifier length.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:element name="Qualifier" type="QualifierType">
		<xsd:annotation>
			<xsd:documentation>Name/value pair each describing one object characteristic.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:element name="QualifierDeclaration" type="QualifierDeclarationType">
		<xsd:annotation>
			<xsd:documentation>Individiual object characteristic identifier declaration.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:element name="QualifierDescription" type="QualifierDescriptionType">
		<xsd:annotation>
			<xsd:documentation>Individual Qualifier descriptions and details.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:element name="QualifierSet" type="QualifierSetType">
		<xsd:annotation>
			<xsd:documentation>Group of name/value object characteristic pairs describing an object.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:element name="Query" type="QueryType">
		<xsd:annotation>
			<xsd:documentation>Detailed data model inquiry.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:element name="QueryResult" type="QueryResultType">
		<xsd:annotation>
			<xsd:documentation>Detailed data model inquiry result set.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:element name="ServiceDataModel" type="ServiceDataModelType">
		<xsd:annotation>
			<xsd:documentation>Service data model identifier. Typically, a URI.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:element name="ServiceDataModelProfile">
		<xsd:annotation>
			<xsd:documentation>Service data model identification and its supported features.</xsd:documentation>
		</xsd:annotation>
		<xsd:complexType>
			<xsd:complexContent>
				<xsd:extension base="ServiceDataModelProfileType"/>
			</xsd:complexContent>
		</xsd:complexType>
	</xsd:element>
	<xsd:element name="UniqueQualifier" type="UniqueQualifierType">
		<xsd:annotation>
			<xsd:documentation>A sequence of one or more Qualifiers that, taken together, uniquely identify a single object in a service data
				model.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<xsd:element name="UniqueQualifierDeclaration" type="UniqueQualifierDeclarationType">
		<xsd:annotation>
			<xsd:documentation>Identification of the set of Qualifiers that, taken together, may identify a single object in a service data
				model.</xsd:documentation>
		</xsd:annotation>
	</xsd:element>
	<!-- =================== -->
	<!-- Part 8 basic types -->
	<!-- =================== -->
	<xsd:simpleType name="cursorIdAttrType">
		<xsd:annotation>
			<xsd:documentation>Original cursor identifier.</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="core:nonEmptyStringType"/>
	</xsd:simpleType>
	<xsd:simpleType name="cursorIdRefAttrType">
		<xsd:annotation>
			<xsd:documentation>Reference to original cursor.</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="cursorIdAttrType"/>
	</xsd:simpleType>
	<xsd:simpleType name="expandOutputAttrType">
		<xsd:annotation>
			<xsd:documentation>Expanded output indicator.</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="xsd:boolean"/>
	</xsd:simpleType>
	<xsd:simpleType name="filterElementNameAttrType">
		<xsd:annotation>
			<xsd:documentation>Name attribute value.</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="core:nonEmptyStringType"/>
	</xsd:simpleType>
	<xsd:simpleType name="filterElementValueAttrType">
		<xsd:annotation>
			<xsd:documentation>Value attribute value.</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="core:nonEmptyStringType"/>
	</xsd:simpleType>
	<xsd:simpleType name="NotificationTypeEnumeration">
		<xsd:annotation>
			<xsd:documentation>One of "new", "update" or "delete".</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="core:nonEmptyStringType">
			<xsd:pattern value="(new|update|delete|private:.+)"/>
		</xsd:restriction>
	</xsd:simpleType>
	<xsd:simpleType name="qualifierNameAttrType">
		<xsd:restriction base="core:nonEmptyStringType"/>
	</xsd:simpleType>
	<xsd:simpleType name="QualifierValueTypeEnumerationType">
		<xsd:restriction base="core:nonEmptyStringType">
			<xsd:pattern value="(integer|float|string|enumeration|private:.+)"/>
		</xsd:restriction>
	</xsd:simpleType>
	<xsd:simpleType name="QueryFilterOpTypeEnumeration">
		<xsd:restriction base="core:nonEmptyStringType">
			<xsd:enumeration value="include"/>
			<xsd:enumeration value="exclude"/>
		</xsd:restriction>
	</xsd:simpleType>
	<xsd:simpleType name="queryIdAttrType">
		<xsd:annotation>
			<xsd:documentation>Original query identifier.</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="core:nonEmptyStringType"/>
	</xsd:simpleType>
	<xsd:simpleType name="queryIdRefAttrType">
		<xsd:annotation>
			<xsd:documentation>Reference to original query.</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="queryIdAttrType"/>
	</xsd:simpleType>
	<xsd:simpleType name="queryLanguageAttrType">
		<xsd:annotation>
			<xsd:documentation>Query language attribute value.</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="core:nonEmptyStringType">
			<xsd:pattern value="(XPath|XQuery|private:.+)"/>
		</xsd:restriction>
	</xsd:simpleType>
	<xsd:simpleType name="resultSetSizeOnlyAttrType">
		<xsd:annotation>
			<xsd:documentation>Result set size only output indicator.</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="xsd:boolean"/>
	</xsd:simpleType>
	<xsd:simpleType name="totalResultSetSizeAttrType">
		<xsd:annotation>
			<xsd:documentation>Total available results.</xsd:documentation>
		</xsd:annotation>
		<xsd:restriction base="xsd:nonNegativeInteger"/>
	</xsd:simpleType>
	<xsd:simpleType name="uniqueQualifierNameAttrType">
		<xsd:restriction base="core:nonEmptyStringType"/>
	</xsd:simpleType>
	<!-- =================== -->
	<!-- Part 8 message types -->
	<!-- =================== -->
	<!-- ================================ -->
	<!-- ListSupportedFeaturesRequestType -->
	<!-- ================================ -->
	<xsd:complexType name="ListSupportedFeaturesRequestType">
		<xsd:complexContent>
			<xsd:extension base="core:Msg_RequestBaseType">
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<!-- ================================= -->
	<!-- ListSupportedFeaturesResponseType -->
	<!-- ================================= -->
	<xsd:complexType name="ListSupportedFeaturesResponseType">
		<xsd:complexContent>
			<xsd:extension base="core:Msg_ResponseBaseType">
				<xsd:sequence>
					<xsd:element ref="core:Callout" minOccurs="0" maxOccurs="unbounded"/>
					<xsd:element ref="ServiceDataModelProfile" minOccurs="0" maxOccurs="unbounded"/>
				</xsd:sequence>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<!-- ========================= -->
	<!-- ListQualifiersRequestType -->
	<!-- ========================= -->
	<xsd:complexType name="ListQualifiersRequestType">
		<xsd:complexContent>
			<xsd:extension base="core:Msg_RequestBaseType">
				<xsd:sequence>
					<xsd:element ref="ServiceDataModel" minOccurs="0"/>
				</xsd:sequence>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<!-- ========================== -->
	<!-- ListQualifiersResponseType -->
	<!-- ========================== -->
	<xsd:complexType name="ListQualifiersResponseType">
		<xsd:complexContent>
			<xsd:extension base="core:Msg_ResponseBaseType">
				<xsd:sequence>
					<xsd:element ref="BasicQueryDataModelDescription" minOccurs="0"/>
				</xsd:sequence>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<!-- ================ -->
	<!-- NotificationType -->
	<!-- ================ -->
	<xsd:complexType name="NotificationType">
		<xsd:complexContent>
			<xsd:extension base="core:Msg_NotificationBaseType">
				<xsd:sequence>
					<xsd:element ref="QueryResult"/>
				</xsd:sequence>
				<xsd:attribute name="noticeType" type="NotificationTypeEnumeration" use="required">
					<xsd:annotation>
						<xsd:documentation>Data change identifier.</xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<!-- =============================== -->
	<!-- NotificationAcknowledgementType -->
	<!-- =============================== -->
	<xsd:complexType name="NotificationAcknowledgementType">
		<xsd:complexContent>
			<xsd:extension base="core:Msg_AcknowledgementBaseType">
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<!-- =================================== -->
	<!-- NotificationRegistrationRequestType -->
	<!-- =================================== -->
	<xsd:complexType name="NotificationRegistrationRequestType">
		<xsd:complexContent>
			<xsd:extension base="core:Msg_RequestBaseType">
				<xsd:sequence>
					<xsd:element ref="core:Callout" maxOccurs="unbounded"/>
					<xsd:element ref="Query"/>
				</xsd:sequence>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<!-- ==================================== -->
	<!-- NotificationRegistrationResponseType -->
	<!-- ==================================== -->
	<xsd:complexType name="NotificationRegistrationResponseType">
		<xsd:complexContent>
			<xsd:extension base="core:Msg_ResponseBaseType">
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<!-- ======================================= -->
	<!-- ListNotificationRegistrationRequestType -->
	<!-- ======================================= -->
	<xsd:complexType name="ListNotificationRegistrationRequestType">
		<xsd:complexContent>
			<xsd:extension base="core:Msg_RequestBaseType">
				<xsd:attribute name="registrationRef" type="core:registrationRefAttrType" use="optional">
					<xsd:annotation>
						<xsd:documentation>Reference to a previous, original NotificationRegistrationRequest message. If this attribute is ommitted
							then all registrations may be returned.</xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<!-- ======================================== -->
	<!-- ListNotificationRegistrationResponseType -->
	<!-- ======================================== -->
	<xsd:complexType name="ListNotificationRegistrationResponseType">
		<xsd:complexContent>
			<xsd:extension base="core:Msg_ResponseBaseType">
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<!-- =============================== -->
	<!-- DeregistrationNotificationType -->
	<!-- =============================== -->
	<xsd:complexType name="DeregistrationNotificationType">
		<xsd:complexContent>
			<xsd:extension base="core:Msg_NotificationBaseType">
				<xsd:sequence>
					<xsd:element ref="core:StatusCode" minOccurs="0"/>
				</xsd:sequence>
				<xsd:attribute name="registrationRef" type="core:registrationRefAttrType" use="optional">
					<xsd:annotation>
						<xsd:documentation>Reference to a previous, original NotificationRegistrationRequest message. If this attribute is ommitted
							then all registrations, scoped by the identity attribute, are being deleted.</xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<!-- ================================= -->
	<!-- DeregistrationAcknowledgementType -->
	<!-- ================================= -->
	<xsd:complexType name="DeregistrationAcknowledgementType">
		<xsd:complexContent>
			<xsd:extension base="core:Msg_AcknowledgementBaseType">
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<!-- ================================= -->
	<!-- NotificationDeregisterRequestType -->
	<!-- ================================= -->
	<xsd:complexType name="NotificationDeregisterRequestType">
		<xsd:complexContent>
			<xsd:extension base="core:Msg_RequestBaseType">
				<xsd:attribute name="registrationRef" type="core:registrationRefAttrType" use="optional">
					<xsd:annotation>
						<xsd:documentation>Reference to a previous, original NotificationRegistrationRequest message. If this attribute is ommitted
							then all registrations, scoped by the identity attribute, are being deleted.</xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<!-- ================================== -->
	<!-- NotificationDeregisterResponseType -->
	<!-- ================================== -->
	<xsd:complexType name="NotificationDeregisterResponseType">
		<xsd:complexContent>
			<xsd:extension base="core:Msg_ResponseBaseType">
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<!-- ================= -->
	<!-- QueryRequestType -->
	<!-- ================= -->
	<xsd:complexType name="QueryRequestType">
		<xsd:complexContent>
			<xsd:extension base="core:Msg_RequestBaseType">
				<xsd:sequence>
					<xsd:choice>
						<xsd:element ref="Cursor"/>
						<xsd:element ref="Query"/>
					</xsd:choice>
				</xsd:sequence>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<!-- ================== -->
	<!-- QueryResponseType -->
	<!-- ================== -->
	<xsd:complexType name="QueryResponseType">
		<xsd:complexContent>
			<xsd:extension base="core:Msg_ResponseBaseType">
				<xsd:sequence>
					<xsd:element ref="QueryResult" minOccurs="0"/>
				</xsd:sequence>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<!-- ======================= -->
	<!-- CreateCursorRequestType -->
	<!-- ======================= -->
	<xsd:complexType name="CreateCursorRequestType">
		<xsd:complexContent>
			<xsd:extension base="core:Msg_RequestBaseType">
				<xsd:sequence>
					<xsd:element ref="Query"/>
				</xsd:sequence>
				<xsd:attribute name="cursorId" type="cursorIdAttrType" use="required">
					<xsd:annotation>
						<xsd:documentation>Unique cursor identifier.</xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
				<xsd:attribute name="cursorExpires" type="core:dateTimeTimezoneType" use="required">
					<xsd:annotation>
						<xsd:documentation>Proposed cursor expiration date and time.</xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<!-- ======================== -->
	<!-- CreateCursorResponseType -->
	<!-- ======================== -->
	<xsd:complexType name="CreateCursorResponseType">
		<xsd:complexContent>
			<xsd:extension base="core:Msg_ResponseBaseType">
				<xsd:attribute name="cursorExpires" type="core:dateTimeTimezoneType" use="required">
					<xsd:annotation>
						<xsd:documentation>Definitive date and time when the cursor becomes invalid and the cursor data is no longer is available.
						</xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
				<xsd:attribute name="totalResultSetSize" type="totalResultSetSizeAttrType" use="optional">
					<xsd:annotation>
						<xsd:documentation>Total available results.</xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<!-- ======================= -->
	<!-- CancelCursorRequestType -->
	<!-- ======================= -->
	<xsd:complexType name="CancelCursorRequestType">
		<xsd:complexContent>
			<xsd:extension base="core:Msg_RequestBaseType">
				<xsd:attribute name="cursorRef" type="cursorIdRefAttrType" use="required">
					<xsd:annotation>
						<xsd:documentation>Unique cursor identifier (i.e., the cursorId attribute) supplied in the CreateCursorRequest message.
						</xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<!-- ======================== -->
	<!-- CancelCursorResponseType -->
	<!-- ======================== -->
	<xsd:complexType name="CancelCursorResponseType">
		<xsd:complexContent>
			<xsd:extension base="core:Msg_ResponseBaseType">
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<!-- =================== -->
	<!-- Part 8 element types -->
	<!-- =================== -->
	<!-- ========================= -->
	<!-- AdvancedFilterElementType -->
	<!-- ========================= -->
	<xsd:complexType name="AdvancedFilterElementType" mixed="true">
		<xsd:complexContent mixed="true">
			<xsd:extension base="xsd:anyType">
				<xsd:attribute name="queryId" type="queryIdAttrType" use="required">
					<xsd:annotation>
						<xsd:documentation>Unique query identifier.</xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
				<xsd:attribute name="ql" type="queryLanguageAttrType" use="required">
					<xsd:annotation>
						<xsd:documentation>Query language specifier.</xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<!-- ======================== -->
	<!-- AdvancedQueryFilterType -->
	<!-- ======================== -->
	<xsd:complexType name="AdvancedQueryFilterType">
		<xsd:sequence>
			<xsd:element ref="AdvancedFilterElement" maxOccurs="unbounded"/>
		</xsd:sequence>
		<xsd:attribute name="op" type="QueryFilterOpTypeEnumeration" use="optional">
			<xsd:annotation>
				<xsd:documentation>Processing instruction relative to cumulative result set.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<!-- ======================== -->
	<!-- AdvancedQueryLanguageType -->
	<!-- ======================== -->
	<xsd:complexType name="AdvancedQueryLanguageType">
		<xsd:simpleContent>
			<xsd:extension base="core:nonEmptyStringType">
				<xsd:attribute name="version" type="core:nonEmptyStringType" use="optional">
					<xsd:annotation>
						<xsd:documentation>Advanced query language revision identification.</xsd:documentation>
					</xsd:annotation>
				</xsd:attribute>
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:simpleContent>
	</xsd:complexType>
	<!-- ======================= -->
	<!-- AdvancedQueryResultType -->
	<!-- ======================= -->
	<xsd:complexType name="AdvancedQueryResultType">
		<xsd:choice>
			<xsd:element ref="UniqueQualifier" maxOccurs="unbounded">
				<xsd:annotation>
					<xsd:documentation>Returned when an advanced query sets @expandOutput to false.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element ref="AdvancedQueryResultData" maxOccurs="unbounded">
				<xsd:annotation>
					<xsd:documentation>Returned for all other advanced queries.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
		</xsd:choice>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<!-- =========================== -->
	<!-- AdvancedQueryResultDataType -->
	<!-- =========================== -->
	<xsd:complexType name="AdvancedQueryResultDataType" mixed="true">
		<xsd:complexContent mixed="true">
			<xsd:extension base="xsd:anyType"/>
		</xsd:complexContent>
	</xsd:complexType>
	<!-- ====================== -->
	<!-- BasicFilterElementType -->
	<!-- ====================== -->
	<xsd:complexType name="BasicFilterElementType">
		<xsd:attribute name="name" type="filterElementNameAttrType" use="required">
			<xsd:annotation>
				<xsd:documentation>Characteristic identifier.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="value" type="filterElementValueAttrType" use="required">
			<xsd:annotation>
				<xsd:documentation>Characteristic query value.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="valueIsRegex" type="xsd:boolean" use="optional">
			<xsd:annotation>
				<xsd:documentation>Data contained in the value attribute is a regular expression.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<!-- ================================== -->
	<!-- BasicQueryDataModelDescriptionType -->
	<!-- ================================== -->
	<xsd:complexType name="BasicQueryDataModelDescriptionType">
		<xsd:sequence>
			<xsd:element ref="ServiceDataModel"/>
			<xsd:element ref="UniqueQualifierDeclaration" maxOccurs="unbounded"/>
			<xsd:element ref="QualifierDescription" maxOccurs="unbounded"/>
		</xsd:sequence>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<!-- ===================== -->
	<!-- BasicQueryFilterType -->
	<!-- ===================== -->
	<xsd:complexType name="BasicQueryFilterType">
		<xsd:annotation>
			<xsd:documentation>Sequence of BasicFilterElements.</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element ref="BasicFilterElement" maxOccurs="unbounded"/>
		</xsd:sequence>
		<xsd:attribute name="op" type="QueryFilterOpTypeEnumeration" use="optional">
			<xsd:annotation>
				<xsd:documentation>Processing instruction relative to cumulative result set.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<!-- ===================== -->
	<!-- BasicQueryResultAbstractType -->
	<!-- ===================== -->
	<xsd:complexType name="BasicQueryResultAbstractType" abstract="true">
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<!-- ===================== -->
	<!-- BasicQueryResultType -->
	<!-- ===================== -->
	<xsd:complexType name="BasicQueryResultType">
		<xsd:complexContent>
			<xsd:extension base="BasicQueryResultAbstractType">
				<xsd:choice>
					<xsd:element ref="UniqueQualifier" maxOccurs="unbounded">
						<xsd:annotation>
							<xsd:documentation>Returned when Query's BasicFilterElement has @expandOuptut=false.</xsd:documentation>
						</xsd:annotation>
					</xsd:element>
					<xsd:element ref="QualifierSet" maxOccurs="unbounded">
						<xsd:annotation>
							<xsd:documentation>Return only 1 when query uses UniqueQualifier and returns 1 to unbounded when Query's BasciFilterElement
								has @expandOutput=true.</xsd:documentation>
						</xsd:annotation>
					</xsd:element>
				</xsd:choice>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<!-- ========== -->
	<!-- CursorType -->
	<!-- ========== -->
	<xsd:complexType name="CursorType">
		<xsd:attribute name="cursorRef" type="cursorIdRefAttrType" use="required">
			<xsd:annotation>
				<xsd:documentation>Unique cursor identifier supplied in the original query.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="queryRef" type="queryIdAttrType" use="required">
			<xsd:annotation>
				<xsd:documentation>Original unique query identifier.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="startIndex" type="xsd:nonNegativeInteger" use="required">
			<xsd:annotation>
				<xsd:documentation>Start point within the result set.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="count" type="xsd:nonNegativeInteger" use="required">
			<xsd:annotation>
				<xsd:documentation>Result items to return.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<!-- ===================== -->
	<!--EnumeationValueType -->
	<!-- ===================== -->
	<xsd:complexType name="EnumerationValueType">
		<xsd:simpleContent>
			<xsd:extension base="core:nonEmptyStringType">
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:simpleContent>
	</xsd:complexType>
	<!-- ================== -->
	<!--MinMaxIntegerType -->
	<!-- ================== -->
	<xsd:complexType name="MinMaxIntegerType">
		<xsd:simpleContent>
			<xsd:extension base="xsd:integer">
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:simpleContent>
	</xsd:complexType>
	<!-- ================ -->
	<!--MinMaxFloatType -->
	<!-- ================ -->
	<xsd:complexType name="MinMaxFloatType">
		<xsd:simpleContent>
			<xsd:extension base="xsd:float">
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:simpleContent>
	</xsd:complexType>
	<!-- =============== -->
	<!--MaxLengthType -->
	<!-- =============== -->
	<xsd:complexType name="MaxLengthType">
		<xsd:simpleContent>
			<xsd:extension base="xsd:nonNegativeInteger">
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:simpleContent>
	</xsd:complexType>
	<!-- ============= -->
	<!-- QualifierType -->
	<!-- ============= -->
	<xsd:complexType name="QualifierType">
		<xsd:attribute name="name" type="qualifierNameAttrType" use="required">
			<xsd:annotation>
				<xsd:documentation>The characteristic's identification string.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="value" type="core:nonEmptyStringType" use="required">
			<xsd:annotation>
				<xsd:documentation>The named characteristic's data value.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<!-- ============= -->
	<!--QualifierDeclarationType -->
	<!-- ============= -->
	<xsd:complexType name="QualifierDeclarationType">
		<xsd:attribute name="name" type="qualifierNameAttrType" use="required">
			<xsd:annotation>
				<xsd:documentation>The characteristic's identification string.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<!-- ========================= -->
	<!-- QualifierDescriptionType -->
	<!-- ========================= -->
	<xsd:complexType name="QualifierDescriptionType">
		<xsd:choice minOccurs="0">
			<xsd:sequence>
				<xsd:element ref="MinInteger" minOccurs="0"/>
				<xsd:element ref="MaxInteger" minOccurs="0"/>
			</xsd:sequence>
			<xsd:sequence>
				<xsd:element ref="MinFloat" minOccurs="0"/>
				<xsd:element ref="MaxFloat" minOccurs="0"/>
			</xsd:sequence>
			<xsd:element ref="MaxLength"/>
			<xsd:element ref="EnumerationValue" maxOccurs="unbounded"/>
			<xsd:element ref="core:Ext"/>
		</xsd:choice>
		<xsd:attribute name="name" type="core:nonEmptyStringType" use="required">
			<xsd:annotation>
				<xsd:documentation>Qualifier name.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="valueType" type="QualifierValueTypeEnumerationType" use="required">
			<xsd:annotation>
				<xsd:documentation>The qualifier's value type.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="description" type="core:nonEmptyStringType" use="optional">
			<xsd:annotation>
				<xsd:documentation>Descriptive text.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<!-- ================ -->
	<!-- QualifierSetType -->
	<!-- ================ -->
	<xsd:complexType name="QualifierSetType">
		<xsd:sequence>
			<xsd:element ref="Qualifier" maxOccurs="unbounded"/>
		</xsd:sequence>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<!-- ========== -->
	<!-- QueryType -->
	<!-- ========== -->
	<xsd:complexType name="QueryType">
		<xsd:sequence>
			<xsd:element ref="ServiceDataModel" minOccurs="0"/>
			<xsd:choice>
				<xsd:element ref="UniqueQualifier"/>
				<xsd:element ref="BasicQueryFilter" maxOccurs="unbounded"/>
				<xsd:element ref="AdvancedQueryFilter" maxOccurs="unbounded"/>
			</xsd:choice>
		</xsd:sequence>
		<xsd:attribute name="queryId" type="queryIdAttrType" use="required">
			<xsd:annotation>
				<xsd:documentation>Unique query identifier.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="expandOutput" type="expandOutputAttrType" use="optional">
			<xsd:annotation>
				<xsd:documentation>Result expansion control.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="resultSetSizeOnly" type="resultSetSizeOnlyAttrType" use="optional">
			<xsd:annotation>
				<xsd:documentation>Return only a result count.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="uniqueQualifierNameRef" type="uniqueQualifierNameAttrType" use="optional">
			<xsd:annotation>
				<xsd:documentation>Return the named UniqueQualifer based result, if more than one UniqueQualifier is available for the service
					data model.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<!-- =============== -->
	<!-- QueryResultType -->
	<!-- =============== -->
	<xsd:complexType name="QueryResultType">
		<xsd:choice minOccurs="0">
			<xsd:element ref="BasicQueryResultAbstract"/>
			<xsd:element ref="AdvancedQueryResult"/>
		</xsd:choice>
		<xsd:attribute name="queryRef" type="queryIdAttrType" use="required">
			<xsd:annotation>
				<xsd:documentation>Original query identifier.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="resultSetSize" type="xsd:nonNegativeInteger" use="required">
			<xsd:annotation>
				<xsd:documentation>Result count contained within the accompanying result element.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:attribute name="totalResultSetSize" type="totalResultSetSizeAttrType">
			<xsd:annotation>
				<xsd:documentation>Total available results.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<!-- ==================== -->
	<!-- ServiceDataModelType -->
	<!-- ==================== -->
	<xsd:complexType name="ServiceDataModelType">
		<xsd:simpleContent>
			<xsd:extension base="xsd:anyURI">
				<xsd:anyAttribute namespace="##any" processContents="lax"/>
			</xsd:extension>
		</xsd:simpleContent>
	</xsd:complexType>
	<!-- =============================== -->
	<!-- ServiceDataModelProfileType -->
	<!-- =============================== -->
	<xsd:complexType name="ServiceDataModelProfileType">
		<xsd:sequence>
			<xsd:element ref="ServiceDataModel"/>
			<xsd:element ref="AdvancedQueryLanguage" minOccurs="0" maxOccurs="unbounded"/>
			<xsd:element ref="core:Ext" minOccurs="0"/>
		</xsd:sequence>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<!-- ======================= -->
	<!-- UniqueQualifierType -->
	<!-- ======================= -->
	<xsd:complexType name="UniqueQualifierType">
		<xsd:sequence>
			<xsd:element ref="Qualifier" maxOccurs="unbounded"/>
		</xsd:sequence>
		<xsd:attribute name="uniqueQualifierNameRef" type="uniqueQualifierNameAttrType" use="optional">
			<xsd:annotation>
				<xsd:documentation>Unique name identifying the qualifier group. This attribute shall be present when two unique qualifiers exist
					for a single service data model.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<!-- ======================= -->
	<!-- UniqueQualifierDeclarationType -->
	<!-- ======================= -->
	<xsd:complexType name="UniqueQualifierDeclarationType">
		<xsd:sequence>
			<xsd:element ref="QualifierDeclaration" maxOccurs="unbounded"/>
		</xsd:sequence>
		<xsd:attribute name="uniqueQualifierName" type="uniqueQualifierNameAttrType" use="optional">
			<xsd:annotation>
				<xsd:documentation>Unique name identifying the qualifier group. This attribute shall be present when two unique qualifiers exist
					for a single service data model.</xsd:documentation>
			</xsd:annotation>
		</xsd:attribute>
		<xsd:anyAttribute namespace="##any" processContents="lax"/>
	</xsd:complexType>
	<!---->
</xsd:schema>""" % (IP, PORT)
            contentLength = len(response)
            self.server_version = "Python ADSSim"
            self.sys_version = ""
            self.send_response(200)
            self.send_header("X-Powered-By", "Servlet/2.5")
            self.send_header("Content-Type", 'text/xml;charset="utf-8"')
            self.send_header("Content-Length", contentLength)
            self.end_headers()
            self.wfile.write(response)
            print "\nSending " + self.path + " >>>>>>>>>>\n", response
            print "-----------------------------------------------\n"
###############################################################################

        elif 'registerADM' in parse_qs(urlparse(self.path).query):
            #register sim with ADM and allows for particular message registration
            #http://ip:port/whatever?registerADM=10.116.2.157
            #http://ip:port/whatever?registerADM=10.116.2.157&message=PlacementRequest

            clearMessageCache()
            clearNegativeResponseSettings()

            admIp = parse_qs(urlparse(self.path).query)['registerADM'][0]
            messageId = str(uuid.uuid1())

            if 'message' in parse_qs(urlparse(self.path).query):
                #if message query parameter included, add message attribute to Callout element
                message = ' message="' + parse_qs(urlparse(self.path).query)['message'][0] + '"'
                print "Registering with" + message
            else:
                message = ''

            payload = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:adm="http://www.scte.org/schemas/130-3/2008a/adm" xmlns:core="http://www.scte.org/schemas/130-2/2008a/core">
   <soapenv:Header/>
   <soapenv:Body>
      <adm:ADSRegistrationRequest system="%s" identity="%s" version="1.1" messageId="%s" xmlns:podm="http://www.scte.org/schemas/130-3/2008a/adm/podm" xmlns:ns4="http://www.scte.org/schemas/130-8/2010a/gis">
         <adm:ServiceDescription>
            <adm:SystemContext/>
            <adm:Service id="All ADM Offered Services"/>
         </adm:ServiceDescription>
         <core:Callout%s>
            <core:Address type="SOAP">http://%s:%s/2010/soap</core:Address>
         </core:Callout>
      </adm:ADSRegistrationRequest>
   </soapenv:Body>
</soapenv:Envelope>""" % (SYSTEM, IDENTITY, messageId, message, IP, str(PORT))

            print "Attempting to register with ADM at " + admIp
            print "Sending >>>>>>>>>>\n", payload

            #admPort and url hard coded right now
            url = 'http://%s:8282/adm/2010' % (admIp)
            r = requests.post(url, payload, headers={'content-type': 'text/xml;charset=UTF-8'}, timeout=10)

            print "Response from ADM: <<<<<<<<<<\n" + r.text

            if r.text.find('StatusCode class="0"') >= 0:
                print "\n**********Registration SUCCESSFUL!"
                temp = {"registrationRef" : messageId}
                response = json.dumps(temp)
                self.server_version = "Python ADSSim"
                self.sys_version = ""
                self.send_response(200)
                self.send_header("Content-Type", 'application/json')
                self.send_header("Content-Length", len(response))
                self.end_headers()
                self.wfile.write(response)
                print response
            else:
                print "\n**********Registration FAILED!"
                self.send_response(400)

            print "-----------------------------------------------\n"

###############################################################################

        elif self.path.find("lastPlacementRequest") >= 0:
            #return last PlacementRequest received
            response = lastPlacementRequest
            self.server_version = "Python ADSSim"
            self.sys_version = ""
            self.send_response(200)
            self.send_header("Content-Type", 'text/xml;charset="utf-8"')
            self.send_header("Content-Length", len(response))
            self.end_headers()
            self.wfile.write(response)
            print "\nSending >>>>>>>>>>\n", response
            print "-----------------------------------------------\n"

###############################################################################

        elif self.path.find("lastPlacementStatusNotification") >= 0:
            #return last PlacementStatusNotification received
            response = lastPlacementStatusNotification
            self.server_version = "Python ADSSim"
            self.sys_version = ""
            self.send_response(200)
            self.send_header("Content-Type", 'text/xml;charset="utf-8"')
            self.send_header("Content-Length", len(response))
            self.end_headers()
            self.wfile.write(response)
            print "\nSending >>>>>>>>>>\n", response
            print "-----------------------------------------------\n"

###############################################################################

        elif self.path.find("lastPlacementResponse") >= 0:
            #return last PlacementResponse sent
            response = lastPlacementResponse
            self.server_version = "Python ADSSim"
            self.sys_version = ""
            self.send_response(200)
            self.send_header("Content-Type", 'text/xml;charset="utf-8"')
            self.send_header("Content-Length", len(response))
            self.end_headers()
            self.wfile.write(response)
            print "\nSending >>>>>>>>>>\n", response
            print "-----------------------------------------------\n"

###############################################################################

        elif self.path.find("lastDeregistrationNotification") >= 0:
            #return last ADSDeregistrationNotification received
            response = lastDeregistrationNotification
            self.server_version = "Python ADSSim"
            self.sys_version = ""
            self.send_response(200)
            self.send_header("Content-Type", 'text/xml;charset="utf-8"')
            self.send_header("Content-Length", len(response))
            self.end_headers()
            self.wfile.write(response)
            print "\nSending >>>>>>>>>>\n", response
            print "-----------------------------------------------\n"

###############################################################################

        elif self.path.find("clearMessageCache") >= 0:
            #initiates clearing the cache of last* messages the sim remembers
            clearMessageCache()
            print "Message cache cleared"
            self.send_response(200)

###############################################################################

        elif self.path.find("lastServiceStatusNotification") >= 0:
            #return last ServiceStatusNotification received
            response = lastServiceStatusNotification
            self.server_version = "Python ADSSim"
            self.sys_version = ""
            self.send_response(200)
            self.send_header("Content-Type", 'text/xml;charset="utf-8"')
            self.send_header("Content-Length", len(response))
            self.end_headers()
            self.wfile.write(response)
            print "\nSending >>>>>>>>>>\n", response
            print "-----------------------------------------------\n"

###############################################################################

        elif self.path.find("negativePlacementStatusAck") >= 0:
            #tells sim to respond with StatusCode class=1 for PSN received
            global negativePlacementStatusAck
            global negativePlacementStatusAckCount
            negativePlacementStatusAck = True
            if 'count' in parse_qs(urlparse(self.path).query):
                negativePlacementStatusAckCount = int(parse_qs(urlparse(self.path).query)['count'][0])
            else:
                negativePlacementStatusAckCount = 1
            print "Will send", negativePlacementStatusAckCount, "PlacementStatusAcknowledgement messages with StatusCode class=1"
            self.send_response(200)

###############################################################################

        elif self.path.find("slowPlacementStatusAck") >= 0:
            #tells sim to sleep before sending PlacementStatusAcknowledgement
            global psnAckSleep
            if 'duration' in parse_qs(urlparse(self.path).query):
                psnAckSleep = int(parse_qs(urlparse(self.path).query)['duration'][0])
            else:
                psnAckSleep = 20

            print "Will sleep for", psnAckSleep, "seconds before responding to next PSN"
            self.send_response(200)

###############################################################################

        elif self.path.find("startSoapRequestSleep") >= 0:
            #tells sim to sleep before sending SOAP responses
            global soapRequestSleep
            soapRequestSleep = True

            print "Will sleep for 20 seconds before responding to SOAP requests"
            self.send_response(200)

###############################################################################

        elif self.path.find("stopSoapRequestSleep") >= 0:
            #tells sim to stop sleeping before sending SOAP responses
            #global soapRequestSleep
            soapRequestSleep = False

            print "Disabling sleep before responding to SOAP requets"
            self.send_response(200)

###############################################################################

        elif self.path.find("repeatAds") >= 0:
            #tells sim to repeat ads on next PlacementResponse
            global repeatAds
            repeatAds = True

            print "Will repeat ads in next PlacementResponse"
            self.send_response(200)

###############################################################################

        elif self.path.find("reloadAssets") >= 0:
            #refresh ad placement dictionary at runtime
            print "Reloading ad placement dictionary from assets.csv..."
            adPlacements.clear()
            readAdPlacements()
            self.send_response(200)

###############################################################################

        elif self.path.find("dynamicAdPlacements") >= 0:
            #enable dynamic ad placements for PlacementResponse
            global dynamicAdPlacements
            global numPreroll
            global numMidroll
            global numPostroll

            dynamicAdPlacements = True

            if 'numPreroll' in parse_qs(urlparse(self.path).query):
                numPreroll = int(parse_qs(urlparse(self.path).query)['numPreroll'][0])

            if 'numMidroll' in parse_qs(urlparse(self.path).query):
                numMidroll = int(parse_qs(urlparse(self.path).query)['numMidroll'][0])

            if 'numPostroll' in parse_qs(urlparse(self.path).query):
                numPostroll = int(parse_qs(urlparse(self.path).query)['numPostroll'][0])

            print "Dynamic ad placements ENABLED.  numPreroll =", numPreroll, "numMidroll =", numMidroll, "numPostroll =", numPostroll
            self.send_response(200)

###############################################################################

        else:
            self.send_response(404)

###############################################################################
###############################################################################

    def do_DELETE(self):
        """Used to deregister the sim with an ADM"""

        if 'deregisterADM' in parse_qs(urlparse(self.path).query):
            admIp = parse_qs(urlparse(self.path).query)['deregisterADM'][0]

            if 'registrationRef' in parse_qs(urlparse(self.path).query):
                registrationRef = ' registrationRef="' + parse_qs(urlparse(self.path).query)['registrationRef'][0] + '"'
                print "Attempting to deregister" + registrationRef + " from ADM at " + admIp
            else:
                registrationRef = ''
                print "Attempting to deregister identity " + IDENTITY + " from ADM at " + admIp

            payload = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:adm="http://www.scte.org/schemas/130-3/2008a/adm" xmlns:core="http://www.scte.org/schemas/130-2/2008a/core">
   <soapenv:Header/>
   <soapenv:Body>
      <adm:ADSDeregisterRequest messageId="%s" version="1.1" identity="%s" system="%s"%s/>
   </soapenv:Body>
</soapenv:Envelope>""" % (str(uuid.uuid1()), IDENTITY, SYSTEM, registrationRef)

            print "Sending >>>>>>>>>>\n", payload

            #de-register with ADM
            #admPort and url hardcoded for now
            url = 'http://%s:8282/adm/2010' % (admIp)
            r = requests.post(url, payload, headers={'content-type': 'text/xml;charset=UTF-8'}, timeout=10)

            print "\nResponse from ADM: <<<<<<<<<<\n" + r.text

            #return just status code from ADM since this delete is an 'internal' call
            self.send_response(r.status_code)

            if r.text.find('StatusCode class="0"') >= 0:
                print "**********De-registration SUCCESSFUL!"
            else:
                print "**********De-registration FAILED!"

        else:
            #assumes that any delete method call is to deregister
            print "ERROR: deregisterADM query parameter not passed"
            self.send_response(400)

        print "-----------------------------------------------"

 ##############################################################################
 ##############################################################################

    def do_POST(self):
        """SCTE130 calls the sim needs to handle
        It is assumed anything posted to the sim is XML"""

        ####INCOMING REQUEST####
        #self.log_message("Command: %s" % self.command)
        #self.log_message("Path: %s" % self.path)
        #self.log_message("Headers: %r" % self.headers.items())

        #time.clock() did not work correctly on linux but was fine on windows.  time.time() worked on both
        #start = time.clock()
        start = time.time()

        time.sleep(DELAY / 1000.0)

        #http header field names are case insensitive
        if self.headers.getheader('content-length') != None:
            request = self.rfile.read(int(self.headers.getheader('content-length')))
        elif self.headers.getheader('transfer-encoding') == 'chunked':
            #http://en.wikipedia.org/wiki/Chunked_transfer_encoding
            request = ''
            chunkSize = int(self.rfile.readline().strip(), 16)
            while chunkSize != 0:
                request += self.rfile.read(chunkSize)
                #need to read \r\n since this isn't counted in chunk size
                self.rfile.read(2)
                chunkSize = int(self.rfile.readline().strip(), 16)
        else:
            self.send_response(400)
            return

        if DEBUG:
            print "\nRequest from " + str(self.client_address[0]) + ":" + str(self.client_address[1]) + " <<<<<<<<<<<<<<<\n"
            if self.headers.getheader('content-type').find('/xml') >= 0:
                print minidom.parseString(request).toprettyxml()
            else:
                print request

###############################################################################

        if request.find("ServiceCheckRequest ") >= 0:
            if DEBUG:
                print "\n*ServiceCheckRequest*"
            #outgoing response needs messageRef set to incoming request's messageId
            messageId = request.split('messageId="')[1].split('"')[0]
            response = """<soap-env:Envelope xmlns:soap-env="http://schemas.xmlsoap.org/soap/envelope/"><soap-env:Header/><soap-env:Body><core:ServiceCheckResponse xmlns:core="http://www.scte.org/schemas/130-2/2008a/core" identity="%s" messageId="%s" messageRef="%s" system="%s" version="1.1"><core:StatusCode class="0"/></core:ServiceCheckResponse></soap-env:Body></soap-env:Envelope>""" % (IDENTITY, str(uuid.uuid1()), messageId, SYSTEM)

###############################################################################

        elif request.find("ADSDeregistrationNotification ") >= 0:
            if DEBUG:
                print "\n*ADSDeregistrationNotification*"

            global lastDeregistrationNotification
            lastDeregistrationNotification = request

            #outgoing response needs messageRef set to incoming request's messageId
            messageId = request.split('messageId="')[1].split('"')[0]

            #registrationRef is optional
            if request.find("registrationRef=") >= 0:
                registrationRef = request.split('registrationRef="')[1].split('"')[0]
                if DEBUG:
                    print "ADM deregistered " + registrationRef
            else:
                if DEBUG:
                    print "You didn't send me a registrationRef."

            response = """<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Header/><soap:Body><adm:ADSDeregistrationAcknowledgement messageId="%s" version="1.1" identity="%s" system="%s" messageRef="%s" xmlns:adm="http://www.scte.org/schemas/130-3/2008a/adm"><core:StatusCode class="0" xmlns:core="http://www.scte.org/schemas/130-2/2008a/core"/></adm:ADSDeregistrationAcknowledgement></soap:Body></soap:Envelope>""" % (str(uuid.uuid1()), IDENTITY, SYSTEM, messageId)

###############################################################################

        elif request.find("PlacementStatusNotification ") >= 0:
            if DEBUG:
                print "\n*PlacementStatusNotification*"

            global lastPlacementStatusNotification
            lastPlacementStatusNotification = request

            global negativePlacementStatusAck
            global negativePlacementStatusAckCount
            if negativePlacementStatusAck:
                statusCodeClass = "1"
                negativePlacementStatusAckCount = negativePlacementStatusAckCount - 1
                if negativePlacementStatusAckCount == 0:
                    negativePlacementStatusAck = False
                print negativePlacementStatusAckCount, "negative PlacementStatusAck(s) remaining"
            else:
                statusCodeClass = "0"

            global psnAckSleep
            time.sleep(psnAckSleep)
            psnAckSleep = 0

            #outgoing response needs messageRef set to incoming request's messageId
            messageId = request.split('messageId="')[1].split('"')[0]
            response = """<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Header/><soap:Body><adm:PlacementStatusAcknowledgement messageId="%s" version="1.1" identity="%s" system="%s" messageRef="%s" xmlns:adm="http://www.scte.org/schemas/130-3/2008a/adm"><core:StatusCode class="%s" xmlns:core="http://www.scte.org/schemas/130-2/2008a/core"/></adm:PlacementStatusAcknowledgement></soap:Body></soap:Envelope>""" % (str(uuid.uuid1()), IDENTITY, SYSTEM, messageId, statusCodeClass)

###############################################################################

        elif request.find("ServiceStatusNotification ") >= 0:
            if DEBUG:
                print "\n*ServiceStatusNotification*"

            global lastServiceStatusNotification
            lastServiceStatusNotification = request

            #outgoing response needs messageRef set to incoming request's messageId
            messageId = request.split('messageId="')[1].split('"')[0]
            response = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:core="http://www.scte.org/schemas/130-2/2008a/core"><soapenv:Header/><soapenv:Body><core:ServiceStatusAcknowledgement messageId="%s" version="1.1" identity="%s" system="%s" messageRef="%s"><core:StatusCode class="0"/></core:ServiceStatusAcknowledgement></soapenv:Body></soapenv:Envelope>""" % (str(uuid.uuid1()), IDENTITY, SYSTEM, messageId)

###############################################################################

        elif request.find("PlacementRequest ") >= 0:

            #for right now, the PlacementResponse matches the behavior seen with the BA PIE
            #it features a single preroll, two midroll/interstitial placements, and a single postroll
            #the ads are hardcoded would be easy to use the adPlacements dictionary populated
            #using assets.csv with the readAdPlacements() method

            if DEBUG:
                print "\n*PlacementRequest*"

            global lastPlacementRequest
            global lastPlacementResponse
            global repeatAds
            lastPlacementRequest = request

            messageId = request.split('messageId="')[1].split('"')[0]

            #parse opportunity id and don't assume it's the first attribute
            poId = request.split('PlacementOpportunity ')[1].split('id="')[1].split('"')[0]

            #assumes TerminalAddress if present is of type MAC
            if request.find('TerminalAddress type="MAC">') >= 0:
                mac = request.split('TerminalAddress type="MAC">')[1].split('<')[0]
                client = '\n    <ns2:Client><ns2:TerminalAddress type="MAC">%s</ns2:TerminalAddress></ns2:Client>' % (mac)
            else:
                client = ""

            #parsing AssetRef element to dig out providerID and assetID
            assetRef = request.split('AssetRef ')[1].split('>')[0]
            assetId = assetRef.split('assetID="')[1].split('"')[0]
            providerId = assetRef.split('providerID="')[1].split('"')[0]

            if dynamicAdPlacements:
                response = buildPlacementResponse(IDENTITY, SYSTEM, messageId, poId, providerId, assetId, numPreroll, numMidroll, numPostroll)
            elif repeatAds:
                response = """<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
    <S:Body>
            <ns2:PlacementResponse xmlns="http://www.scte.org/schemas/130-2/2008a/core" xmlns:ns2="http://www.scte.org/schemas/130-3/2008a/adm" xmlns:ns3="http://www.scte.org/schemas/130-8/2010a/gis" xmlns:ns4="http://www.scte.org/schemas/130-3/2008a/adm/podm" identity="%s" messageId="%s" messageRef="%s" system="%s" version="1.1">
    <StatusCode class="0" />%s
    <ns2:PlacementDecision id="%s" placementOpportunityRef="%s">
        <ns2:OpportunityBinding breakOpportunitiesExpected="1" breakOpportunitySequence="1" opportunityNumber="1" opportunityType="Preroll" poGroupIndex="1" />
        <ns2:Placement action="replace" id="%s" position="1">
            <Content>
                <AssetRef assetID="repeated_ad" providerID="AdvertiserF01" />
                <Tracking>%s</Tracking>
            </Content>
            <ns2:PlacementConstraints trickModeRestriction="fastForward"/>
        </ns2:Placement>
    </ns2:PlacementDecision>
    <ns2:PlacementDecision id="%s" placementOpportunityRef="%s">
        <ns2:Entertainment poid="1.1,action:replace,npt1:00:00:00.000-00:00:30.000,npt2:00:10:00.000-00:10:30.000">
            <Content>
                <AssetRef assetID="%s" providerID="%s" />
            </Content>
            <ns2:EntertainmentNPT scale="1.0">30.000-600.000</ns2:EntertainmentNPT>
        </ns2:Entertainment>
    </ns2:PlacementDecision>
    <ns2:PlacementDecision id="%s" placementOpportunityRef="%s">
        <ns2:OpportunityBinding breakOpportunitiesExpected="2" breakOpportunitySequence="1:2" opportunityNumber="2" opportunityType="Midroll" poGroupIndex="2" />
        <ns2:Placement action="replace" id="%s" position="2">
            <Content>
                <AssetRef assetID="repeated_ad" providerID="AdvertiserF01" />
                <Tracking>%s</Tracking>
            </Content>
            <ns2:PlacementConstraints trickModeRestriction="rewind,pause,fastForward"/>
        </ns2:Placement>
        <ns2:Placement action="replace" id="%s" position="3">
            <Content>
                <AssetRef assetID="repeated_ad" providerID="AdvertiserF01" />
                <Tracking>%s</Tracking>
            </Content>
        </ns2:Placement>
    </ns2:PlacementDecision>
    <ns2:PlacementDecision id="%s" placementOpportunityRef="%s">
        <ns2:Entertainment poid="3.1,action:replace,npt1:00:10:00.000-00:10:30.000,npt2:00:30:00.000-00:30:00.000">
            <Content>
                <AssetRef assetID="%s" providerID="%s" />
            </Content>
            <ns2:EntertainmentNPT scale="1.0">630.000-1800.000</ns2:EntertainmentNPT>
        </ns2:Entertainment>
    </ns2:PlacementDecision>
    <ns2:PlacementDecision id="%s" placementOpportunityRef="%s">
        <ns2:OpportunityBinding breakOpportunitiesExpected="1" breakOpportunitySequence="1" opportunityNumber="4" opportunityType="Postroll" poGroupIndex="99" />
        <ns2:Placement action="replace" id="%s" position="4">
            <Content>
                <AssetRef assetID="repeated_ad" providerID="AdvertiserF01" />
                <Tracking>%s</Tracking>
            </Content>
        </ns2:Placement>
    </ns2:PlacementDecision>
</ns2:PlacementResponse>
    </S:Body>
</S:Envelope>""" % (IDENTITY, str(uuid.uuid1()), messageId, SYSTEM, client, str(uuid.uuid1()), poId, str(uuid.uuid1()), str(uuid.uuid1()), str(uuid.uuid1()), poId, assetId, providerId, str(uuid.uuid1()), poId, str(uuid.uuid1()), str(uuid.uuid1()), str(uuid.uuid1()), str(uuid.uuid1()), str(uuid.uuid1()), poId, assetId, providerId, str(uuid.uuid1()), poId, str(uuid.uuid1()), str(uuid.uuid1()))
                repeatAds = False
            else:
                response = """<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
    <S:Body>
            <ns2:PlacementResponse xmlns="http://www.scte.org/schemas/130-2/2008a/core" xmlns:ns2="http://www.scte.org/schemas/130-3/2008a/adm" xmlns:ns3="http://www.scte.org/schemas/130-8/2010a/gis" xmlns:ns4="http://www.scte.org/schemas/130-3/2008a/adm/podm" identity="%s" messageId="%s" messageRef="%s" system="%s" version="1.1">
    <StatusCode class="0" />%s
    <ns2:PlacementDecision id="%s" placementOpportunityRef="%s">
        <ns2:OpportunityBinding breakOpportunitiesExpected="1" breakOpportunitySequence="1" opportunityNumber="1" opportunityType="Preroll" poGroupIndex="1" />
        <ns2:Placement action="replace" id="%s" position="1">
            <Content>
                <AssetRef assetID="AdvertiserF01_creative_pre_30s_movie" providerID="AdvertiserF01" />
                <Tracking>%s</Tracking>
            </Content>
            <ns2:PlacementConstraints trickModeRestriction="fastForward"/>
        </ns2:Placement>
    </ns2:PlacementDecision>
    <ns2:PlacementDecision id="%s" placementOpportunityRef="%s">
        <ns2:Entertainment poid="1.1,action:replace,npt1:00:00:00.000-00:00:30.000,npt2:00:10:00.000-00:10:30.000">
            <Content>
                <AssetRef assetID="%s" providerID="%s" />
            </Content>
            <ns2:EntertainmentNPT scale="1.0">30.000-600.000</ns2:EntertainmentNPT>
        </ns2:Entertainment>
    </ns2:PlacementDecision>
    <ns2:PlacementDecision id="%s" placementOpportunityRef="%s">
        <ns2:OpportunityBinding breakOpportunitiesExpected="2" breakOpportunitySequence="1:2" opportunityNumber="2" opportunityType="Midroll" poGroupIndex="2" />
        <ns2:Placement action="replace" id="%s" position="2">
            <Content>
                <AssetRef assetID="AdvertiserF01_creative_mid1_30s_movie" providerID="AdvertiserF01" />
                <Tracking>%s</Tracking>
            </Content>
            <ns2:PlacementConstraints trickModeRestriction="rewind,pause,fastForward"/>
        </ns2:Placement>
        <ns2:Placement action="replace" id="%s" position="3">
            <Content>
                <AssetRef assetID="AdvertiserF01_creative_mid2_30s_movie" providerID="AdvertiserF01" />
                <Tracking>%s</Tracking>
            </Content>
        </ns2:Placement>
    </ns2:PlacementDecision>
    <ns2:PlacementDecision id="%s" placementOpportunityRef="%s">
        <ns2:Entertainment poid="3.1,action:replace,npt1:00:10:00.000-00:10:30.000,npt2:00:30:00.000-00:30:00.000">
            <Content>
                <AssetRef assetID="%s" providerID="%s" />
            </Content>
            <ns2:EntertainmentNPT scale="1.0">630.000-1800.000</ns2:EntertainmentNPT>
        </ns2:Entertainment>
    </ns2:PlacementDecision>
    <ns2:PlacementDecision id="%s" placementOpportunityRef="%s">
        <ns2:OpportunityBinding breakOpportunitiesExpected="1" breakOpportunitySequence="1" opportunityNumber="4" opportunityType="Postroll" poGroupIndex="99" />
        <ns2:Placement action="replace" id="%s" position="4">
            <Content>
                <AssetRef assetID="AdvertiserF01_creative_post_30s_movie" providerID="AdvertiserF01" />
                <Tracking>%s</Tracking>
            </Content>
        </ns2:Placement>
    </ns2:PlacementDecision>
</ns2:PlacementResponse>
    </S:Body>
</S:Envelope>""" % (IDENTITY, str(uuid.uuid1()), messageId, SYSTEM, client, str(uuid.uuid1()), poId, str(uuid.uuid1()), str(uuid.uuid1()), str(uuid.uuid1()), poId, assetId, providerId, str(uuid.uuid1()), poId, str(uuid.uuid1()), str(uuid.uuid1()), str(uuid.uuid1()), str(uuid.uuid1()), str(uuid.uuid1()), poId, assetId, providerId, str(uuid.uuid1()), poId, str(uuid.uuid1()), str(uuid.uuid1()))

            lastPlacementResponse = response

###############################################################################

        else:
            print "\n??????? Unrecognized request ???????\n"
            self.send_response(400)
            return

###########################OUTGOING RESPONSE###################################

        if soapRequestSleep:
            time.sleep(20)

        self.server_version = "Python ADSSim"
        self.sys_version = ""

        self.send_response(200)
        self.send_header("Content-Type", 'text/xml;charset="utf-8"')
        self.send_header("Content-Length", len(response))
        self.end_headers()

        self.wfile.write(response)

        #end = time.clock()
        end = time.time()
        print int((end - start) * 1000), "ms"

        if DEBUG:
            print "\nSending >>>>>>>>>>\n", response
            print "-----------------------------------------------\n"

###############################################################################
###############################################################################

class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
    """Handle requests in a separate thread."""

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", dest="ip", required=True)
    parser.add_argument("--port", dest="port", required=True, type=int)
    parser.add_argument("--identity", dest="identity", default="PyADSSim")
    parser.add_argument("--system", dest="system", default="ads")
    parser.add_argument("--delayms", dest="delayms", default=0, type=int, help="ms delay before processing HTTP POST requests")
    parser.add_argument("--debug", action="store_true", dest="debug", default=False, help="enable debug logging of HTTP POST which is normally quieted for load/stress testing")
    args = parser.parse_args()
    IP = args.ip
    PORT = args.port
    IDENTITY = args.identity
    SYSTEM = args.system
    DELAY = args.delayms
    DEBUG = args.debug

    adPlacements = {}
    readAdPlacements()
    lastPlacementRequest = ''
    lastPlacementResponse = ''
    lastPlacementStatusNotification = ''
    lastDeregistrationNotification = ''
    lastServiceStatusNotification = ''
    negativePlacementStatusAck = False
    negativePlacementStatusAckCount = 0
    psnAckSleep = 0
    soapRequestSleep = False
    repeatAds = False

    dynamicAdPlacements = False
    numPreroll = 0
    numMidroll = 0
    numPostroll = 0

    server = ThreadedHTTPServer((IP, PORT), ADSSim)
    print 'Starting server, use <Ctrl-C> to stop.'
    server.serve_forever()
