"""admcore sim for trick mode validation"""

from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import SocketServer
from urlparse import urlparse
import argparse
import csv
import json
import logging
#import logging.handlers
import time
import uuid


def load_ads():
    """load ads from csv file

    format: provider_id,asset_id[,trick_mode1,trick_mode2...]
    ad1_provider_id,ad1_asset_id
    ad2_provider_id,ad2_asset_id,FF,REW
    """
    try:
        with open("ads.csv", "rb") as f:
            del ads[:]
            reader = csv.reader(f)
            for row in reader:
                #filter rows with < 2 fields
                if len(row) >= 2:
                    ads.append(row)
        my_logger.debug("Parsed ads.csv.  ads:" + str(ads))
    except IOError:
        my_logger.warning("Could not find ./ads.csv")
    return

def build_segment(provider_id, asset_id, seg_type, disabled_tricks=None, start_npt="BOS", end_npt="EOS"):
    """create segment to be placed in response"""
    seg = dict()
    seg['id'] = str(uuid.uuid4())
    seg['assetId'] = asset_id
    seg['providerId'] = provider_id
    seg['startNpt'] = str(start_npt)
    seg['endNpt'] = str(end_npt)
    seg['resumeSeg'] = False
    seg['segmentType'] = seg_type
    seg['contentRequirement'] = "REQUIRED"
    seg['disabledTrickModes'] = list()
    if disabled_tricks:
        seg['disabledTrickModes'].extend(disabled_tricks)
    #seg['orderNum'] = 0
    seg['trackingId'] = str(uuid.uuid4())
    return seg

def interstitial_segments(feature_provider_id, feature_asset_id, inter_ads):
    """segments for feature with interstitial ad placements"""
    segs = list()
    if inter_ads:
        segs.append(build_segment(feature_provider_id, feature_asset_id, "FEATURE", end_npt=30000))
        for ad in inter_ads:
            segs.append(build_segment(ad[0], ad[1], "INTERSTITIAL", ad[2:]))
        segs.append(build_segment(feature_provider_id, feature_asset_id, "FEATURE", start_npt=30000))
    else:
        # no interstitial ads so just include the feature BOS to EOS
        segs.append(build_segment(feature_provider_id, feature_asset_id, "FEATURE"))
    return segs


class AdmCoreSim(BaseHTTPRequestHandler):
    """admcore sim"""

    def do_GET(self):
        """sim control"""
        global preroll_only
        global interstitial_only
        global postroll_only

        parsed_path = urlparse(self.path)
        if parsed_path.path == "/reloadAds":
            load_ads()
        elif parsed_path.path == "/prerollOnly":
            preroll_only, interstitial_only, postroll_only = True, False, False
        elif parsed_path.path == "/interstitialOnly":
            preroll_only, interstitial_only, postroll_only = False, True, False
        elif parsed_path.path == "/postrollOnly":
            preroll_only, interstitial_only, postroll_only = False, False, True
        elif parsed_path.path == "/reset":
            load_ads()
            preroll_only, interstitial_only, postroll_only = False, False, False
        else:
            self.send_error(404)
            return

        response_body = "0"
        self.send_response(200)
        self.send_header("Content-Type", 'text/plain')
        self.send_header("Content-Length", len(response_body))
        self.end_headers()
        self.wfile.write(response_body)
        return

    def do_POST(self):
        """ad placements and ad playout reporting"""
        content_len = self.headers.getheader('content-length')
        if content_len is None:
            self.send_error(400, "no Content-Length header found.  sim does not handle chunking")
            return
        request_body = self.rfile.read(int(content_len))
        my_logger.debug("Received POST with message body: " + request_body)

        parsed_path = urlparse(self.path)

        if parsed_path.path == "/api/v1/playlist":
            segment_request = json.loads(request_body)

            segment_response = dict()
            segment_response['sessionId'] = segment_request['sessionId']
            segment_response['admHost'] = args.ip
            segment_response['region'] = segment_request['region']
            segment_response['createdAt'] = time.time() * 1000
            segment_response['performanceStats'] = {'executionTime': 1, 'databaseTime': 2, 'adsTime': 3}
            segment_response['segments'] = list()

            if preroll_only:
                for ad in ads:
                    preroll = build_segment(ad[0], ad[1], "PREROLL", ad[2:])
                    segment_response['segments'].append(preroll)
                feature = build_segment(segment_request['providerId'], segment_request['movieAssetId'], "FEATURE")
                segment_response['segments'].append(feature)

            elif interstitial_only:
                int_segs = interstitial_segments(segment_request['providerId'], segment_request['movieAssetId'], ads)
                segment_response['segments'].extend(int_segs)

            elif postroll_only:
                feature = build_segment(segment_request['providerId'], segment_request['movieAssetId'], "FEATURE")
                segment_response['segments'].append(feature)
                for ad in ads:
                    postroll = build_segment(ad[0], ad[1], "POSTROLL", ad[2:])
                    segment_response['segments'].append(postroll)

            else:
                #segments- preroll ads, feature with interstitial ads, postroll ads.
                #ads in ads.csv are alternated preroll, interstitial, postroll.  ads[::3], ads[1::3], ads[2::3] respectively.

                #preroll ads
                for ad in ads[::3]:
                    preroll = build_segment(ad[0], ad[1], "PREROLL", ad[2:])
                    segment_response['segments'].append(preroll)

                #feature with interstitial ad placements
                int_segs = interstitial_segments(segment_request['providerId'], segment_request['movieAssetId'], ads[1::3])
                segment_response['segments'].extend(int_segs)

                #postroll ads
                for ad in ads[2::3]:
                    postroll = build_segment(ad[0], ad[1], "POSTROLL", ad[2:])
                    segment_response['segments'].append(postroll)

            #easiest to set orderNum after segment list built
            for idx, seg in enumerate(segment_response['segments']):
                #orderNum starts at 1
                seg['orderNum'] = idx+1

            response_body = json.dumps(segment_response)
            self.send_response(201)
            self.send_header("Content-Type", "application/json")
            self.send_header("Content-Length", len(response_body))
            self.end_headers()
            self.wfile.write(response_body)

        elif parsed_path.path == "/api/v1/playlist/events":
            events_request = json.loads(request_body)

            events_response = dict()
            events_response['sessionId'] = events_request['sessionId']
            events_response['trickModeEvents'] = events_request['trickModeEvents']
            events_response['processStatus'] = "SUCCESS"
            events_response['processStatusEvents'] = [{'status': "SUCCESS", 'message': "Sim just ACK's", 'eventTime': time.time() * 1000}]
            events_response['createdAt'] = time.time() * 1000
            events_response['performanceStats'] = {'executionTime': 1, 'databaseTime': 2, 'adsTime': 3}
            events_response['region'] = events_request['region']

            response_body = json.dumps(events_response)
            self.send_response(200)
            self.send_header("Content-Type", "application/json")
            self.send_header("Content-Length", len(response_body))
            self.end_headers()
            self.wfile.write(response_body)
        else:
            self.send_error(404)
        return


class ThreadedAdmCoreSim(ThreadingMixIn, HTTPServer):
    """multithread"""

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="admcore simulator", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--ip", default="localhost", help="ip for the sim to bind")
    parser.add_argument("--port", type=int, default=1234, help="open port on ip for sim to bind")
    args = parser.parse_args()

    #setup rotating log file, (3) 1 MB files
    #nohup <start script with options> >/dev/null 2>&1 &
    my_logger = logging.getLogger('MyLogger')
    my_logger.setLevel(logging.DEBUG)
    handler = logging.handlers.RotatingFileHandler('admcore_sim.log', maxBytes=1048576, backupCount=3)
    handler.setLevel(logging.DEBUG)
    logging.Formatter.converter = time.gmtime
    #log format: timestamp|log level|thread id|message
    formatter = logging.Formatter('%(asctime)s.%(msecs)03dZ|%(levelname)s|%(thread)d|%(message)s', '%Y-%m-%dT%H:%M:%S')
    handler.setFormatter(formatter)
    my_logger.addHandler(handler)

    ads = []
    preroll_only, interstitial_only, postroll_only = False, False, False
    load_ads()

    try:
        server = ThreadedAdmCoreSim((args.ip, args.port), AdmCoreSim)
        my_logger.info("Started httpsim at http://{}:{}".format(args.ip, args.port))
        server.serve_forever()
    except KeyboardInterrupt:
        my_logger.info("^C received, stopping server")
        server.server_close()
