FROM ubuntu:latest
FROM python:2.7

EXPOSE 8080

WORKDIR /Simulators/ADSSim5

COPY admcore_sim.py .
COPY ads.csv .
COPY ADSSim.py .
COPY ADSSimRESTART.py .
COPY ADSSimWSDL.py .
COPY assets.csv .
COPY mdmsRtspClient.py .
COPY requirements.txt .
COPY simRestartADSpythonSim5_m229.sh /Simulators/ADSSim5


RUN pip install --upgrade pip 
RUN pip install Werkzeug --upgrade
RUN pip install --no-cache-dir -r requirements.txt 







ENTRYPOINT ["python","ADSSim.py","--ip","0.0.0.0","--port","8080"]