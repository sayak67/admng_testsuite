"""Script to simulate a simple ADS plus helper methods for automated functional testing"""

from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
from SocketServer import ThreadingMixIn
from urlparse import urlparse, parse_qs

import argparse
import uuid
import requests
import json
import time
import logging
import logging.handlers

from xml.dom import minidom

def readAdPlacements():
    """read assets.csv file containing ad placements
    first value is the provider id followed by a list of paids which are the ads
    to be placed into content for that provider id
    Example: providerA,adprovider1::adasset1,adprovider2::adasset2
    """

    try:
        with open("assets.csv", "r") as f:
            adPlacements.clear()
            for line in f:
                line = line.strip()
                adPlacements[line.split(',')[0]] = line.split(',')[1:]
        my_logger.debug("Parsed assets.csv.  adPlacements:" + str(adPlacements))
    except IOError:
        my_logger.warning("Could not find ./assets.csv")

###############################################################################

def buildPlacementDecision(numAds, placementOpportunityId, providerId, opportunityType):
    """build PlacementDecision element for ads"""

    if providerId in adPlacements:
        ads = adPlacements[providerId]
    else:
        ads = ['adsrus.com::ADAD1234123412341234']

    #opportunityType attribute is not required according to SCTE130-3 but ADMCore
    #parses it to be included in the playlist response.  It was present in the example from
    # the BlackArrow PIE.
    beginning = """\n<ns2:PlacementDecision id="%s" placementOpportunityRef="%s"><ns2:OpportunityBinding opportunityType="%s" />""" % (str(uuid.uuid1()), placementOpportunityId, opportunityType)
    end = """\n</ns2:PlacementDecision>"""
    placements = ""

    if numAds > 0:
        for i in range(numAds):
            indexOfAd = (len(ads)+i) % len(ads)
            if ads[indexOfAd].split('::')[1] == "":
                break
            temp = """\n    <ns2:Placement action="replace" id="%s">
        <Content>
            <AssetRef assetID="%s" providerID="%s" />
            <Tracking>%s</Tracking>
        </Content>
    </ns2:Placement>""" % (str(uuid.uuid1()), ads[indexOfAd].split('::')[1], ads[indexOfAd].split('::')[0], str(uuid.uuid1()))
            placements = placements + temp

    return beginning + placements + end

###############################################################################

def buildEntertainmentPlacementDecision(providerId, assetId, startNpt, endNpt, placementOpportunityId):
    """build PlacementDecision element for entertainment"""

    beginning = """\n<ns2:PlacementDecision id="%s" placementOpportunityRef="%s">""" % (str(uuid.uuid1()), placementOpportunityId)
    end = """\n</ns2:PlacementDecision>"""

    #build optional entertainmentNpt if not playing in entirety
    startNpt = str(startNpt)
    endNpt = str(endNpt)
    if startNpt == "BOS" and endNpt == "EOS":
        entertainmentNpt = ""
    else:
        entertainmentNpt = """\n        <ns2:EntertainmentNPT scale="1.0">%s-%s</ns2:EntertainmentNPT>""" % (startNpt, endNpt)

    entertainment = """\n    <ns2:Entertainment>
        <Content>
            <AssetRef assetID="%s" providerID="%s" />
        </Content>%s
    </ns2:Entertainment>""" % (assetId, providerId, entertainmentNpt)

    return beginning + entertainment + end

###############################################################################
#Manojna's code
def buildFaultyPlacementResponse(identity, system, messageId, poId, providerId, assetId, serviceGroup, sessionNum, currentDate, mac, postalCode, serviceReg, requestT):

    beginning = """<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
    <S:Body>
        <adm:PlacementResponse xmlns:core="http://www.scte.org/schemas/130-2/2008a/core" xmlns:adm="http://www.scte.org/schemas/130-3/2008a/adm" xmlns:gis="http://www.scte.org/schemas/130-8/2010a/gis" xmlns:podm="http://www.scte.org/schemas/130-3/2008a/adm/podm" identity="%s" messageId="%s" messageRef="%s" system="%s" version="1.1">
    <core:StatusCode class="0" />""" % (identity, str(uuid.uuid1()), messageId, system)
    end = """\n        </adm:PlacementResponse>
    </S:Body>
</S:Envelope>"""
    mid = """\n<adm:SystemContext> 
        <adm:ServiceGroup>%s</adm:ServiceGroup>
        <adm:Session>%s</adm:Session>
    </adm:SystemContext>
    <adm:Client>
        <core:CurrentDateTime>%s</core:CurrentDateTime>
        <adm:TerminalAddress type="MAC">%s</adm:TerminalAddress>
        <adm:TargetCode key="postalCodes">%s</adm:TargetCode>
        <adm:TargetCode key="serviceRegion">%s</adm:TargetCode>
        <adm:TargetCode key="requestType">%s</adm:TargetCode>
    </adm:Client>""" %(serviceGroup, sessionNum, currentDate, mac, postalCode, serviceReg, requestT)

    return beginning + mid + end

###############################################################################
#Manojna's code
def buildErrorResponse(identity, system, messageId, poId, providerId, assetId):

    beginning = """<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
    <S:Body>
        <ns2:PlacementResponse xmlns="http://www.scte.org/schemas/130-2/2008a/core" xmlns:ns2="http://www.scte.org/schemas/130-3/2008a/adm" xmlns:ns3="http://www.scte.org/schemas/130-8/2010a/gis" xmlns:ns4="http://www.scte.org/schemas/130-3/2008a/adm/podm" identity="%s" messageId="%s" messageRef="%s" system="%s" version="1.1">
    <StatusCode class="1" />""" % (identity, str(uuid.uuid1()), messageId, system)
    end = """\n        </ns2:PlacementResponse>
    </S:Body>
</S:Envelope>"""
    errorStatusCode = False
    return beginning+end

###############################################################################
#Manojna's code
def buildEmptyPlacementDecisionResponse(identity, system, messageId, poId, providerId, assetId):

    beginning = """<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
    <S:Body>
        <ns2:PlacementResponse xmlns="http://www.scte.org/schemas/130-2/2008a/core" xmlns:ns2="http://www.scte.org/schemas/130-3/2008a/adm" xmlns:ns3="http://www.scte.org/schemas/130-8/2010a/gis" xmlns:ns4="http://www.scte.org/schemas/130-3/2008a/adm/podm" identity="%s" messageId="%s" messageRef="%s" system="%s" version="1.1">
    <StatusCode class="0" />""" % (identity, str(uuid.uuid1()), messageId, system)
    end = """\n        </ns2:PlacementResponse>
    </S:Body>
</S:Envelope>"""

    emptyPlacementDecision = False

    return beginning+end

###############################################################################
#Manojna's code
def buildTopLevelEntertainmentResponse(identity, system, messageId, poId, providerId, assetId):

    beginning = """<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
    <S:Body>
        <ns2:PlacementResponse xmlns="http://www.scte.org/schemas/130-2/2008a/core" xmlns:ns2="http://www.scte.org/schemas/130-3/2008a/adm" xmlns:ns3="http://www.scte.org/schemas/130-8/2010a/gis" xmlns:ns4="http://www.scte.org/schemas/130-3/2008a/adm/podm" identity="%s" messageId="%s" messageRef="%s" system="%s" version="1.1">
    <StatusCode class="0" />""" % (identity, str(uuid.uuid1()), messageId, system)
    startNpt = 20;
    endNpt = 30;
    entertainmentNpt = """\n        <ns2:EntertainmentNPT scale="1.0">%s-%s</ns2:EntertainmentNPT>""" % (startNpt, endNpt)

    entertainment = """\n    <ns2:Entertainment>
        <Content>
            <AssetRef assetID="%s" providerID="%s" />
        </Content>%s
    </ns2:Entertainment>""" % (assetId, providerId, entertainmentNpt)


    end = """\n        </ns2:PlacementResponse>
    </S:Body>
</S:Envelope>"""
    return beginning+entertainment+end

###############################################################################
#Manojna's code
def buildNullPlacementResponse(identity, system, messageId, placementOpportunityId, providerId, assetId):

    #     beginning = """<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
    #     <S:Body>"""
    #     end = """\n
    #     </S:Body>
    # </S:Envelope>"""
    end = ""
    return end

###############################################################################
###############################################################################

def buildPlacementResponse(identity, system, messageId, placementOpportunityId, providerId, assetId):
    """build entire PlacementRequest"""

    beginning = """<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
    <S:Body>
        <ns2:PlacementResponse xmlns="http://www.scte.org/schemas/130-2/2008a/core" xmlns:ns2="http://www.scte.org/schemas/130-3/2008a/adm" xmlns:ns3="http://www.scte.org/schemas/130-8/2010a/gis" xmlns:ns4="http://www.scte.org/schemas/130-3/2008a/adm/podm" identity="%s" messageId="%s" messageRef="%s" system="%s" version="1.1">
    <StatusCode class="0" />""" % (identity, str(uuid.uuid1()), messageId, system)
    end = """\n        </ns2:PlacementResponse>
    </S:Body>
</S:Envelope>"""

    if numPreroll > 0:
        preRoll = buildPlacementDecision(numPreroll, placementOpportunityId, providerId, "Preroll")
    else:
        preRoll = ""

    if numMidroll > 0:
        #currently only one midroll is supported with n ads
        midRoll = buildPlacementDecision(numMidroll, placementOpportunityId, providerId, "Midroll")
        entertainment1 = buildEntertainmentPlacementDecision(providerId, assetId, 0, 30, placementOpportunityId)
        entertainment2 = buildEntertainmentPlacementDecision(providerId, assetId, 30, 60, placementOpportunityId)
    else:
        midRoll = ""
        entertainment1 = buildEntertainmentPlacementDecision(providerId, assetId, "BOS", "EOS", placementOpportunityId)
        entertainment2 = ""

    if numPostroll > 0:
        postRoll = buildPlacementDecision(numPostroll, placementOpportunityId, providerId, "Postroll")
    else:
        postRoll = ""

    return beginning + preRoll + entertainment1 + midRoll + entertainment2 + postRoll + end

###############################################################################

def clearMessageCache():
    """clear the cache of last* messages the sim remembers"""

    global lastPlacementRequest
    global lastPlacementResponse
    global lastPlacementStatusNotification
    global lastDeregistrationNotification
    global lastServiceStatusNotification

    lastPlacementRequest = ''
    lastPlacementResponse = ''
    lastPlacementStatusNotification = ''
    lastDeregistrationNotification = ''
    lastServiceStatusNotification = ''

    my_logger.debug("Cleared last* message cache")

###############################################################################

def clearNegativeResponseSettings():
    """reset negative response settings"""

    global negativePlacementStatusAck
    global negativePlacementStatusAckCount
    global negativePlacementResponse
    global negativePlacementResponseCount
    global psnAckSleep
    global prSleep
    global soapRequestSleep
    global repeatAds
    global dynamicAdPlacements
    global numPreroll
    global numMidroll
    global numPostroll
    global faulty
    global nullPlacementResponse
    global errorStatusCode
    global emptyPlacementDecision
    global topLevelEntertainment
    global systemContextCheck

    negativePlacementStatusAck = False
    negativePlacementStatusAckCount = 0
    psnAckSleep = 0
    negativePlacementResponse = False
    negativePlacementResponseCount = 0
    prSleep = 0
    soapRequestSleep = False
    repeatAds = False
    dynamicAdPlacements = False
    faulty = False
    nullPlacementResponse = False
    emptyPlacementDecision = False
    errorStatusCode = False
    topLevelEntertainment = False
    systemContextCheck = False
    numPreroll = 0
    numMidroll = 0
    numPostroll = 0

    my_logger.debug("Reset negative response settings")

###############################################################################

class ADSSim(BaseHTTPRequestHandler):
    """Class to extend BaseHTTPRequestHandler to implement HTTP methods"""

    def do_GET(self):
        """Get method triggers registration with ADM, controls response behavior and returns some specific last requests/responses"""
        my_logger.info("Client request: " + self.path)

        if 'registerADM' in parse_qs(urlparse(self.path).query):
            #http://ip:port/whatever?registerADM=10.116.2.157
            #http://ip:port/whatever?registerADM=10.116.2.157&message=PlacementRequest

            clearMessageCache()
            clearNegativeResponseSettings()

            admIp = parse_qs(urlparse(self.path).query)['registerADM'][0]
            messageId = str(uuid.uuid1())

            if 'registrationIP' in parse_qs(urlparse(self.path).query):
                registrationIp = parse_qs(urlparse(self.path).query)['registrationIP'][0]
                my_logger.info("Registering sim for IP: " + registrationIp)
            else:
                registrationIp = IP

            if 'registrationPort' in parse_qs(urlparse(self.path).query):
                registrationPort = parse_qs(urlparse(self.path).query)['registrationPort'][0]
                my_logger.info("Registering sim for Port: " + registrationPort)
            else:
                registrationPort = str(PORT)

            if 'message' in parse_qs(urlparse(self.path).query):
                #if message query parameter included, add message attribute to Callout element
                message = ' message="' + parse_qs(urlparse(self.path).query)['message'][0] + '"'
                my_logger.info("Registering sim for " + message)
            else:
                message = ''

            payload = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:adm="http://www.scte.org/schemas/130-3/2008a/adm" xmlns:core="http://www.scte.org/schemas/130-2/2008a/core">
   <soapenv:Header/>
   <soapenv:Body>
      <adm:ADSRegistrationRequest system="%s" identity="%s" version="1.1" messageId="%s" xmlns:podm="http://www.scte.org/schemas/130-3/2008a/adm/podm" xmlns:ns4="http://www.scte.org/schemas/130-8/2010a/gis">
         <adm:ServiceDescription>
            <adm:SystemContext/>
            <adm:Service id="All ADM Offered Services"/>
         </adm:ServiceDescription>
         <core:Callout%s>
            <core:Address type="SOAP">http://%s:%s/2010/soap</core:Address>
         </core:Callout>
      </adm:ADSRegistrationRequest>
   </soapenv:Body>
</soapenv:Envelope>""" % (SYSTEM, IDENTITY, messageId, message, registrationIp, registrationPort)

            #admPort and url are hard coded
            url = 'http://%s:8282/adm/2010' % (admIp)
            my_logger.info("Registering with ADM at " + url)
            my_logger.debug("SCTE-130 ADSRegistrationRequest: " + payload)

            try:
                r = requests.post(url, payload, headers={'content-type': 'text/xml; charset=utf-8'}, timeout=10)
                my_logger.debug("Response from ADM: " + r.text)

                if 'StatusCode class="0"' in r.text:
                    my_logger.info("Registration successful. registrationRef=" + messageId)
                    temp = {"registrationRef" : messageId}
                    response = json.dumps(temp)
                    self.server_version = "Python ADSSim"
                    self.sys_version = ""
                    self.send_response(200)
                    self.send_header("Content-Type", 'application/json')
                    self.send_header("Content-Length", len(response))
                    self.end_headers()
                    self.wfile.write(response)
                else:
                    my_logger.warning("Registration FAILED")
                    self.send_response(409)
            except (requests.exceptions.ConnectionError, requests.exceptions.Timeout) as e:
                #http://docs.python-requests.org/en/latest/api/#requests.exceptions.RequestException
                my_logger.warning("Error sending ADSRegistrationRequest: " + str(type(e)) + str(e))
                self.send_response(502)

            ###############################################################################

        elif "lastPlacementRequest" in self.path:
            #return last PlacementRequest received
            response = lastPlacementRequest
            my_logger.debug("Last PlacementRequest received: " + response)
            self.server_version = "Python ADSSim"
            self.sys_version = ""
            self.send_response(200)
            self.send_header("Content-Type", 'text/xml;charset="utf-8"')
            self.send_header("Content-Length", len(response))
            self.end_headers()
            self.wfile.write(response)

        ###############################################################################

        elif "lastPlacementStatusNotification" in self.path:
            #return last PlacementStatusNotification received
            response = lastPlacementStatusNotification
            my_logger.debug("Last PlacementStatusNotification received: " + response)
            self.server_version = "Python ADSSim"
            self.sys_version = ""
            self.send_response(200)
            self.send_header("Content-Type", 'text/xml;charset="utf-8"')
            self.send_header("Content-Length", len(response))
            self.end_headers()
            self.wfile.write(response)

        ###############################################################################

        elif "lastPlacementResponse" in self.path:
            #return last PlacementResponse sent
            response = lastPlacementResponse
            my_logger.debug("Last PlacementResponse sent: " + response)
            self.server_version = "Python ADSSim"
            self.sys_version = ""
            self.send_response(200)
            self.send_header("Content-Type", 'text/xml;charset="utf-8"')
            self.send_header("Content-Length", len(response))
            self.end_headers()
            self.wfile.write(response)

        ###############################################################################

        elif "lastDeregistrationNotification" in self.path:
            #return last ADSDeregistrationNotification received
            response = lastDeregistrationNotification
            my_logger.debug("Last DeregistrationNotification received: " + response)
            self.server_version = "Python ADSSim"
            self.sys_version = ""
            self.send_response(200)
            self.send_header("Content-Type", 'text/xml;charset="utf-8"')
            self.send_header("Content-Length", len(response))
            self.end_headers()
            self.wfile.write(response)

        ###############################################################################

        elif "clearMessageCache" in self.path:
            #initiates clearing the cache of last* messages the sim remembers
            clearMessageCache()
            self.send_response(200)

        ###############################################################################

        elif "lastServiceStatusNotification" in self.path:
            #return last ServiceStatusNotification received
            response = lastServiceStatusNotification
            my_logger.debug("Last ServiceStatusNotification received: " + response)
            self.server_version = "Python ADSSim"
            self.sys_version = ""
            self.send_response(200)
            self.send_header("Content-Type", 'text/xml;charset="utf-8"')
            self.send_header("Content-Length", len(response))
            self.end_headers()
            self.wfile.write(response)

        ###############################################################################

        elif "negativePlacementStatusAck" in self.path:
            #tells sim to respond with StatusCode class=1 in PlacementStatusAcknowledgement
            global negativePlacementStatusAck
            global negativePlacementStatusAckCount
            negativePlacementStatusAck = True
            if 'count' in parse_qs(urlparse(self.path).query):
                negativePlacementStatusAckCount = int(parse_qs(urlparse(self.path).query)['count'][0])
            else:
                negativePlacementStatusAckCount = 1
            my_logger.info("Will send " + str(negativePlacementStatusAckCount) + " PlacementStatusAcknowledgement messages with StatusCode class=1")
            self.send_response(200)

        ###############################################################################

        elif "negativePlacementResponse" in self.path:
            #tells sim to respond with StatusCode class=1 in PlacementResponse
            global negativePlacementResponse
            global negativePlacementResponseCount
            negativePlacementResponse = True
            if 'count' in parse_qs(urlparse(self.path).query):
                negativePlacementResponseCount = int(parse_qs(urlparse(self.path).query)['count'][0])
            else:
                negativePlacementResponseCount = 1
            my_logger.info("Will send " + str(negativePlacementResponseCount) + " PlacementResponse messages with StatusCode class=1")
            self.send_response(200)

        ###############################################################################

        elif "slowPlacementStatusAck" in self.path:
            #tells sim to sleep before sending PlacementStatusAcknowledgement
            global psnAckSleep
            if 'duration' in parse_qs(urlparse(self.path).query):
                psnAckSleep = int(parse_qs(urlparse(self.path).query)['duration'][0])
            else:
                psnAckSleep = 20
            my_logger.info("Will sleep for " + str(psnAckSleep) + " seconds before responding to next PlacementStatusNotification")
            self.send_response(200)

        ###############################################################################

        elif "slowPlacementResponse" in self.path:
            #tells sim to sleep before sending PlacementResponse
            global prSleep
            if 'duration' in parse_qs(urlparse(self.path).query):
                prSleep = int(parse_qs(urlparse(self.path).query)['duration'][0])
            else:
                prSleep = 20
            my_logger.info("Will sleep for " + str(prSleep) + " seconds before responding to next PlacementRequest")
            self.send_response(200)

        ###############################################################################

        elif "startSoapRequestSleep" in self.path:
            #tells sim to sleep before sending SOAP responses
            global soapRequestSleep
            soapRequestSleep = True
            my_logger.info("Will sleep for 20 seconds before responding to SOAP requests")
            self.send_response(200)

        ###############################################################################

        elif "stopSoapRequestSleep" in self.path:
            #tells sim to stop sleeping before sending SOAP responses
            #global soapRequestSleep
            soapRequestSleep = False
            my_logger.info("Disabling sleep before responding to SOAP requets")
            self.send_response(200)

        ###############################################################################

        elif "repeatAds" in self.path:
            #tells sim to repeat ads in next PlacementResponse
            global repeatAds
            repeatAds = True
            my_logger.info("Will repeat ads in next PlacementResponse")
            self.send_response(200)

        ###############################################################################

        elif "reloadAssets" in self.path:
            #refresh ad placement dictionary at runtime
            readAdPlacements()
            self.send_response(200)

        ###############################################################################

        elif "errorStatusCode" in self.path:
            global errorStatusCode
            errorStatusCode = True
            my_logger.info("Placement response with status code 1 will be generated")
            self.send_response(200);

        ###############################################################################

        elif "faulty" in self.path:
            global faulty
            faulty = True
            my_logger.info("Wrong response will be generated")
            self.send_response(200);

        ###############################################################################

        elif "systemContextCheck" in self.path:
            global systemContextCheck
            systemContextCheck = True
            my_logger.info("System context check flag has been enabled")
            self.send_response(200);

        ###############################################################################

        elif "emptyPlacementDecision" in self.path:
            global emptyPlacementDecision
            emptyPlacementDecision = True
            my_logger.info("Empty placement decision response will be generated")
            self.send_response(200)

        ###############################################################################

        elif "topLevelEntertainment" in self.path:
            global topLevelEntertainment
            topLevelEntertainment = True
            my_logger.info("Top level entertainment will be inserted")
            self.send_response(200)

        ###############################################################################

        elif "nullPlacementResponse" in self.path:
            global nullPlacementResponse
            nullPlacementResponse = True
            my_logger.info("Null placement response will be generated.")
            self.send_response(200)

        ###############################################################################

        ###############################################################################

        elif "dynamicAdPlacements" in self.path:
            #enable dynamic ad placements for PlacementResponse
            global dynamicAdPlacements
            global numPreroll
            global numMidroll
            global numPostroll

            dynamicAdPlacements = True

            if 'numPreroll' in parse_qs(urlparse(self.path).query):
                numPreroll = int(parse_qs(urlparse(self.path).query)['numPreroll'][0])

            if 'numMidroll' in parse_qs(urlparse(self.path).query):
                numMidroll = int(parse_qs(urlparse(self.path).query)['numMidroll'][0])

            if 'numPostroll' in parse_qs(urlparse(self.path).query):
                numPostroll = int(parse_qs(urlparse(self.path).query)['numPostroll'][0])

            my_logger.info("Dynamic ad placements ENABLED. numPreroll=" + str(numPreroll) + " numMidroll=" + str(numMidroll) + " numPostroll=" + str(numPostroll))
            self.send_response(200)

        ###############################################################################

        elif "settings" in self.path:
            #return current simulator settings

            #calculate the number of segments returned in PlacementResponse
            if dynamicAdPlacements:
                # the +1 is for the content ads are being placed into
                totalSegments = numPreroll + numMidroll + numPostroll + 1
                if numMidroll > 0:
                    #increment since the midroll ad(s) splits the content
                    totalSegments = totalSegments +1
            else:
                #six segments are always returned in pre-built (id's and content PAID are dynamic) PlacementResponse message
                totalSegments = 6

            #sim only supports "joined midrolls", i.e. single midroll containing numMidroll ads
            settings = {"ip": IP, "port": PORT, "identity": IDENTITY, "system": SYSTEM, "delay": DELAY, "negativePlacementStatusAck": negativePlacementStatusAck, "negativePlacementStatusAckCount": negativePlacementStatusAckCount, "psnAckSleep": psnAckSleep, "negativePlacementResponse": negativePlacementResponse, "negativePlacementResponseCount": negativePlacementResponseCount, "prSleep": prSleep, "soapRequestSleep": soapRequestSleep, "repeatAds": repeatAds, "dynamicAdPlacements": dynamicAdPlacements, "numPreroll": numPreroll, "numMidroll": numMidroll, "numPostroll": numPostroll, "joinMidRolls": True, "totalSegments": totalSegments}
            response = json.dumps(settings)

            my_logger.debug("Returning ADS Sim settings: " + response)

            self.server_version = "Python ADSSim"
            self.sys_version = ""
            self.send_response(200)
            self.send_header("Content-Type", 'application/json')
            self.send_header("Content-Length", len(response))
            self.end_headers()
            self.wfile.write(response)

        ###############################################################################

        elif "doRollover" in self.path:
            #roll the log file
            my_logger.info("Client forced log rollover")
            handler.doRollover()
            self.send_response(200)

        ###############################################################################

        else:
            my_logger.warning("Unrecognized path.  Returning 404 response code")
            self.send_response(404)

        ###############################################################################
        ###############################################################################

    def do_DELETE(self):
        """Used to deregister the sim with an ADM"""

        if 'deregisterADM' in parse_qs(urlparse(self.path).query):
            admIp = parse_qs(urlparse(self.path).query)['deregisterADM'][0]
            #admPort and url are hard coded
            url = 'http://%s:8282/adm/2010' % (admIp)

            if 'registrationRef' in parse_qs(urlparse(self.path).query):
                registrationRef = ' registrationRef="' + parse_qs(urlparse(self.path).query)['registrationRef'][0] + '"'
                my_logger.info("Deregistering " + registrationRef + " at " + url)
            else:
                registrationRef = ''
                my_logger.info("Deregistering " + IDENTITY + " at " + url)

            payload = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:adm="http://www.scte.org/schemas/130-3/2008a/adm" xmlns:core="http://www.scte.org/schemas/130-2/2008a/core">
   <soapenv:Header/>
   <soapenv:Body>
      <adm:ADSDeregisterRequest messageId="%s" version="1.1" identity="%s" system="%s"%s/>
   </soapenv:Body>
</soapenv:Envelope>""" % (str(uuid.uuid1()), IDENTITY, SYSTEM, registrationRef)

            my_logger.debug("SCTE-130 ADSDeregisterRequest: " + payload)

            try:
                #admPort and url hardcoded for now
                r = requests.post(url, payload, headers={'content-type': 'text/xml; charset=utf-8'}, timeout=10)
                my_logger.debug("Response from ADM: " + r.text)

                if 'StatusCode class="0"' in r.text:
                    my_logger.info("Deregistration successful")
                    self.send_response(200)
                else:
                    my_logger.warning("Deregistration failed")
                    self.send_response(409)
            except (requests.exceptions.ConnectionError, requests.exceptions.Timeout) as e:
                my_logger.warning("Error sending ADSRegistrationRequest: " + str(type(e)) + str(e))
                self.send_response(502)

        else:
            #assumes that any delete method call is to deregister
            my_logger.warning("deregisterADM query parameter not passed")
            self.send_response(400)

            ##############################################################################
            ##############################################################################

    def do_POST(self):
        """SCTE130 calls the sim needs to handle
        It is assumed anything posted to the sim is XML"""

        ####INCOMING REQUEST####
        #self.log_message("Command: %s" % self.command)
        #self.log_message("Path: %s" % self.path)
        #self.log_message("Headers: %r" % self.headers.items())

        #time.clock() did not work correctly on linux but was fine on windows.  time.time() worked on both
        #start = time.clock()
        start = time.time()

        if DELAY > 0:
            time.sleep(DELAY / 1000.0)

        #http header field names are case insensitive
        if self.headers.getheader('content-length') != None:
            request = self.rfile.read(int(self.headers.getheader('content-length')))
            print
        elif self.headers.getheader('transfer-encoding') == 'chunked':
            #http://en.wikipedia.org/wiki/Chunked_transfer_encoding
            request = ''
            chunkSize = int(self.rfile.readline().strip(), 16)
            while chunkSize != 0:
                request += self.rfile.read(chunkSize)
                #need to read \r\n since this isn't counted in chunk size
                self.rfile.read(2)
                chunkSize = int(self.rfile.readline().strip(), 16)
        else:
            self.send_response(400)
            return

        my_logger.info("Received request from " + str(self.client_address[0]) + ":" + str(self.client_address[1]))

        if "/xml" in self.headers.getheader('content-type'):
            my_logger.debug("Request received: " + minidom.parseString(request).toprettyxml())
        else:
            my_logger.debug("Request received: " + request)

        ###############################################################################

        if "ServiceCheckRequest " in request:
            #outgoing response needs messageRef set to incoming request's messageId
            messageId = request.split('messageId="')[1].split('"')[0]
            my_logger.info("ServiceCheck Request received with messageId: " + messageId)
            response = """<soap-env:Envelope xmlns:soap-env="http://schemas.xmlsoap.org/soap/envelope/"><soap-env:Header/><soap-env:Body><core:ServiceCheckResponse xmlns:core="http://www.scte.org/schemas/130-2/2008a/core" identity="%s" messageId="%s" messageRef="%s" system="%s" version="1.1"><core:StatusCode class="0"/></core:ServiceCheckResponse></soap-env:Body></soap-env:Envelope>""" % (IDENTITY, str(uuid.uuid1()), messageId, SYSTEM)

        ###############################################################################

        elif "ADSDeregistrationNotification " in request:
            global lastDeregistrationNotification
            lastDeregistrationNotification = request

            messageId = request.split('messageId="')[1].split('"')[0]
            my_logger.info("ADSDeregistrationNotification received with messageId: " + messageId)

            #registrationRef is optional
            if "registrationRef=" in request:
                registrationRef = request.split('registrationRef="')[1].split('"')[0]
                my_logger.info("ADM deregistered " + registrationRef)
            else:
                my_logger.debug("ADM sent ADSDeregistrationNotification without a registrationRef")

            response = """<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Header/><soap:Body><adm:ADSDeregistrationAcknowledgement messageId="%s" version="1.1" identity="%s" system="%s" messageRef="%s" xmlns:adm="http://www.scte.org/schemas/130-3/2008a/adm"><core:StatusCode class="0" xmlns:core="http://www.scte.org/schemas/130-2/2008a/core"/></adm:ADSDeregistrationAcknowledgement></soap:Body></soap:Envelope>""" % (str(uuid.uuid1()), IDENTITY, SYSTEM, messageId)

        ###############################################################################

        elif "PlacementStatusNotification " in request:
            global lastPlacementStatusNotification
            lastPlacementStatusNotification = request
            global negativePlacementStatusAck
            global negativePlacementStatusAckCount
            #######################Manojna's code
            # if 'SystemContext>' in request:
            #     if 'ServiceGroup>' in request:
            #         serviceGroup = request.split('ServiceGroup>')[1].split('<')[0]
            #         print serviceGroup
            #         if serviceGroup == "12":
            #             negativePlacementStatusAck=True
            #         else:
            #             negativePlacementStatusAck=False

            # print negativePlacementStatusAck
            if systemContextCheck:
                if 'SystemContext>' in request:
                    if 'ServiceGroup>' in request:
                        if 'Session>' in request:
                            negativePlacementStatusAck = False
                        else:
                            negativePlacementStatusAck = True
                    else:
                        negativePlacementStatusAck = True
                else:
                    negativePlacementStatusAck = True

            #parsing AssetRef element to dig out providerID and assetID
            # assetRef = request.split('AssetRef ')[1].split('>')[0]
            # assetId = assetRef.split('assetID="')[1].split('"')[0]
            # providerId = assetRef.split('providerID="')[1].split('"')[0]
            # if providerId == "hbo.com":
            #     if "SystemContext" in request:
            #         negativePlacementStatusAck = false
            #     else:
            #         negativePlacementStatusAck = true

            messageId = request.split('messageId="')[1].split('"')[0]
            my_logger.info("PlacementStatusNotification received with messageId: " + messageId)
            #####################################

            if negativePlacementStatusAck:
                statusCodeClass = "1"
                print "entered here"
                negativePlacementStatusAckCount = negativePlacementStatusAckCount - 1
                if negativePlacementStatusAckCount == 0:
                    negativePlacementStatusAck = False
                my_logger.info(str(negativePlacementStatusAckCount) + " negative PlacementStatusAck(s) remaining")
            else:
                statusCodeClass = "0"

            global psnAckSleep
            if psnAckSleep > 0:
                time.sleep(psnAckSleep)
                psnAckSleep = 0

            response = """<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Header/><soap:Body><adm:PlacementStatusAcknowledgement messageId="%s" version="1.1" identity="%s" system="%s" messageRef="%s" xmlns:adm="http://www.scte.org/schemas/130-3/2008a/adm"><core:StatusCode class="%s" xmlns:core="http://www.scte.org/schemas/130-2/2008a/core"/></adm:PlacementStatusAcknowledgement></soap:Body></soap:Envelope>""" % (str(uuid.uuid1()), IDENTITY, SYSTEM, messageId, statusCodeClass)

        ###############################################################################

        elif "ServiceStatusNotification " in request:
            global lastServiceStatusNotification
            lastServiceStatusNotification = request

            messageId = request.split('messageId="')[1].split('"')[0]
            my_logger.info("ServiceStatusNotification received with messageId: " + messageId)
            response = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:core="http://www.scte.org/schemas/130-2/2008a/core"><soapenv:Header/><soapenv:Body><core:ServiceStatusAcknowledgement messageId="%s" version="1.1" identity="%s" system="%s" messageRef="%s"><core:StatusCode class="0"/></core:ServiceStatusAcknowledgement></soapenv:Body></soapenv:Envelope>""" % (str(uuid.uuid1()), IDENTITY, SYSTEM, messageId)

        ###############################################################################

        elif "PlacementRequest " in request:
            #see OneNote page for details on how the PlacementResponse is built
            messageId = request.split('messageId="')[1].split('"')[0]
            my_logger.info("PlacementRequest received with messageId: " + messageId)

            global lastPlacementRequest
            global lastPlacementResponse
            global repeatAds
            lastPlacementRequest = request

            global negativePlacementResponse
            global negativePlacementResponseCount
            if negativePlacementResponse:
                statusCodeClass = "1"
                negativePlacementResponseCount = negativePlacementResponseCount - 1
                if negativePlacementResponseCount == 0:
                    negativePlacementResponse = False
                my_logger.info(str(negativePlacementResponseCount) + " negative PlacementResponse(s) remaining")
            else:
                statusCodeClass = "0"

            global prSleep
            if prSleep > 0:
                time.sleep(prSleep)
                prSleep = 0

            #parse opportunity id and don't assume it's the first attribute
            poId = request.split('PlacementOpportunity ')[1].split('id="')[1].split('"')[0]

            #assumes TerminalAddress if present is of type MAC
            if 'TerminalAddress type="MAC">' in request:
                mac = request.split('TerminalAddress type="MAC">')[1].split('<')[0]
                client = '\n    <ns2:Client><ns2:TerminalAddress type="MAC">%s</ns2:TerminalAddress></ns2:Client>' % (mac)
            else:
                client = ""

            #added by Manojna
            #gets the Service Group
            if 'ServiceGroup>' in request:
                serviceGroup = request.split('ServiceGroup>')[1].split('<')[0]
                print serviceGroup
            if 'Session>' in request:
                sessionNum = request.split('Session>')[1].split('<')[0]
                print sessionNum
            if 'CurrentDateTime>' in request:
                currentDate = request.split('CurrentDateTime>')[1].split('<')[0]
                print currentDate
            if 'TargetCode key="postalCodes">' in request:
                postalCode = request.split('TargetCode key="postalCodes">')[1].split('<')[0]
                print postalCode
            if 'TargetCode key="serviceRegion">' in request:
                serviceReg = request.split('TargetCode key="serviceRegion">')[1].split('<')[0]
                print serviceReg
            if 'TargetCode key="requestType">' in request:
                requestT = request.split('TargetCode key="requestType">')[1].split('<')[0]
                print requestT


            #parsing AssetRef element to dig out providerID and assetID
            assetRef = request.split('AssetRef ')[1].split('>')[0]
            assetId = assetRef.split('assetID="')[1].split('"')[0]
            providerId = assetRef.split('providerID="')[1].split('"')[0]

            my_logger.info("PlacementRequest parameters: PlacementOpportunity id=" + poId + " assetId=" + assetId + " providerId=" + providerId)

            if statusCodeClass == "1":
                response = """<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
   <S:Body>
      <ns2:PlacementResponse identity="%s" messageId="%s" messageRef="%s" system="%s" version="1.1" xmlns="http://www.scte.org/schemas/130-2/2008a/core" xmlns:ns2="http://www.scte.org/schemas/130-3/2008a/adm" xmlns:ns3="http://www.scte.org/schemas/130-8/2010a/gis" xmlns:ns4="http://www.scte.org/schemas/130-3/2008a/adm/podm">
         <StatusCode class="1"/>
      </ns2:PlacementResponse>
   </S:Body>
</S:Envelope>""" % (IDENTITY, str(uuid.uuid1()), messageId, SYSTEM)
            elif topLevelEntertainment:
                response = buildTopLevelEntertainmentResponse(IDENTITY, SYSTEM, messageId, poId, providerId, assetId)
            elif emptyPlacementDecision:
                response = buildEmptyPlacementDecisionResponse(IDENTITY, SYSTEM, messageId, poId, providerId, assetId)
            elif errorStatusCode:
                response = buildErrorResponse(IDENTITY, SYSTEM, messageId, poId, providerId, assetId)
            elif nullPlacementResponse:
                response = buildNullPlacementResponse(IDENTITY, SYSTEM, messageId, poId, providerId, assetId)
            elif faulty:
                response = buildFaultyPlacementResponse(IDENTITY, SYSTEM, messageId, poId, providerId, assetId, serviceGroup, sessionNum, currentDate, mac, postalCode, serviceReg, requestT)
            elif dynamicAdPlacements:
                response = buildPlacementResponse(IDENTITY, SYSTEM, messageId, poId, providerId, assetId)
            elif repeatAds:
                response = """<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
    <S:Body>
            <ns2:PlacementResponse xmlns="http://www.scte.org/schemas/130-2/2008a/core" xmlns:ns2="http://www.scte.org/schemas/130-3/2008a/adm" xmlns:ns3="http://www.scte.org/schemas/130-8/2010a/gis" xmlns:ns4="http://www.scte.org/schemas/130-3/2008a/adm/podm" identity="%s" messageId="%s" messageRef="%s" system="%s" version="1.1">
    <StatusCode class="0" />%s
    <ns2:PlacementDecision id="%s" placementOpportunityRef="%s">
        <ns2:OpportunityBinding breakOpportunitiesExpected="1" breakOpportunitySequence="1" opportunityNumber="1" opportunityType="Preroll" poGroupIndex="1" />
        <ns2:Placement action="replace" id="%s" position="1">
            <Content>
                <AssetRef assetID="repeated_ad" providerID="AdvertiserF01" />
                <Tracking>%s</Tracking>
            </Content>
            <ns2:PlacementConstraints trickModeRestriction="fastForward"/>
        </ns2:Placement>
    </ns2:PlacementDecision>
    <ns2:PlacementDecision id="%s" placementOpportunityRef="%s">
        <ns2:Entertainment poid="1.1,action:replace,npt1:00:00:00.000-00:00:30.000,npt2:00:10:00.000-00:10:30.000">
            <Content>
                <AssetRef assetID="%s" providerID="%s" />
            </Content>
            <ns2:EntertainmentNPT scale="1.0">30.000-600.000</ns2:EntertainmentNPT>
        </ns2:Entertainment>
    </ns2:PlacementDecision>
    <ns2:PlacementDecision id="%s" placementOpportunityRef="%s">
        <ns2:OpportunityBinding breakOpportunitiesExpected="2" breakOpportunitySequence="1:2" opportunityNumber="2" opportunityType="Midroll" poGroupIndex="2" />
        <ns2:Placement action="replace" id="%s" position="2">
            <Content>
                <AssetRef assetID="repeated_ad" providerID="AdvertiserF01" />
                <Tracking>%s</Tracking>
            </Content>
            <ns2:PlacementConstraints trickModeRestriction="rewind,pause,fastForward"/>
        </ns2:Placement>
        <ns2:Placement action="replace" id="%s" position="3">
            <Content>
                <AssetRef assetID="repeated_ad" providerID="AdvertiserF01" />
                <Tracking>%s</Tracking>
            </Content>
        </ns2:Placement>
    </ns2:PlacementDecision>
    <ns2:PlacementDecision id="%s" placementOpportunityRef="%s">
        <ns2:Entertainment poid="3.1,action:replace,npt1:00:10:00.000-00:10:30.000,npt2:00:30:00.000-00:30:00.000">
            <Content>
                <AssetRef assetID="%s" providerID="%s" />
            </Content>
            <ns2:EntertainmentNPT scale="1.0">630.000-1800.000</ns2:EntertainmentNPT>
        </ns2:Entertainment>
    </ns2:PlacementDecision>
    <ns2:PlacementDecision id="%s" placementOpportunityRef="%s">
        <ns2:OpportunityBinding breakOpportunitiesExpected="1" breakOpportunitySequence="1" opportunityNumber="4" opportunityType="Postroll" poGroupIndex="99" />
        <ns2:Placement action="replace" id="%s" position="4">
            <Content>
                <AssetRef assetID="repeated_ad" providerID="AdvertiserF01" />
                <Tracking>%s</Tracking>
            </Content>
        </ns2:Placement>
    </ns2:PlacementDecision>
</ns2:PlacementResponse>
    </S:Body>
</S:Envelope>""" % (IDENTITY, str(uuid.uuid1()), messageId, SYSTEM, client, str(uuid.uuid1()), poId, str(uuid.uuid1()), str(uuid.uuid1()), str(uuid.uuid1()), poId, assetId, providerId, str(uuid.uuid1()), poId, str(uuid.uuid1()), str(uuid.uuid1()), str(uuid.uuid1()), str(uuid.uuid1()), str(uuid.uuid1()), poId, assetId, providerId, str(uuid.uuid1()), poId, str(uuid.uuid1()), str(uuid.uuid1()))
                repeatAds = False
            else:
                response = """<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
    <S:Body>
            <ns2:PlacementResponse xmlns="http://www.scte.org/schemas/130-2/2008a/core" xmlns:ns2="http://www.scte.org/schemas/130-3/2008a/adm" xmlns:ns3="http://www.scte.org/schemas/130-8/2010a/gis" xmlns:ns4="http://www.scte.org/schemas/130-3/2008a/adm/podm" identity="%s" messageId="%s" messageRef="%s" system="%s" version="1.1">
    <StatusCode class="0" />%s
    <ns2:PlacementDecision id="%s" placementOpportunityRef="%s">
        <ns2:OpportunityBinding breakOpportunitiesExpected="1" breakOpportunitySequence="1" opportunityNumber="1" opportunityType="Preroll" poGroupIndex="1" />
        <ns2:Placement action="replace" id="%s" position="1">
            <Content>
                <AssetRef assetID="AdvertiserF01_creative_pre_30s_movie" providerID="AdvertiserF01" />
                <Tracking>%s</Tracking>
            </Content>
            <ns2:PlacementConstraints trickModeRestriction="fastForward"/>
        </ns2:Placement>
    </ns2:PlacementDecision>
    <ns2:PlacementDecision id="%s" placementOpportunityRef="%s">
        <ns2:Entertainment poid="1.1,action:replace,npt1:00:00:00.000-00:00:30.000,npt2:00:10:00.000-00:10:30.000">
            <Content>
                <AssetRef assetID="%s" providerID="%s" />
            </Content>
            <ns2:EntertainmentNPT scale="1.0">30.000-600.000</ns2:EntertainmentNPT>
        </ns2:Entertainment>
    </ns2:PlacementDecision>
    <ns2:PlacementDecision id="%s" placementOpportunityRef="%s">
        <ns2:OpportunityBinding breakOpportunitiesExpected="2" breakOpportunitySequence="1:2" opportunityNumber="2" opportunityType="Midroll" poGroupIndex="2" />
        <ns2:Placement action="replace" id="%s" position="2">
            <Content>
                <AssetRef assetID="AdvertiserF01_creative_mid1_30s_movie" providerID="AdvertiserF01" />
                <Tracking>%s</Tracking>
            </Content>
            <ns2:PlacementConstraints trickModeRestriction="rewind,pause,fastForward"/>
        </ns2:Placement>
        <ns2:Placement action="replace" id="%s" position="3">
            <Content>
                <AssetRef assetID="AdvertiserF01_creative_mid2_30s_movie" providerID="AdvertiserF01" />
                <Tracking>%s</Tracking>
            </Content>
        </ns2:Placement>
    </ns2:PlacementDecision>
    <ns2:PlacementDecision id="%s" placementOpportunityRef="%s">
        <ns2:Entertainment poid="3.1,action:replace,npt1:00:10:00.000-00:10:30.000,npt2:00:30:00.000-00:30:00.000">
            <Content>
                <AssetRef assetID="%s" providerID="%s" />
            </Content>
            <ns2:EntertainmentNPT scale="1.0">630.000-1800.000</ns2:EntertainmentNPT>
        </ns2:Entertainment>
    </ns2:PlacementDecision>
    <ns2:PlacementDecision id="%s" placementOpportunityRef="%s">
        <ns2:OpportunityBinding breakOpportunitiesExpected="1" breakOpportunitySequence="1" opportunityNumber="4" opportunityType="Postroll" poGroupIndex="99" />
        <ns2:Placement action="replace" id="%s" position="4">
            <Content>
                <AssetRef assetID="AdvertiserF01_creative_post_30s_movie" providerID="AdvertiserF01" />
                <Tracking>%s</Tracking>
            </Content>
        </ns2:Placement>
    </ns2:PlacementDecision>
</ns2:PlacementResponse>
    </S:Body>
</S:Envelope>""" % (IDENTITY, str(uuid.uuid1()), messageId, SYSTEM, client, str(uuid.uuid1()), poId, str(uuid.uuid1()), str(uuid.uuid1()), str(uuid.uuid1()), poId, assetId, providerId, str(uuid.uuid1()), poId, str(uuid.uuid1()), str(uuid.uuid1()), str(uuid.uuid1()), str(uuid.uuid1()), str(uuid.uuid1()), poId, assetId, providerId, str(uuid.uuid1()), poId, str(uuid.uuid1()), str(uuid.uuid1()))

            lastPlacementResponse = response

        ###############################################################################

        else:
            my_logger.warning("Unknown request")
            self.send_response(400)
            return

        ###########################OUTGOING RESPONSE###################################

        if soapRequestSleep:
            my_logger.info("Sleeping for 20 seconds")
            time.sleep(20)

        self.server_version = "Python ADSSim"
        self.sys_version = ""

        self.send_response(200)
        self.send_header("Content-Type", 'text/xml;charset="utf-8"')
        self.send_header("Content-Length", len(response))
        self.end_headers()

        self.wfile.write(response)

        #end = time.clock()
        end = time.time()
        #int to to truncate
        my_logger.info("Processing time: " + str(int((end - start) * 1000)) + "ms")

        my_logger.debug("ADS Sim response: " + response)

###############################################################################
###############################################################################

class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
    """Handle requests in a separate thread."""

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", dest="ip", required=True)
    parser.add_argument("--port", dest="port", required=True, type=int)
    parser.add_argument("--identity", dest="identity", default="PyADSSim")
    parser.add_argument("--system", dest="system", default="ads")
    parser.add_argument("--delayms", dest="delayms", default=0, type=int, help="ms delay before processing HTTP POST requests")
    parser.add_argument("--logdir", dest="logdir", default="", help="directory to store log files.  directory must already exist prior to running script")
    parser.add_argument("--timedlogrotation", action="store_true", dest="timedlogrotation", default=False)
    args = parser.parse_args()
    IP = args.ip
    PORT = args.port
    IDENTITY = args.identity
    SYSTEM = args.system
    DELAY = args.delayms
    TIMED_LOG_ROTATION = args.timedlogrotation

    LOG_DIR = args.logdir
    LOG_FILENAME = "adssim.log"

    if LOG_DIR:
        LOG_FILENAME = LOG_DIR + "/" + LOG_FILENAME
    my_logger = logging.getLogger('MyLogger')
    my_logger.setLevel(logging.DEBUG)
    if TIMED_LOG_ROTATION:
        #12 hours of log files rotated every 10 mins.  adssim.log.2015-10-28_10-49-40
        #https://docs.python.org/2/library/logging.handlers.html#timedrotatingfilehandler
        handler = logging.handlers.TimedRotatingFileHandler(LOG_FILENAME, when="M", interval=10, backupCount=72, utc=True)
        #http://stackoverflow.com/questions/338450/timedrotatingfilehandler-changing-file-name
        handler.suffix = "%Y-%m-%d_%H-%M-%S"
    else:
        handler = logging.handlers.RotatingFileHandler(LOG_FILENAME, maxBytes=5242880, backupCount=5)
    handler.setLevel(logging.DEBUG)
    logging.Formatter.converter = time.gmtime
    #log format: timestamp|log level|thread id|message
    #example 2015-09-30T18:28:55.885Z|INFO|8176|ServiceCheck Request received with messageId: 08cd26aa-8439-411b-8103-d63ad04f8e87
    formatter = logging.Formatter('%(asctime)s.%(msecs)03dZ|%(levelname)s|%(thread)d|%(message)s', '%Y-%m-%dT%H:%M:%S')
    handler.setFormatter(formatter)
    my_logger.addHandler(handler)
    my_logger.info("Python ADS simulator started.  Ready for action!")
    my_logger.info("All HTTP POST requests will be delayed by " + str(DELAY) + "ms")

    adPlacements = {}
    readAdPlacements()
    lastPlacementRequest = ''
    lastPlacementResponse = ''
    lastPlacementStatusNotification = ''
    lastDeregistrationNotification = ''
    lastServiceStatusNotification = ''
    negativePlacementStatusAck = False
    negativePlacementStatusAckCount = 0
    psnAckSleep = 0
    negativePlacementResponse = False
    negativePlacementResponseCount = 0
    prSleep = 0
    soapRequestSleep = False
    repeatAds = False

    dynamicAdPlacements = False
    numPreroll = 0
    numMidroll = 0
    numPostroll = 0
    faulty = False
    nullPlacementResponse = False
    errorStatusCode = False
    emptyPlacementDecision = False
    topLevelEntertainment = False
    systemContextCheck = False

    server = ThreadedHTTPServer((IP, PORT), ADSSim)
    print 'Starting ADS simulator, use <Ctrl-C> to stop.'
    server.serve_forever()