#!/bin/bash

# simRestartADSpythonSim.sh - used to start/stop/restart (python) ADS simulator
#
#   usage : /root/simRestartADSpythonSim.sh [SCRIPT_ACTION] [SIMULATOR_VERSION]
#
#     $1  :  [SCRIPT_ACTION]      :  valid actions = [ start, stop, restart, status ]
#
#     $2  :  [SIMULATOR_VERSION]  :  specific simulator version to start (optional)


SIMULATOR_HOME="$( dirname $( readlink -f $0 ) )"     # where this script runs from

#LATEST_VERSION="$( basename $( ls -d ${SIMULATOR_HOME}/adssim.admng-tests.*/ | sed 's/.admng-tests//g' | sed 's+/$++g' | sort -V | tail -1 ) )"
LATEST_VERSION=adssim5


function current_status ()
{
  if [[ $( ps -ef | grep "python ADSSim5.py --ip 10.116.1.131 --port 1028" | grep -v "grep" ) ]]; then
    echo "running"
  else
    echo "stopped"
  fi
}


if (( $# > 0 )) && ( [[ $1 = "start" ]] || [[ $1 = "stop" ]] || [[ $1 = "restart" ]] || [[ $1 = "status" ]] ); then
  SCRIPT_ACTION=$1
  shift
else
  SCRIPT_ACTION="status"
fi


if (( $# > 0 )) && [[ -d "${SIMULATOR_HOME}/$1" ]]; then
  START_VERSION="$( echo $1 | sed 's/.admng-tests//g' )"
elif (( $# > 0 )) && [[ -d "${SIMULATOR_HOME}/adssim5.admng-tests.$( echo $1 | sed 's/adssim.//g' )" ]]; then
  START_VERSION="$( echo "adssim5.$1" | sed 's/adssim5.adssim5/adssim5/g' )"
else
  START_VERSION="${LATEST_VERSION}"
fi


if [[ $( current_status ) = "running" ]]; then
  STOP_VERSION="$( basename $( pwdx $( ps -ef | grep "python ADSSim5.py --ip 10.116.1.131 --port 1028" | grep -v "grep" | head -1 | awk '{ print $2 }' ) | awk '{ print $NF }' | sed 's+admng-tests/sims+git+g' ) )"
else
  STOP_VERSION="${LATEST_VERSION}"
fi


IP_ADDRESS="$( hostname -I 2>/dev/null | awk '{ print $1 }' )"

if [[ -z ${IP_ADDRESS} ]]; then
  IP_ADDRESS="$( ifconfig eth0 2>/dev/null | grep "inet addr" | awk '{ print $2 }' | cut -d: -f2 )"
fi

if [[ -z ${IP_ADDRESS} ]]; then
  IP_ADDRESS="$( ifconfig eth1 2>/dev/null | grep "inet addr" | awk '{ print $2 }' | cut -d: -f2 )"
fi

if [[ $( current_status ) = "running" ]]; then
  IP_ADDRESS="$( ps -ef | grep "python ADSSim5.py --ip 10.116.1.131 --port 1028" | grep -v "grep" | grep -o "[-][-]ip[ =][.0-9]*" | sed 's/=/ /g' | awk '{ print $NF }' )"
fi


function start_simulator ()
{
  echo "$( date ) : $( basename $0 ) start: (${START_VERSION})"
  echo " IP Address is : ${IP_ADDRESS}"

  cd ${SIMULATOR_HOME}/$( echo ${START_VERSION} | sed 's+git+admng-tests/sims+g' )
  python ADSSim5.py   --ip ${IP_ADDRESS} --port 1028 --identity adm --system ERICSSON_ADM --delayms 0 --logdir /tmp/ADSSim5_m229 --timedlogrotation > /dev/null 2>&1 &
  disown -h     # new process will ignore SIGHUP (to run as background process)

  if [[ $( current_status ) = "running" ]]; then
    echo "$( date ) : $( basename $0 ) start: (${START_VERSION}) success"
  else
    echo "$( date ) : $( basename $0 ) start: (${START_VERSION}) failure"
  fi
}


function stop_simulator ()
{
  echo "$( date ) : $( basename $0 ) stop : (${STOP_VERSION})"

  if [[ $( current_status ) = "running" ]]; then
    echo "$( date ) : $( basename $0 ) stop : (${STOP_VERSION}) kill -9"
    ps -ef | grep "python ADSSim5.py --ip 10.116.1.131 --port 1028" | grep -v "grep" | head -1 | awk '{ print $2 }' | xargs kill -9 > /dev/null 2>&1
  fi
}


if [[ ${SCRIPT_ACTION} = "start" ]]; then

  if [[ $( current_status ) = "running" ]]; then
    echo "$( date ) : $( basename $0 ) start: (${START_VERSION}) running"
  else
    start_simulator ${START_VERSION}
  fi

elif [[ ${SCRIPT_ACTION} = "stop" ]]; then

  if [[ $( current_status ) = "running" ]]; then
    stop_simulator ${STOP_VERSION}
  else
    echo "$( date ) : $( basename $0 ) stop : (${STOP_VERSION}) stopped"
  fi

elif [[ ${SCRIPT_ACTION} = "restart" ]]; then

  if [[ $( current_status ) = "running" ]]; then
    stop_simulator ${STOP_VERSION}
  else
    echo "$( date ) : $( basename $0 ) stop : (${STOP_VERSION}) stopped"
  fi

  start_simulator ${START_VERSION}

else    # default action = "status"

  if [[ $( current_status ) = "running" ]]; then
    echo "$( date ) : $( basename $0 ) status (${STOP_VERSION}) running"
  else
    echo "$( date ) : $( basename $0 ) status (${START_VERSION}) stopped"
  fi

fi

